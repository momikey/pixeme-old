import Vue from 'vue'
import VueTailwind from 'vue-tailwind'
import VueScreen from 'vue-screen'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import Theme from './theme'
import './assets/tailwind.css'

Vue.config.productionTip = false

Vue.use(VueTailwind, {
    theme: Theme
});

Vue.use(VueScreen, 'tailwind');

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
