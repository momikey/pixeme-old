module.exports = {
  plugins: {
    tailwindcss: {},
    'vue-cli-plugin-tailwind/purgecss': {
      content: [
        './src/theme.js'
      ]
    },
    autoprefixer: {}
  }
}
