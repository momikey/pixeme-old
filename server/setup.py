from setuptools import setup, find_packages

version = {}
with open('./pixeme/version.py') as fp:
    exec(fp.read(), version)

setup(
    name='pixeme',
    version=version['__version__'],
    packages=['pixeme']
)