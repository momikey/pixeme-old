"""Initial

Revision ID: 269103fff526
Revises: 
Create Date: 2020-04-10 17:48:40.557163

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '269103fff526'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('accounts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=64), nullable=False),
    sa.Column('password', sa.Text(), nullable=False),
    sa.Column('email', sa.Text(), nullable=False),
    sa.Column('date_created', sa.TIMESTAMP(), server_default=sa.text("TIMEZONE('utc', CURRENT_TIMESTAMP)"), nullable=True),
    sa.Column('last_login', sa.TIMESTAMP(), nullable=True),
    sa.Column('last_activity', sa.TIMESTAMP(), nullable=True),
    sa.Column('role', sa.Enum('banned', 'user', 'moderator', 'admin', name='accountrole'), server_default='user', nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('username')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('accounts')
    op.drop_table('users')
    # ### end Alembic commands ###
