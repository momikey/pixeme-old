"""Language search

Revision ID: c6273fdfba1e
Revises: 760a13c62203
Create Date: 2020-06-23 17:33:47.830374

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c6273fdfba1e'
down_revision = '760a13c62203'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('create extension pg_trgm;')
    op.create_index('name_english_idx', 'languages', ['english'], unique=False, postgresql_using='gin', postgresql_ops={'english': 'gin_trgm_ops'})
    op.create_index('name_native_idx', 'languages', ['native'], unique=False, postgresql_using='gin', postgresql_ops={'native': 'gin_trgm_ops'})
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('name_native_idx', table_name='languages')
    op.drop_index('name_english_idx', table_name='languages')
    # ### end Alembic commands ###
