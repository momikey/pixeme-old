"""Phrase table

Revision ID: c48fb87271c9
Revises: 799a1a4b8c22
Create Date: 2020-04-11 20:11:07.709233

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c48fb87271c9'
down_revision = '799a1a4b8c22'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('phrases',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('text', sa.Text(), nullable=False),
    sa.Column('status', sa.Enum('proposed', 'accepted', 'rejected', 'questioned', name='phrasestatus'), server_default='proposed', nullable=True),
    sa.Column('language_id', sa.Integer(), nullable=False),
    sa.Column('submitter_id', sa.Integer(), nullable=False),
    sa.Column('approved_by_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['approved_by_id'], ['users.id'], ),
    sa.ForeignKeyConstraint(['language_id'], ['languages.id'], ),
    sa.ForeignKeyConstraint(['submitter_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('phrases')
    # ### end Alembic commands ###
