import pytest

from pixeme.models import *
from pixeme.api.tag import BLUEPRINT_PATH

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

def test_tag_api_get_tags(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'tags' in json
    assert len(json['tags']) > 0
    assert json['tags'][0]['id'] == mock_tag
    assert json['tags'][0]['text'] == 'test image'

def test_tag_api_get_tag_by_id(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_tag))
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_tag
    assert json['text'] == 'test image'

def test_tag_api_get_tag_by_id_bad_id(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_tag+1))
    assert rv.status_code == 404

def test_tag_api_get_tag_by_text(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/text/{text}'.format(text='test image'))
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_tag
    assert json['text'] == 'test image'

def test_tag_api_new_tag(client, mock_account, make_user_admin, access_token):
    rv = client.post(BLUEPRINT_PATH + '/text/my-tag',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ]
    )
    assert rv.status_code == 201
    assert rv.headers.get('Location') is not None

def test_tag_api_new_tag(client, mock_tag, mock_account, make_user_admin, access_token):
    rv = client.post(BLUEPRINT_PATH + '/text/test image',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ]
    )
    assert rv.status_code == 409

def test_tag_api_get_new_tags(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/new')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'tags' in json
    assert json['tags'][0]['id'] == mock_tag

def test_tag_api_get_new_tags(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/popular')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'tags' in json
    assert json['tags'][0]['id'] == mock_tag

def test_tag_api_search(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/search?q=test')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'results' in json
    assert len(json['results']) == 1
    assert json['results'][0]['id'] == mock_tag

def test_tag_api_search_no_results(client):
    rv = client.get(BLUEPRINT_PATH + '/search?q=test')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'results' in json
    assert json['results'] == []

@skip_pytest_flask_sqlalchemy_delete
def test_tag_api_delete_tag(client, mock_tag, mock_account, make_user_admin, access_token):
    rv = client.delete(BLUEPRINT_PATH + '/text/test image',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ]
    )
    assert rv.status_code == 204

def test_tag_api_delete_tag_bad_text(client, mock_account, make_user_admin, access_token):
    rv = client.delete(BLUEPRINT_PATH + '/text/my-tag',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ]
    )
    assert rv.status_code == 404
