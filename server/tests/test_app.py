def test_client(client):
    rv = client.get('/')
    assert rv.status_code == 200

def test_debug_routes(client):
    rv = client.get('/debug/routes')
    assert rv.get_json() is not None

