import pytest

from pixeme.models import *
from pixeme.controllers.language import *

def test_get_most_popular_languagess(app, db_session, mock_account, mock_language):
    # Add the mock language as a fluency for the mock user.
    with app.app_context():
        l = db_session.query(Language).get(mock_language)
        a = db_session.query(Account).get(mock_account)
        l.speakers.append(a.user)

        db_session.add(l)
        db_session.commit()

    result = get_most_popular_languages()
    assert result is not None
    assert len(result) == 1

    first = result[0]
    assert first[0].id == mock_language
    assert first[1] == 1

def test_get_language_count(app, db_session, mock_account, mock_language):
    with app.app_context():
        l = db_session.query(Language).get(mock_language)
        a = db_session.query(Account).get(mock_account)
        l.speakers.append(a.user)

        db_session.add(l)
        db_session.commit()

    result = get_language_count()
    assert result == 1

def test_search_similar(app, db_session, mock_language):
    result = search_similar('en')
    assert mock_language in [r.id for r in result]

def test_search_similar_no_results(mock_language):
    bad_result = search_similar('x')
    assert bad_result == []
