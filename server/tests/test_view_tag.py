import pytest

import re

from flask import session

from pixeme.models import *
from pixeme.views.tag import BLUEPRINT_PATH

def test_tag_view_get_by_id(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_tag))
    assert rv.status_code != 404

def test_tag_view_get_by_id_bad_id(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_tag + 1))
    assert rv.status_code == 404

def test_tag_view_get_by_text(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/text/test image')
    assert rv.status_code == 200

    assert b"Tag: test image" in rv.data
    assert b"1 image with this tag" in rv.data

def test_tag_view_get_by_text_bad(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/text/nothing')
    assert rv.status_code == 404

def test_tag_view_search_id(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/search?id=' + str(mock_tag))
    assert rv.status_code < 400

def test_tag_view_search_text(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/search?text=test image')
    assert rv.status_code < 400

def test_tag_view_search_partial_text(app, client, db_session, mock_tag):
    with app.app_context():
        other = Tag(text="other image")
        db_session.add(other)
        db_session.commit()

    rv = client.get(BLUEPRINT_PATH + '/search?text=image')
    assert rv.status_code < 400 or rv.status_code > 499

def test_tag_view_search_no_data(client):
    rv = client.get(BLUEPRINT_PATH + '/search')
    assert rv.status_code == 400

def test_tag_explore_tags(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert "Explore tags" in data
    # Most of this view is dynamically generated
