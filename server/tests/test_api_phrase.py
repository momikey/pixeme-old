import pytest

from pixeme.models import *
from pixeme.api.phrase import BLUEPRINT_PATH

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

def test_phrase_api_get_phrases(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'phrases' in json
    assert len(json['phrases']) > 0
    assert json['phrases'][0]['id'] == mock_phrase
    assert json['phrases'][0]['text'] == 'test'

def test_phrase_api_get_language_by_id(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_phrase))
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_phrase
    assert json['text'] == 'test'

def test_phrase_api_get_language_by_id_bad_id(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_phrase+1))
    assert rv.status_code == 404

def test_phrase_api_search(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/search?q=test')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'results' in json
    assert len(json['results']) == 1
    assert json['results'][0]['id'] == mock_phrase

def test_phrase_api_search_no_results(client):
    rv = client.get(BLUEPRINT_PATH + '/search?q=test')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'results' in json
    assert json['results'] == []

def test_phrase_api_get_phrase_images(client, mock_phrase, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_phrase) + '/images')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'images' in json
    assert len(json['images']) == 1
    assert json['images'][0]['id'] == mock_image
    assert 'url' in json['images'][0]

def test_phrase_api_get_phrase_images_bad_id(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_phrase+1) + '/images')
    assert rv.status_code == 404

def test_phrase_api_add_phrase(client, mock_language, mock_account, make_user_admin, access_token):
    rv = client.post(BLUEPRINT_PATH + '/',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'text': 'test',
            'status': 'proposed',
            'submitter_id': mock_account,
            'language_id': mock_language
        }
    )
    assert rv.status_code == 201

    json = rv.get_json()

    get = client.get(rv.headers['Location'])
    assert get.status_code == 200

    retrieved = get.get_json()
    assert retrieved['id'] == json['phrase']['id']

def test_phrase_api_update_phrase(client, mock_phrase, mock_account, access_token, make_user_admin):
    rv = client.put(BLUEPRINT_PATH + '/id/' + str(mock_phrase),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'id': mock_phrase,
            'status': 'questioned'
        }
    )
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'phrase' in json
    assert json['phrase']['id'] == mock_phrase
    assert json['phrase']['status'] == 'questioned'

@skip_pytest_flask_sqlalchemy_delete
def test_phrase_api_delete_phrase(client, mock_phrase, mock_account, access_token, make_user_admin):
    rv = client.delete(BLUEPRINT_PATH + '/id/' + str(mock_phrase),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ]
    )
    assert rv.status_code == 204

def test_phrase_api_update_phrase_status(client, mock_phrase, mock_account, access_token, make_user_admin):
    rv = client.put(BLUEPRINT_PATH + '/id/{id}/status'.format(id=mock_phrase),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'id': mock_phrase,
            'status': 'questioned'
        }
    )
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'phrase' in json
    assert json['phrase']['id'] == mock_phrase
    assert json['phrase']['status'] == 'questioned'
