import pytest
from flask import session

from pixeme.models import *
from pixeme.views.user import BLUEPRINT_PATH

def test_user_view_get_profile(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_account))
    assert rv.status_code != 404

def test_user_view_get_profile_bad_id(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_account + 1))
    assert rv.status_code == 404

def test_user_view_get_my_profile(client, mock_account, session_login):
    rv = client.get(BLUEPRINT_PATH + '/me')
    assert rv.status_code == 200

def test_user_view_get_my_profile_not_logged_in(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/me')
    assert rv.status_code == 403

def test_user_view_get_user_languages(app, db_session, client, mock_account, mock_language):
    # Add fluency for the mock user
    with app.app_context():
        l = db_session.query(Language).get(mock_language)
        a = db_session.query(Account).get(mock_account)
        l.speakers.append(a.user)

        db_session.add(l)
        db_session.commit()

    rv = client.get(BLUEPRINT_PATH + '/id/{user}/languages'.format(user=str(mock_account)))
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert "/language/id/" + str(mock_language) in data

def test_user_view_get_user_images(client, mock_account, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/{user}/images'.format(user=str(mock_account)))
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert "/image/id/" + str(mock_image) in data

def test_user_view_get_user_phrases(client, mock_account, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/id/{user}/phrases'.format(user=str(mock_account)))
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert "/phrase/id/" + str(mock_phrase) in data
