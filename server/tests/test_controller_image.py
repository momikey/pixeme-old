import pytest

from pixeme.models import *
from pixeme.controllers.image import *

def test_controller_remove_tags_from_image(app, db_session, mock_image, mock_tag):
    result = remove_tags_from_image(mock_image, 'test image')
    assert mock_tag not in [t.id for t in result.tags]

def test_controller_remove_tags_from_image_not_present(mock_image, mock_tag):
    result = remove_tags_from_image(mock_image, 'nothing')
    assert len(result.tags) == 1

def test_controller_get_random_images_sample(app, db_session):
    with app.app_context():
        result = get_random_images_sample()            
        assert len(result) <= 50

def test_controller_get_newest_images(mock_image):
    result = get_newest_images()

    assert len(result) == 1
    assert result[0].id == mock_image

def test_controller_get_most_tagged_images(mock_image, mock_tag):
    result = get_most_tagged_images()

    assert len(result) == 1
    assert result[0].id == mock_image
    assert result[0].tags[0].id == mock_tag

def test_controller_get_most_translated_images(mock_image, mock_phrase):
    result = get_most_translated_images()

    assert len(result) == 1
    assert result[0].id == mock_image
    assert result[0].phrases[0].id == mock_phrase
