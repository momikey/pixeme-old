import pytest

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

from pixeme.models import *
from pixeme.controllers.tag import *

def test_get_most_popular_tags(app, db_session, mock_account, mock_language, mock_phrase, mock_image, mock_tag):
    result = get_most_popular_tags()
    assert result is not None
    assert len(result) == 1

    first = result[0]
    assert first[0].id == mock_tag
    assert first[1] == 1

def test_get_newest_tags(mock_tag):
    result = get_newest_tags()
    assert result is not None
    assert len(result) == 1

    first = result[0]
    assert first.id == mock_tag

def test_tag_search_similar(app, db_session, mock_tag):
    result = search_similar('test')
    assert mock_tag in [r.id for r in result]

def test_tag_search_similar_no_results(mock_tag):
    bad_result = search_similar('x')
    assert bad_result == []

def test_tag_controller_add_tag(app, db_session):
    result = add_tag("test")
    assert result is not None
    assert result.id is not None

    with app.app_context():
        tag = db_session.query(Tag).get(result.id)
        assert tag.text == "test"

@skip_pytest_flask_sqlalchemy_delete
def test_delete_tag(app, db_session, mock_tag):
    with app.app_context():
        tag = db_session.query(Tag).get(mock_tag)

        result = delete_tag(tag.text)
        assert result is not None

        after = db_session.query(Tag).get(mock_tag)
        assert after is None
