import pytest

from pixeme.models import *
from pixeme.controllers.user import *

def test_user_controller_get_user_by_name(mock_account):
    result = get_single_user(None, name='tester')
    assert result is not None
    assert result.account.id == mock_account

def test_user_controller_get_user_by_name_no_user(mock_account):
    result = get_single_user(None, name="example")
    assert result is None

def test_get_most_active_users(app, db_session, mock_account, mock_language, mock_phrase, mock_image):
    # Set the mock phrase to accepted so it adds to the activity score.
    with app.app_context():
        p = db_session.query(Phrase).get(mock_phrase)
        p.status = PhraseStatus.accepted
        db_session.add(p)
        db_session.commit()

    result = get_most_active_users()
    assert result is not None
    assert len(result) == 1

    first = result[0]
    assert first[0].account.id == mock_account
    assert first[1] == 2

def test_get_newest_users(mock_account):
    result = get_newest_users()
    assert result is not None
    assert len(result) == 1

    first = result[0]
    assert first.account.id == mock_account

def test_get_user_count(mock_account):
    result = get_user_count()
    assert result == 1
