import pytest

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

from pixeme.models import *
from pixeme.api.language import BLUEPRINT_PATH

def test_language_api_get_languages(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'languages' in json
    assert len(json['languages']) > 0
    assert json['languages'][0]['id'] == mock_language
    assert json['languages'][0]['primary'] == 'en'

def test_language_api_get_language_count(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/count')
    assert rv.status_code == 200

    json = rv.get_json()
    assert json == 1

def test_language_api_get_language_by_id(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_language))
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_language
    assert json['primary'] == 'en'

def test_language_api_get_language_by_id_bad_id(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_language+1))
    assert rv.status_code == 404

def test_language_api_get_language_by_tag(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/tag/en-US')
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_language
    assert json['primary'] == 'en'

def test_language_api_get_language_by_tag_insensitive(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/tag/en-us')
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_language
    assert json['primary'] == 'en'

def test_language_api_get_language_by_tag_bad_tag(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/tag/foo')
    assert rv.status_code == 404

def test_language_api_get_languages_by_group(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/group/en')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'languages' in json
    assert len(json['languages']) > 0
    assert json['languages'][0]['id'] == mock_language
    assert json['languages'][0]['primary'] == 'en'

def test_language_api_get_languages_by_group_bad_group(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/group/fr')
    assert rv.status_code == 404

def test_language_api_get_languages_by_group_conlang(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/group/qca')
    assert rv.status_code == 501

def test_language_api_get_language_groups(app, db_session, client, mock_language):
    # Add another language for contrast, and to test grouping.
    with app.app_context():
        other_language = Language(
            primary='de',
            tag='de',
            english="German",
            native="Deutsch"
        )

    db_session.add(other_language)
    db_session.commit()

    rv = client.get(BLUEPRINT_PATH + '/group/')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'groups' in json
    assert len(json['groups']) > 0
    assert 'en' in json['groups']

def test_language_api_create_language(client, mock_account, access_token, make_user_admin):
    rv = client.post(BLUEPRINT_PATH + '/',
        headers=[
            ("Authorization", "Bearer " + access_token)
        ],
        json={
            'tag': 'en-US',
            'english': 'English (US)',
            'native': 'US English'
        })
    assert rv.status_code == 201

    json = rv.get_json()
    assert 'id' in json
    assert json['primary'] == 'en'

def test_language_api_create_language_not_admin(client, mock_account, access_token):
    rv = client.post(BLUEPRINT_PATH + '/',
        headers=[
            ("Authorization", "Bearer " + access_token)
        ],
        json={
            'tag': 'en-US',
            'english': 'English (US)',
            'native': 'US English'
        })
    assert rv.status_code == 403

def test_language_api_create_language_no_native_name(client, mock_account, access_token, make_user_admin):
    rv = client.post(BLUEPRINT_PATH + '/',
        headers=[
            ("Authorization", "Bearer " + access_token)
        ],
        json={
            'tag': 'en-US',
            'english': 'English (US)'
        })
    assert rv.status_code == 201

    json = rv.get_json()
    assert 'id' in json
    assert json['native'] == json['english']

def test_language_api_update_language(
    app, db_session, client, mock_account, access_token, mock_language, make_user_admin):
    rv = client.put(BLUEPRINT_PATH + '/id/' + str(mock_language),
        headers=[
            ("Authorization", "Bearer " + access_token)
        ],
        json={
            'id': mock_language,
            'tag': 'en-US',
            'native': "American English"
        })
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'id' in json
    assert json['id'] == mock_language

    with app.app_context():
        language = db_session.query(Language).get(mock_language)
        assert language.native == "American English"

@skip_pytest_flask_sqlalchemy_delete
def test_language_api_delete_language(client, mock_account, access_token, mock_language, make_user_admin):
    # NOTE: This test currently fails due to a bug in Flask-Pytest-SQLAlchemy
    rv = client.delete(BLUEPRINT_PATH + '/id/' + str(mock_language),
        headers=[
            ("Authorization", "Bearer " + access_token)
        ])
    assert rv.status_code == 204

    with app.app_context():
        language = db_session.query(Language).get(mock_language)
        assert language is None
    
def test_language_api_get_most_popular(client, mock_account, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/popular')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'languages' in json
    assert json['languages'][0]['id'] == mock_language

def test_language_api_search(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/search?q=en')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'results' in json
    assert len(json['results']) == 1
    assert json['results'][0]['id'] == mock_language

def test_language_api_search_no_results(client):
    rv = client.get(BLUEPRINT_PATH + '/search?q=en')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'results' in json
    assert json['results'] == []