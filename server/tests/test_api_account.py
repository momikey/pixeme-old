from flask_jwt_extended import create_access_token

from pixeme.models import *
from pixeme.api.account import BLUEPRINT_PATH

def test_account_api_create(app, client, db_session):
    rv = client.post(BLUEPRINT_PATH + '/create', json={
        'username': 'tester',
        'email': 'test@example.com',
        'password': '123456'
    })

    assert rv.status_code == 201

    json_data = rv.get_json()
    assert json_data['username'] == 'tester'
    assert 'temp_token' in json_data

    with app.app_context():
        account = db_session.query(Account).filter_by(username=json_data['username']).first()
    
    assert account is not None
    assert account.email == 'test@example.com'
    assert account.check_password('123456') == True

def test_account_api_create_bad_name(client):
    rv = client.post(BLUEPRINT_PATH + '/create', json={
        'username': 'name_is_too_long' + ('$' * 50),
        'email': 'test@example.com',
        'password': '123456'
    })

    assert rv.status_code == 422

def test_account_api_create_no_email(client):
    rv = client.post(BLUEPRINT_PATH + '/create', json={
        'username': 'tester',
        'password': '123456'
    })

    assert rv.status_code == 422

def test_account_api_create_bad_password(client):
    rv = client.post(BLUEPRINT_PATH + '/create', json={
        'username': 'tester',
        'email': 'test@example.com',
        'password': '12345'     # Too short
    })

    assert rv.status_code == 422

def test_account_api_create_user_already_exists(client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/create', json={
        'username': 'tester',
        'email': 'anothertester@example.com',
        'password': '123456'
    })

    assert rv.status_code == 400

def test_account_api_create_duplicate_email(client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/create', json={
        'username': 'another_tester',
        'email': 'test@example.com',
        'password': '123456'
    })

    assert rv.status_code == 400

def test_account_api_login(client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/login', json={
        'username': 'tester',
        'password': '123456'
    })

    assert rv.status_code == 200

    json_data = rv.get_json()
    assert json_data['role'] == AccountRole.user.name
    assert 'access_token' in json_data
    assert 'refresh_token' in json_data

def test_account_api_login_bad_username(client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/login', json={
        'username': 'badname',
        'password': '123456'
    })
    
    assert rv.status_code == 403

def test_account_api_login_bad_password(client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/login', json={
        'username': 'tester',
        'password': 'badpass'
    })

    assert rv.status_code == 403

def test_account_api_logout(client, db_session, access_token):
    rv = client.post(BLUEPRINT_PATH + '/logout', headers=[
        ('Authorization', 'Bearer ' + access_token)
    ])

    assert rv.status_code == 200
    
    json_data = rv.get_json()
    assert json_data['logout'] == 'tester'

def test_account_api_logout_not_logged_in(client, db_session):
    token = create_access_token('tester')

    rv = client.post(BLUEPRINT_PATH + '/logout', headers=[
        ('Authorization', 'Bearer ' + token)
    ])

    assert rv.status_code == 403

def test_account_api_refresh(client, db_session, refresh_token):
    rv = client.post(BLUEPRINT_PATH + '/refresh', headers=[
        ('Authorization', 'Bearer ' + refresh_token)
    ])

    assert rv.status_code == 200
    
    json_data = rv.get_json()
    assert 'access_token' in json_data

def test_account_api_change_role(app, client, db_session, mock_account, access_token, make_user_admin):
    rv = client.put(BLUEPRINT_PATH + '/name/tester/role', headers=[
        ('Authorization', 'Bearer ' + access_token)
    ], json={
        'admin_id': mock_account,
        'user_id': mock_account,
        'new_role': 'moderator'
    })
    assert rv.status_code == 204

    with app.app_context():
        user = db_session.query(Account).get(mock_account)
        assert user.role == AccountRole.moderator

def test_account_api_ban_user(app, client, db_session, mock_account, access_token, make_user_admin):
    rv = client.post(BLUEPRINT_PATH + '/name/tester/ban', headers=[
        ('Authorization', 'Bearer ' + access_token)
    ], json={
        'admin_id': mock_account,
        'user_id': mock_account
    })
    assert rv.status_code == 204

    with app.app_context():
        user = db_session.query(Account).get(mock_account)
        assert user.role == AccountRole.banned

def test_account_api_unban_user(app, client, db_session, mock_account, access_token, make_user_admin):
    rv = client.post(BLUEPRINT_PATH + '/name/tester/unban', headers=[
        ('Authorization', 'Bearer ' + access_token)
    ], json={
        'admin_id': mock_account,
        'user_id': mock_account
    })
    assert rv.status_code == 204

    with app.app_context():
        user = db_session.query(Account).get(mock_account)
        assert user.role == AccountRole.user

def test_account_api_account_exists(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/exists/tester')
    assert rv.status_code == 200
    
    json_data = rv.get_json()
    assert json_data == True

def test_account_api_account_does_not_exist(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/exists/sample')
    assert rv.status_code == 200

    json_data = rv.get_json()
    assert json_data == False

def test_account_api_change_password(app, db_session, client, mock_account, access_token):
    new_password = '654321'

    rv = client.put(BLUEPRINT_PATH + '/name/tester/password', headers=[
        ('Authorization', 'Bearer ' + access_token)
    ], json={
        'password': new_password
    })
    assert rv.status_code == 204

    with app.app_context():
        user = db_session.query(Account).get(mock_account)
        assert user.check_password(new_password)