import pytest

from pixeme.models import *
from pixeme.api.user import BLUEPRINT_PATH

@pytest.fixture
def mock_ban(app, db_session, mock_account):
    """Set the mock user's account to banned, to test APIs."""
    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        account.role = AccountRole.banned
        db_session.add(account)
        db_session.commit()

    return mock_account

def test_user_api_get_users(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'users' in json
    assert len(json['users']) == 1
    assert json['users'][0]['id'] == mock_account

def test_user_api_get_user_by_id(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_account))
    assert rv.status_code == 200

    json = rv.get_json()

    assert json['account']['id'] == mock_account
    assert json['name'] == "Test user"

def test_user_api_get_user_by_id_bad_id(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_account + 1))
    assert rv.status_code == 404

def test_user_api_get_user_by_id_banned_user(client, mock_account, mock_ban):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_account))
    assert rv.status_code == 403

def test_user_api_get_new_users(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/new')
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'users' in json
    assert len(json['users']) == 1
    assert json['users'][0]['id'] == mock_account

def test_user_api_get_most_active_users(client, mock_account, mock_phrase, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/active')
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'users' in json
    assert len(json['users']) == 1
    assert json['users'][0]['id'] == mock_account

def test_user_api_get_user_count(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/count')
    assert rv.status_code == 200

    json = rv.get_json()

    assert json == 1

def test_user_api_change_display_name(client, mock_account, access_token):
    rv = client.put(BLUEPRINT_PATH + '/id/{id}/name'.format(id=mock_account),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'user_id': mock_account,
            'name': "New name"
        }
    )
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'user' in json
    assert json['user']['id'] == mock_account
    assert json['user']['name'] == "New name"

def test_user_api_change_display_name_bad_user(client, mock_account, access_token):
    rv = client.put(BLUEPRINT_PATH + '/id/{id}/name'.format(id=mock_account),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'user_id': mock_account+1,
            'name': "New name"
        }
    )
    assert rv.status_code >= 400

def test_user_api_change_fluencies(client, mock_account, mock_language, access_token):
    rv = client.put(BLUEPRINT_PATH + '/id/{id}/fluencies'.format(id=mock_account),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'user_id': mock_account,
            'fluencies': [mock_language]
        }
    )
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'user' in json
    assert json['user']['id'] == mock_account
    assert mock_language in [f['id'] for f in json['user']['fluencies']]

def test_user_api_update_profile(client, mock_account, access_token):
    rv = client.put(BLUEPRINT_PATH + '/id/{id}/profile'.format(id=mock_account),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'user_id': mock_account,
            'profile': {
                'test_data': 'foo'
            }
        }
    )
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'user' in json
    assert json['user']['id'] == mock_account
    assert json['user']['profile']['test_data'] == 'foo'

def test_user_api_update_name_and_profile(client, mock_account, access_token):
    rv = client.put(BLUEPRINT_PATH + '/id/{id}/name-and-profile'.format(id=mock_account),
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'user_id': mock_account,
            'name': "New user name",
            'profile': {
                'test_data': 'foo'
            }
        }
    )
    assert rv.status_code == 200

    json = rv.get_json()

    assert 'user' in json
    assert json['user']['id'] == mock_account
    assert json['user']['name'] == "New user name"
    assert json['user']['profile']['test_data'] == 'foo'