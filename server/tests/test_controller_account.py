from pixeme.models import *
from pixeme.controllers.account import *

def test_create_account_record(app, db_session, mock_account_data, mock_account):
    assert mock_account is not None

    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        json = AccountSchema().dump(account)
        user_json = UserSchema().dump(account.user)

    assert account is not None
    assert account.username == mock_account_data['username']
    assert account.email == mock_account_data['email']
    assert account.check_password(mock_account_data['password']) == True

    assert json['username'] == account.username
    assert 'password' not in json

    assert 'account' in user_json
    assert user_json['account'] == json

def test_account_change_password(app, db_session, mock_account, mock_account_data):
    new_pw = "654321"

    result = change_user_password(mock_account, mock_account_data['password'], new_pw)

    assert result is True

    with app.app_context():
        a = db_session.query(Account).get(mock_account)

        assert a.check_password(new_pw) is True
