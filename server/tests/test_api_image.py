import pytest

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

from pixeme.models import *
from pixeme.api.image import BLUEPRINT_PATH
from pixeme.controllers.image import path_for, image_url

def test_image_url(app):
    filename = 'test.jpg'

    assert image_url(filename) == 'http://localhost:5000/media/test.jpg'

def test_path_for(app, db_session, mock_image):
    with app.app_context():
        image = db_session.query(Image).get(mock_image)

        path = path_for(image)

    assert path.endswith('/media/tester/test.jpg')

def test_image_api_get_image_by_id(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image))
    assert rv.status_code == 200

    json = rv.get_json()
    assert json['id'] == mock_image
    assert json['filename'] == 'test.jpg'

def test_image_api_get_image_by_id_bad_id(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image+1))
    assert rv.status_code == 404

def test_image_api_get_image_phrases(client, mock_image, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image) + '/phrases')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'phrases' in json
    assert len(json['phrases']) > 0
    assert json['phrases'][0]['id'] == mock_phrase

def test_image_api_get_image_examples(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image) + '/examples')
    assert rv.status_code == 200

    json = rv.get_json()
    assert 'examples' in json
    assert json['examples'] == []

def test_image_api_update_image_metadata(client, mock_image, access_token):
    put = client.put(BLUEPRINT_PATH + '/id/' + str(mock_image) + '/metadata',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'image_id': mock_image,
            'metadata': {
                'foo': 'bar'
            }
        }
    )
    assert put.status_code == 200

    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image))
    json = rv.get_json()
    assert 'image_metadata' in json
    assert json['image_metadata']['foo'] == 'bar'

def test_image_api_add_image_phrase(client, mock_image, mock_language, access_token):
    post = client.post(BLUEPRINT_PATH + '/id/' + str(mock_image) + '/phrases',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'image_id': mock_image,
            'language_id': mock_language,
            'text': "A test phrase"
        }
    )
    assert post.status_code == 200

    json = post.get_json()
    assert mock_image in json['images']
    phrase_id = json['id']

    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image))
    json = rv.get_json()
    assert phrase_id in json['phrases']

def test_image_api_add_image_tags(client, mock_image, access_token):
    post = client.post(BLUEPRINT_PATH + '/id/' + str(mock_image) + '/tags',
        headers=[
            ('Authorization', 'Bearer ' + access_token)
        ],
        json={
            'image_id': mock_image,
            'tags': ['foo', 'bar']
        }
    )
    assert post.status_code == 200

    json = post.get_json()
    assert len(json['tags']) > 0
    assert json['tags'][0]['text'] == 'foo'

def test_image_api_get_images_with_tag(client, mock_image, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/tag/' + 'test image')
    assert rv.status_code == 200

    json = rv.get_json()
    assert len(json['images']) > 0
    assert json['images'][0]['id'] == mock_image

def test_image_api_get_newest_images(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/new')
    assert rv.status_code == 200

    json = rv.get_json()
    assert len(json['images']) > 0
    assert json['images'][0]['id'] == mock_image

def test_image_api_get_random_images(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/random')
    assert rv.status_code == 200

    json = rv.get_json()
    assert len(json['images']) > 0
    assert json['images'][0]['id'] == mock_image

def test_image_api_remove_tag_from_image(app, db_session, client, mock_account, mock_image,
    mock_tag, access_token, make_user_admin):
    rv = client.delete("{path}/id/{id}/tags/{tag}".format(
        path=BLUEPRINT_PATH, id=mock_image, tag='test image'),
        headers=[
        ('Authorization', 'Bearer ' + access_token)
        ])
    assert rv.status_code == 204

    with app.app_context():
        image = db_session.query(Image).get(mock_image)
        assert mock_tag not in [t.id for t in image.tags]

def test_image_api_remove_tag_from_image_no_tag(app, db_session, client, mock_account, mock_image,
    access_token, make_user_admin):
    rv = client.delete("{path}/id/{id}/tags/{tag}".format(
        path=BLUEPRINT_PATH, id=mock_image, tag='test image'),
        headers=[
        ('Authorization', 'Bearer ' + access_token)
        ])
    assert rv.status_code == 204
