import pytest

from flask import session

from pixeme.models import *
from pixeme.views.language import BLUEPRINT_PATH

def test_language_view_get_by_id(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_language))
    assert rv.status_code == 200

    assert b"English (US) - US English" in rv.data

def test_language_view_get_by_id_bad_id(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_language + 1))
    assert rv.status_code == 404

def test_language_view_get_by_name(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/name/English (US)')
    assert rv.status_code != 404

def test_language_view_get_by_name_native(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/name/US English')
    assert rv.status_code != 404

def test_language_view_get_by_name_bad(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/name/English (UK)')
    assert rv.status_code == 404

def test_language_view_search_id(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/search?id=' + str(mock_language))
    assert rv.status_code < 400

def test_language_view_search_name(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/search?name=US English')
    assert rv.status_code < 400

def test_language_view_search_partial_name(app, client, db_session, mock_language):
    with app.app_context():
        other = Language(primary='en', tag='en-GB', english="English (UK)", native="British English")
        db_session.add(other)
        db_session.commit()

    rv = client.get(BLUEPRINT_PATH + '/search?name=English')
    assert rv.status_code < 400 or rv.status_code > 499

def test_language_view_search_no_data(client):
    rv = client.get(BLUEPRINT_PATH + '/search')
    assert rv.status_code == 400

def test_language_view_explore_languages(client, mock_account, mock_language, mock_phrase, make_user_speaker):
    rv = client.get(BLUEPRINT_PATH + '/')
    data = rv.get_data(as_text=True)

    assert 'English (US)' in data
    assert '1 speaker' in data
    assert '1 phrase' in data
