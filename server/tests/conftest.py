import os
import pytest
import sqlalchemy
import logging
from PIL import Image

import flask_migrate
from flask import Flask, current_app, g, session
from flask_jwt_extended import create_access_token, create_refresh_token
from flask_sqlalchemy import SQLAlchemy
from pytest_postgresql.factories import DatabaseJanitor

from pixeme import create_app
from pixeme.models import *

DB_USERNAME = 'postgres'
DB_PASSWORD = 'postgres'
DB_NAME = 'pixeme_testing'
SQLALCHEMY_DATABASE_URI = 'postgresql://{username}:{password}@localhost/{database}'.format(
    username=DB_USERNAME,
    password=DB_PASSWORD,
    database=DB_NAME
)

### Non-database fixtures ###

@pytest.fixture
def mock_pil_image():
    """Create a blank image for use in tests."""
    return Image.new('1', (256,256))

### Basic fixtures for a Flask app using SQLAlchemy ###

@pytest.fixture
def mock_flask_env(monkeypatch):
    """Set up an execution environment for our Flask app."""
    monkeypatch.setenv('FLASK_APP', 'testing')

@pytest.fixture(scope='session')
def database(request):
    """Set up the testing database."""
    janitor = DatabaseJanitor(DB_USERNAME, 'localhost', None, DB_NAME, '9.6', DB_PASSWORD)

    janitor.init()

    @request.addfinalizer
    def drop_database():
        janitor.drop()

@pytest.fixture(scope='session')
def app(database):
    """Set up the Flask app itself."""

    a = create_app()
    a.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI

    with a.test_request_context():
        ctx = a.app_context()
        ctx.push()
        yield a
        ctx.pop()

@pytest.fixture(scope='session')
def _db(app):
    """Create a mock SQLAlchemy instance."""

    db = SQLAlchemy(app=app)
    with app.app_context():
        flask_migrate.upgrade(directory='migrations')    
    return db

@pytest.fixture(scope='session')
def client(app):
    """Set up a Flask test client."""

    app.config['TESTING'] = True

    with app.test_client() as client:
        yield client

### App-specific fixtures ###

# For the database fixtures, we always return the ID column of the created record.
# This does mean that any unit test wanting to check DB values will need to depend
# on the `db_session` fixture, and will need to do extra queries, unfortunately.
# But returning the objects directly causes SQLAlchemy to get horribly confused,
# leading to "detached instance" errors that are pretty much intractable.

@pytest.fixture
def mock_account_data():
    """Return a dictionary containing the mock account data."""

    return {
        'username': 'tester',
        'password': '123456',
        'email': 'test@example.com'
    }

@pytest.fixture
def mock_account(app, db_session, mock_account_data):
    """Create and store a mock user account."""

    with app.app_context():
        account = Account(
            username=mock_account_data['username'],
            email=mock_account_data['email']
        )
        account.set_password(mock_account_data['password'])

        user = User(
            name="Test user",
            account=account
        )

        db_session.add_all((account, user))
        db_session.commit()

    return account.id

@pytest.fixture
def mock_language(app, db_session):
    """Create and store a mock language record."""

    with app.app_context():
        language = Language(
            primary='en',
            tag='en-US',
            english="English (US)",
            native="US English"
        )

        db_session.add(language)
        db_session.commit()

    return language.id

@pytest.fixture
def mock_phrase(app, db_session, mock_account, mock_language):
    """Create and store a mock phrase record."""

    with app.app_context():
        phrase = Phrase(
            text="test",
            language_id=mock_language,
            submitter_id=mock_account
        )

        db_session.add(phrase)
        db_session.commit()

    return phrase.id

@pytest.fixture
def mock_example(app, db_session, mock_phrase, mock_language):
    """Create and store a mock example translation."""

    with app.app_context():
        example = Example(
            source_text="This is a test.",
            target_text="It's a test, you see.",
            phrase_id=mock_phrase,
            target_language_id=mock_language
        )

        db_session.add(example)
        db_session.commit()

    return example.id

@pytest.fixture
def mock_image(app, db_session, mock_account, mock_language, mock_phrase):
    """Create and store a mock image record."""

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)

        image = Image(
            filename='test.jpg',
            caption='A test image',
            uploader_id=mock_account,
            phrases=[phrase]
        )

        db_session.add(image)
        db_session.commit()

    return image.id

@pytest.fixture
def mock_tag(app, db_session, mock_image):
    """Create and store a mock tag record."""

    with app.app_context():
        image = db_session.query(Image).get(mock_image)

        tag = Tag(
            text='test image',
            images=[image]
        )

        db_session.add(tag)
        db_session.commit()

    return tag.id

@pytest.fixture
def mock_notification(app, db_session, mock_account):
    """Create and store a mock notification."""

    with app.app_context():
        account = db_session.query(Account).get(mock_account)

        notification = Notification(
            text="Test notification",
            account=account
        )

        db_session.add(notification)
        db_session.commit()

    return notification.id

### Auth fixtures ###

@pytest.fixture
def access_token(app, mock_account):
    """Create a test JWT authentication token."""
    return create_access_token('tester')

@pytest.fixture
def refresh_token(app, mock_account):
    """Create a test JWT refresh token."""
    yield create_refresh_token('tester')

@pytest.fixture
def make_user_admin(app, db_session, mock_account):
    # Set the mock account to an admin for creation.
    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        account.role = AccountRole.admin
        db_session.add(account)
        db_session.commit()

    return mock_account

@pytest.fixture
def make_user_speaker(app, db_session, mock_account, mock_language):
    # Set the mock account to be a speaker of the mock language
    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        lang = db_session.query(Language).get(mock_language)
        account.user.fluencies.append(lang)
        db_session.add(account)
        db_session.commit()

@pytest.fixture
def session_login(app, client, db_session, mock_account):
    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        user = account.user

    with client.session_transaction() as session:
        session['account'] = AccountSchema().dump(account)
        session['user'] = UserSchema().dump(user)
