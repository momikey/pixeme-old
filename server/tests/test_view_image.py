import pytest

from flask import session

from pixeme.models import *
from pixeme.views.image import BLUEPRINT_PATH

def test_image_view_get_image(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image))
    assert rv.status_code == 200

    assert b'imageView' in rv.data
    assert b'<figcaption>A test image</figcaption>' in rv.data
    assert b'x-text=' in rv.data

def test_image_view_get_image_bad_id(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_image+1))
    assert rv.status_code == 404

def test_image_view_explore_images(client, mock_image, mock_phrase, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    assert b'<figcaption>A test image</figcaption>' in rv.data