import pytest
import re, pprint

from flask import session

from pixeme.models import *
from pixeme.views.phrase import BLUEPRINT_PATH

def test_phrase_view_get_by_id(client, mock_phrase, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_phrase))
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert re.search(r"<h1.*test\s*</h1>", data, re.S)
    assert "English (US) - US English" in data
    assert re.search(r"Submitted by.*Test user</a>", data, re.S)
    assert "1 image" in data
    assert "<template x-for" in data

def test_phrase_view_get_by_id_bad_id(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/id/' + str(mock_phrase + 1))
    assert rv.status_code == 404
