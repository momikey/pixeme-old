import pytest
from flask import session

from pixeme.models import *

def test_account_view_get_login(client):
    rv = client.get('/login')
    assert rv.status_code == 200

    assert b'Login</header>' in rv.data

def test_account_view_handle_login(app, client, mock_account):
    rv = client.post('/login', data={
        'username': 'tester',
        'password': '123456'
    })
    assert rv.status_code < 400

    # Now get the home page to confirm that the user is logged in.
    rv = client.get('/')
    assert 'account' in session
    assert session['account']['id'] == mock_account
    assert b'Hello, ' in rv.data
    assert b'Logout' in rv.data

def test_account_view_handle_login_bad_username(app, client, mock_account):
    rv = client.post('/login', data={
        'username': 'nottester',
        'password': '123456'
    })
    assert rv.status_code != 200

    flash = session['_flashes']
    assert "Invalid username or password", 'error' in flash

def test_account_view_handle_login_bad_password(app, client, mock_account):
    rv = client.post('/login', data={
        'username': 'tester',
        'password': '654321'
    })
    assert rv.status_code != 200

    flash = session['_flashes']
    assert "Invalid username or password", 'error' in flash

def test_account_view_handle_login_no_password(app, client, mock_account):
    rv = client.post('/login', data={
        'username': 'tester'
    })
    assert rv.status_code != 200

    flash = session['_flashes']
    assert len(flash) > 0
    
def test_account_view_handle_logout(app, client, mock_account):
    # Log in first, to get the session stuff set up.
    login = client.post('/login', data={
        'username': 'tester',
        'password': '123456'
    })
    assert login.status_code < 400

    # Logging out requires no POST body.
    rv = client.post('/logout')
    assert rv.status_code < 400
    flash = session['_flashes']
    assert "You have been logged out.",'message' in flash

    # Retrieve a page to check that the user is logged out.
    rv = client.get('/')
    assert 'account' not in session
    assert b'Login' in rv.data

def test_account_create(client):
    rv = client.get('/create-account')
    assert rv.status_code == 200

    assert b'<title>Create account' in rv.data

def test_account_create_fluencies(client, mock_language):
    rv = client.get('/create-account')
    assert rv.status_code == 200

    assert 'value="{}"'.format(mock_language) in rv.get_data(as_text=True)

def test_account_handle_create_account(app, client, db_session, mock_language):
    rv = client.post('/create-account', data={
        'username': 'tester',
        'email': 'test@example.com',
        'password': '123456',
        'retype': '123456',
        'name': "Test user",
        'tosAccept': "true",
        'fluencies': [mock_language]
    })
    assert rv.status_code == 200

    assert b'Welcome to Pixeme' in rv.data

    with app.app_context():
        account = db_session.query(Account).filter_by(username='tester').first()

        assert account is not None
        assert account.email == 'test@example.com'
        assert account.user is not None
        assert account.user.name == "Test user"
        assert mock_language in [f.id for f in account.user.fluencies]

def test_account_handle_create_account_username_exists(app, client, db_session, mock_account):
    rv = client.post('/create-account', data={
        'username': 'tester',
        'email': 'nobody@localhost.invalid',
        'password': '654321'
    })
    assert rv.status_code != 200

    with client.session_transaction() as session:
        flash = session['_flashes']
        assert "A user with this name already exists", 'error' in flash
