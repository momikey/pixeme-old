# Tests pertaining to database models

from pixeme.models import *

def test_user_model(app, db_session, mock_account):
    with app.app_context():
        user = db_session.query(Account).get(mock_account).user

    assert user.account.id == mock_account

def test_user_repr(app, db_session, mock_account):
    with app.app_context():
        user = db_session.query(Account).get(mock_account).user

    assert str(user) == '<User Test user (tester)>'

def test_language_model(app, db_session, mock_language):
    with app.app_context():
        language = db_session.query(Language).get(mock_language)
        json = LanguageSchema().dump(language)

    assert language.primary == 'en'
    assert language.tag == 'en-US'

    assert json['primary'] == 'en'
    assert json['tag'] == 'en-US'
    assert 'speakers' not in json

def test_language_repr(app, db_session, mock_language):
    with app.app_context():
        language = db_session.query(Language).get(mock_language)

    assert str(language) == '<Language English (US) | US English (en-US)>'

def test_user_fluency_relation(app, db_session, mock_account, mock_language):
    with app.app_context():
        user = db_session.query(Account).get(mock_account).user
        language = db_session.query(Language).get(mock_language)

        user.fluencies.append(language)
        db_session.add(user)
        db_session.commit()

    assert user in language.speakers

def test_phrase_model(app, db_session, mock_account, mock_language, mock_phrase):
    with app.app_context():
        user = db_session.query(Account).get(mock_account).user
        phrase = db_session.query(Phrase).get(mock_phrase)
        json = PhraseSchema().dump(phrase)

    assert phrase.status == PhraseStatus.proposed
    assert json['submitter']['id'] == user.id

def test_phrase_repr(app, db_session, mock_phrase):
    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)

    assert str(phrase) == '<Phrase "test" (proposed)>'

def test_example_model(app, db_session, mock_phrase, mock_language, mock_example):
    with app.app_context():
        example = db_session.query(Example).get(mock_example)
        json = ExampleSchema().dump(example)

    assert example.target_text == "It's a test, you see."
    assert json['phrase']['id'] == mock_phrase
    assert json['target_language']['id'] == mock_language

def test_example_repr(app, db_session, mock_example):
    with app.app_context():
        example = db_session.query(Example).get(mock_example)

    assert str(example) == """<Translation of "This is a test." to English (US)>"""

def test_image_model(app, db_session, mock_image):
    with app.app_context():
        image = db_session.query(Image).get(mock_image)
        json = ImageSchema().dump(image)

    assert image.filename == 'test.jpg'
    assert len(json['phrases']) == 1

def test_image_repr(app, db_session, mock_image):
    with app.app_context():
        image = db_session.query(Image).get(mock_image)
    
    assert str(image) == '<Image test.jpg (A test image)>'

def test_tag_model(app, db_session, mock_image, mock_tag):
    with app.app_context():
        image = db_session.query(Image).get(mock_image)
        image_json = ImageSchema().dump(image)

        tag = db_session.query(Tag).get(mock_tag)
        tag_json = TagSchema().dump(tag)

    assert len(tag.images) == 1
    assert tag.text == 'test image'
    assert image_json['tags'][0] == tag_json

def test_tag_repr(app, db_session, mock_tag):
    with app.app_context():
        tag = db_session.query(Tag).get(mock_tag)

    assert str(tag) == '<Tag "test image">'

def test_notification_model(app, db_session, mock_account, mock_notification):
    with app.app_context():
        notification = db_session.query(Notification).get(mock_notification)
        json = NotificationSchema().dump(notification)

    assert notification.account.id == mock_account
    assert notification.text == "Test notification"
    assert json['account'] == mock_account

def test_notification_repr(app, db_session, mock_notification):
    with app.app_context():
        notification = db_session.query(Notification).get(mock_notification)

    assert str(notification) == '<Notification "Test notif..." for tester>'

