# We need to skip a few tests until certain dependencies are fixed.

import pkg_resources
import pytest
import pytest_flask_sqlalchemy

# This is derived from SO, so be wary.
def get_version(package):
    package = package.lower()
    version_string = next(
        (p.version for p in pkg_resources.working_set if p.project_name.lower() == package),
        "No match"
    )

    version_numbers = [int(n) for n in version_string.split('.')] if version_string != 'No match' else []

    return version_string, version_numbers

def pytest_flask_sqlalchemy_is_bad():
    version_string, version_numbers = get_version('pytest-flask-sqlalchemy')

    if version_numbers[0] > 1 or (version_numbers[0] == 1 and version_numbers[1] > 0):
        return False
    else:
        return version_numbers[2] <= 2

skip_pytest_flask_sqlalchemy_delete = pytest.mark.skipif(
    pytest_flask_sqlalchemy_is_bad(),
    reason="Skipping because delete operations fail with this Pytest-Flask-SQLAlchemy version"
)