import pytest

import re

from pixeme.models import *
from pixeme.views.admin import BLUEPRINT_PATH

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

# All admin views are login-protected, but we can set up a module-level
# fixture set that will be used for each test. This encapsulates the
# authentication parts.
pytestmark = pytest.mark.usefixtures('make_user_admin', 'session_login')

def test_admin_view_home(client):
    rv = client.get(BLUEPRINT_PATH + '/')
    assert rv.status_code == 200

    assert b'Welcome' in rv.data

### Users and accounts ###

# NOTE: We had a "no users" test here before. That literally can't happen
# now that the admin dashboard requires login, so it has been removed.

def test_admin_view_users(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/users')
    assert rv.status_code == 200

    assert '<td class="admin-table-cell">{id}</td>'.format(id=mock_account) in rv.get_data(as_text=True)

def test_admin_view_user_details(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/users/' + str(mock_account))
    assert rv.status_code == 200

    assert 'value="{id}'.format(id=mock_account) in rv.get_data(as_text=True)

def test_admin_view_user_details_bad_user(client, mock_account):
    rv = client.get(BLUEPRINT_PATH + '/users/' + str(mock_account+1))
    assert rv.status_code == 404

def test_admin_view_confirm_delete_user(client, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/users/' + str(mock_account) + '/confirm-delete')
    assert rv.status_code == 200

    assert "Are you sure you want to delete the user" in rv.get_data(as_text=True)

@skip_pytest_flask_sqlalchemy_delete
def test_admin_view_delete_user(app, client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/users/' + str(mock_account) + '/delete',
        json={}
    )
    assert rv.status_code == 302

    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        assert account is None

def test_admin_view_confirm_ban_user(client, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/users/' + str(mock_account) + '/confirm-ban')
    assert rv.status_code == 200

    assert "Are you sure you want to ban the user" in rv.get_data(as_text=True)

def test_admin_view_ban_user(app, client, db_session, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/users/' + str(mock_account) + '/ban',
        json={}
    )
    assert rv.status_code == 302

    with app.app_context():
        account = db_session.query(Account).get(mock_account)
        assert account.role == AccountRole.banned

def test_admin_view_start_create_account(client):
    rv = client.get(BLUEPRINT_PATH + '/users/create')
    assert rv.status_code == 200

    assert "Create New Account" in rv.get_data(as_text=True)

def test_admin_view_create_account(app, client, db_session):
    rv = client.post(BLUEPRINT_PATH + '/users/create', data={
        'username': 'tester2',
        'email': 'test2@example.com',
        'password': '123456'
    })
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert "Account Created" in data
    assert "Username:" in data
    assert "Email:" in data
    assert "Password: (not shown)" in data
    assert "tester2" in data
    assert "test2@example.com" in data

    with app.app_context():
        account = db_session.query(Account).filter_by(username='tester2')
        assert account is not None

def test_admin_view_create_account_username_exists(client, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/users/create', data={
        'username': 'tester',
        'email': 'someone@example.com',
        'password': '123456'
    })
    assert rv.status_code == 302

def test_admin_view_create_account_email_exists(client, mock_account):
    rv = client.post(BLUEPRINT_PATH + '/users/create', data={
        'username': 'tester',
        'email': 'someone@example.com',
        'password': '123456'
    })
    assert rv.status_code == 302
    
    with client.session_transaction() as session:
        flash = session['_flashes']
        assert 'Something went wrong','error' in flash

### Languages ###

def test_admin_view_languages_no_language(client):
    rv = client.get(BLUEPRINT_PATH + '/languages')
    assert rv.status_code == 200

    assert b"No languages found" in rv.data

def test_admin_view_languages(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/languages')
    assert rv.status_code == 200

    assert '<td class="admin-table-cell">{id}</td>'.format(id=mock_language) in rv.get_data(as_text=True)

def test_admin_view_language_details(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/languages/' + str(mock_language))
    assert rv.status_code == 200

    assert 'value="{id}'.format(id=mock_language) in rv.get_data(as_text=True)

def test_admin_view_language_details_bad_language(client, mock_language):
    rv = client.get(BLUEPRINT_PATH + '/languages/' + str(mock_language+1))
    assert rv.status_code == 404

def test_admin_view_confirm_delete_user(client, mock_language):
    rv = client.post(BLUEPRINT_PATH + '/languages/' + str(mock_language) + '/confirm-remove')
    assert rv.status_code == 200

    assert "This will remove all phrases, translations, and tags" in rv.get_data(as_text=True)

@skip_pytest_flask_sqlalchemy_delete
def test_admin_view_languages_remove_language(client, mock_language):
    rv = client.post(BLUEPRINT_PATH + '/languages/' + str(mock_language) + '/remove',
        json = {}
    )
    assert rv.status_code == 302

    with app.app_context():
        language = db_session.query(Language).get(mock_language)
        assert language is None

def test_admin_view_languages_start_add_language(client):
    rv = client.get(BLUEPRINT_PATH + '/languages/add')
    assert rv.status_code == 200

    assert b'Add Language' in rv.data

def test_admin_view_languages_add_language(app, db_session, client):
    rv = client.post(BLUEPRINT_PATH + '/languages/add', data={
        'tag': 'en-US',
        'english': "English (US)",
        'native': "US English"
    })
    assert rv.status_code < 400

    with client.session_transaction() as session:
        flash = session['_flashes']
        assert 'Added English (US) language','message' in flash

    with app.app_context():
        language = db_session.query(Language).filter_by(tag='en-US')
        assert language is not None

def test_admin_view_languages_add_language_already_exists(client, mock_language):
    rv = client.post(BLUEPRINT_PATH + '/languages/add', data={
        'tag': 'en-US',
        'english': "English (US)",
        'native': "US English"
    })
    assert rv.status_code < 400

    with client.session_transaction() as session:
        flash = session['_flashes']
        assert 'A language with this code already exists','error' in flash

### Images

def test_admin_view_images_no_image(client):
    rv = client.get(BLUEPRINT_PATH + '/images')
    assert rv.status_code == 200

    assert b"No images found" in rv.data

def test_admin_view_images(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/images')
    assert rv.status_code == 200

    assert '<td class="admin-table-cell">{id}</td>'.format(id=mock_image) in rv.get_data(as_text=True)

def test_admin_view_image_details(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/images/' + str(mock_image))
    assert rv.status_code == 200

    assert b'<img src=' in rv.data
    assert 'value="{id}"'.format(id=mock_image) in rv.get_data(as_text=True)
    assert b'value="A test image"' in rv.data
    assert b'1 phrase' in rv.data
    assert b'Uploaded by Test user' in rv.data

def test_admin_view_image_details_bad_image(client, mock_image):
    rv = client.get(BLUEPRINT_PATH + '/images/' + str(mock_image+1))
    assert rv.status_code == 404

@skip_pytest_flask_sqlalchemy_delete
def test_admin_view_images_remove_image(client, mock_image):
    rv = client.post(BLUEPRINT_PATH + '/images/' + str(mock_image) + '/remove',
        json = {}
    )
    assert rv.status_code == 302

    with app.app_context():
        image = db_session.query(Image).get(mock_image)
        assert image is None

def test_admin_view_images_remove_image_bad_image(client, mock_image):
    rv = client.post(BLUEPRINT_PATH + '/images/' + str(mock_image+1) + '/remove',
        json = {}
    )

    assert rv.status_code == 404

### Phrases

def test_admin_view_phrases_no_phrase(client):
    rv = client.get(BLUEPRINT_PATH + '/phrases')
    assert rv.status_code == 200

    assert b"No phrases found" in rv.data

def test_admin_view_phrases(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/phrases')
    assert rv.status_code == 200

    assert '<td class="admin-table-cell">{id}</td>'.format(id=mock_phrase) in rv.get_data(as_text=True)

def test_admin_view_phrase_details(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/phrases/' + str(mock_phrase))
    assert rv.status_code == 200

    data = rv.get_data(as_text=True)

    assert 'value="{id}"'.format(id=mock_phrase) in data
    assert re.search(r"<option.*proposed.*selected.*>", data, re.S)
    assert 'value="test"' in data

def test_admin_view_phrase_details_bad_phrase(client, mock_phrase):
    rv = client.get(BLUEPRINT_PATH + '/phrases/' + str(mock_phrase + 1))
    assert rv.status_code == 404

@skip_pytest_flask_sqlalchemy_delete
def test_admin_view_phrases_remove_phrase(client, mock_phrase):
    rv = client.post(BLUEPRINT_PATH + '/phrases/' + str(mock_phrase) + '/remove',
        json = {}
    )
    assert rv.status_code == 302

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        assert phrase is None

def test_admin_view_phrases_remove_phrase_bad_phrase(client, mock_phrase):
    rv = client.post(BLUEPRINT_PATH + '/phrases/' + str(mock_phrase+1) + '/remove',
        json = {}
    )

    assert rv.status_code == 404

### Tags

def test_admin_view_tags_no_tag(client):
    rv = client.get(BLUEPRINT_PATH + '/tags')
    assert rv.status_code == 200

    assert b"No tags found" in rv.data

def test_admin_view_tags(client, mock_tag):
    rv = client.get(BLUEPRINT_PATH + '/tags')
    assert rv.status_code == 200

    assert '<td class="admin-table-cell">{id}</td>'.format(id=mock_tag) in rv.get_data(as_text=True)

def test_admin_view_tags_start_add_tag(client):
    rv = client.get(BLUEPRINT_PATH + '/tags/add')
    assert rv.status_code == 200

    assert b'Add Tag' in rv.data