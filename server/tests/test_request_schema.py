from pixeme.schema.request_schema import *

def test_validate_username_basic():
    name = 'tester'
    assert validate_username(name) == True

def test_validate_username_unicode():
    name = 'tëstεŕ_123_さま'
    assert validate_username(name) == True

def test_validate_username_too_short():
    name = 'p'
    assert validate_username(name) == False

def test_validate_username_too_long():
    name = 'x' * 65
    assert validate_username(name) == False

def test_validate_username_no_digit_at_start():
    name = '123abc'
    assert validate_username(name) == False

def test_validate_username_underscore_at_start():
    name = '_okay'
    assert validate_username(name) == True
