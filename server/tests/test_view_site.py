import pytest

from pixeme.models import *

def test_home(client):
    rv = client.get('/')
    assert rv.status_code == 200

    assert b"Most active users" in rv.data

def test_home_with_users(client, mock_account):
    rv = client.get('/')
    assert rv.status_code == 200

    assert b"Test user</a>" in rv.data
    assert b"1 user" in rv.data

def test_home_sidebar(client):
    rv = client.get('/')
    assert rv.status_code == 200

    assert b"<span>Pictures</span>" in rv.data

def test_home_sidebar_logged_in(client, mock_account):
    login = client.post('/login', data={
        'username': 'tester',
        'password': '123456'
    })

    rv = client.get('/')
    assert rv.status_code == 200

    assert b"<span>Profile</span>" in rv.data

def test_home_sidebar_admin(client, mock_account, make_user_admin):
    login = client.post('/login', data={
        'username': 'tester',
        'password': '123456'
    })

    rv = client.get('/')
    assert rv.status_code == 200

    assert b"<span>Admin</span>" in rv.data

def test_home_footer(client):
    rv = client.get('/')

    assert b"Terms" in rv.data
    assert b"Privacy" in rv.data
    assert b"Cookie" in rv.data

def test_home_explore_images(client, mock_image):
    rv = client.get('/')

    assert b"<figure" in rv.data
