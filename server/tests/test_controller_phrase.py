import pytest

from .skip_conditions import skip_pytest_flask_sqlalchemy_delete

from pixeme.models import *
from pixeme.controllers.phrase import *

def test_controller_get_phrase_by_id(mock_phrase):
    result = get_phrase_by_id(mock_phrase)
    assert result is not None

    assert result.text == 'test'

def test_controller_get_phrase_by_id_bad_id(mock_phrase):
    result = get_phrase_by_id(mock_phrase+1)
    assert result is None

def test_phrase_search_similar(mock_phrase):
    result = search_similar('test')
    assert mock_phrase in [r.id for r in result]

def test_phrase_search_similar_no_results(mock_phrase):
    bad_result = search_similar('xx')
    assert bad_result == []

def test_controller_get_phrases_list(mock_phrase):
    results = get_phrases_list()
    assert mock_phrase in [r.id for r in results]

def test_controller_get_phrases_list_by_language_id(mock_phrase, mock_language):
    results = get_phrases_list(language_id=mock_language)
    assert len(results) == 1
    assert results[0].id == mock_phrase

    # Now try with synonym.
    results = get_phrases_list(lang_id=mock_language)
    assert len(results) == 1
    assert results[0].id == mock_phrase

def test_controller_get_phrases_list_by_user(mock_phrase, mock_account):
    results = get_phrases_list(user_id=mock_account)
    assert len(results) == 1
    assert results[0].id == mock_phrase

def test_controller_get_phrases_list_by_status(app, db_session, mock_phrase):
    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        phrase.status = PhraseStatus.accepted
        db_session.add(phrase)
        db_session.commit()

    results = get_phrases_list(status='accepted')
    assert len(results) == 1
    assert results[0].id == mock_phrase

def test_controller_update_phrase(app, db_session, mock_phrase):
    result = update_phrase(mock_phrase, text="no", status=PhraseStatus.rejected)
    assert result is not None

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        assert phrase.text == "no"
        assert phrase.status == PhraseStatus.rejected

def test_controller_update_phrase_with_int(app, db_session, mock_phrase):
    result = update_phrase(mock_phrase, text="no", status=PhraseStatus.rejected.value)
    assert result is not None

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        assert phrase.text == "no"
        assert phrase.status == PhraseStatus.rejected

def test_controller_update_phrase(app, db_session, mock_phrase):
    result = update_phrase(mock_phrase, text="no", status=PhraseStatus.rejected.name)
    assert result is not None

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        assert phrase.text == "no"
        assert phrase.status == PhraseStatus.rejected

def test_controller_update_phrase_bad_phrase(mock_phrase):
    result = update_phrase(mock_phrase+1, status=PhraseStatus.accepted)
    assert result is None

def test_controller_change_phrase_status(app, db_session, mock_phrase):
    result = change_phrase_status(mock_phrase, PhraseStatus.accepted)
    assert result is not None

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        assert phrase.status == PhraseStatus.accepted

def test_controller_add_phrase(app, db_session, mock_account, mock_language):
    result = add_phrase(text="test", status='proposed', language_id=mock_language, submitter_id=mock_account)
    assert result is not None
    assert result.id is not None

    with app.app_context():
        phrase = db_session.query(Phrase).get(result.id)
        assert phrase.text == "test"

def test_controller_add_phrase_already_exists(app, db_session, mock_phrase, mock_language):
    result = add_phrase(text="test", status="rejected", language_id=mock_language)
    assert result is None

@skip_pytest_flask_sqlalchemy_delete
def test_controller_delete_phrase(app, db_session, mock_phrase):
    # Note: This currently fails due to a bug in Pytest-Flask-SQLAlchemy.
    result = delete_phrase(mock_phrase)
    assert result is not None

    with app.app_context():
        phrase = db_session.query(Phrase).get(mock_phrase)
        assert phrase is None
