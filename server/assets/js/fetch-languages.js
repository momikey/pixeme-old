/**
 * Simple function to fetch the languages list from the server.
 * This uses the REST API even if we're on the basic HTML site.
 * That's for async reasons, really; let it run in the background,
 * where it won't bother anything.
 * 
 * Note that this takes the API URL as a parameter. Since it'll
 * most likely only be used in templates, we can fill it in with
 * `url_for` there.
 */

 export default function fetchLanguages(url) {
    // Load the languages list into local storage for caching purposes.
    if (!localStorage.getItem('languages')) {
        fetch(url)
            .then(response => response.json())
            .then(data => localStorage.setItem('languages', JSON.stringify(data.languages)));
    }
}