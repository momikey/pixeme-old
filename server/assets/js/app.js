/* We're going to use Alpine pretty much everywhere. */
import 'alpinejs';

/*
 * The MDI icons are available as path strings, which is good.
 * They're in their own package, which is...not. Our shim module
 * takes care of that in a hackish way, copying the necessary
 * strings into an `mdiIcons` object on `window`.
 */
import mdiIconsShim from './mdi-icons-shim';
mdiIconsShim();

/*
 * Any other site-global functions will be defined as properties
 * of the Pixeme object. This is *only* for core functions that
 * would otherwise need to be copied everywhere.
 */
window.Pixeme = {}

/*
 * Add in everything we import. We do that manually here, rather
 * than inside the modules themselves, because...I don't know.
 * Maybe in case we want to do something else with them later?
 */
import fetchLanguages from './fetch-languages';
window.Pixeme.fetchLanguages = fetchLanguages;