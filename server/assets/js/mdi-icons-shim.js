/**
 * Quick and dirty shim to import specific MDI icon paths into
 * the window scope.
 * 
 * This is more work than just importing the webfont, but it allows
 * us to import only the icons we need, cutting down on network
 * usage. The problem is that Webpack encapsulates the `@mdi/js`
 * package containing path data, but we'd like it to be usable in
 * the browser.
 */

/*
 * Import individual icons. We probably won't use too many of them,
 * so we don't have to worry about maintaining this too much.
 */
import {
    mdiMenu,
    mdiClose,
    mdiEye,
    mdiEyeOff,
    mdiChevronUp,
    mdiChevronDown,
    mdiMinus,
    mdiPlus,
    mdiSend,
    mdiContentSave
} from '@mdi/js';

/*
 * Create a function that fires on DOM load to add our icons to a
 * window global. This isn't as nice as a module, but at least we
 * still get the benefits of treeshaking, etc., while being able
 * to use the icons in Alpine JS.
 */
const init = function () {
    window.mdiIcons = {
        mdiMenu,
        mdiClose,
        mdiEye,
        mdiEyeOff,
        mdiChevronUp,
        mdiChevronDown,
        mdiMinus,
        mdiPlus,
        mdiSend,
        mdiContentSave
    };
};

export default init;