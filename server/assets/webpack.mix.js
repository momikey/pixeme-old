const mix = require('laravel-mix');
const glob = require('glob-all');
require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

mix.setPublicPath('../pixeme/static/dist');

mix.browserSync({
    proxy: "http://localhost:5000",
    injectChanges: false,
    open: false,
    notify: false,
    files: [
        '../pixeme/static/dist/**/*.{css,js}',
        'templates/**/*.html'
    ]
});

mix.tailwind();

mix.js('./js/app.js', 'js');
mix.sass('./scss/app.scss', 'css');

if (mix.inProduction()) {
// No version hashing here, because the Flask extension will take care of it.
//    mix.versionHash();
    mix.sourceMaps();
}
