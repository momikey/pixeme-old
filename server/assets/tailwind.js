module.exports = {
    purge: [
        'templates/**/*.html',
        'js/**/*.js'
    ],
    theme: {
        extend: {
        }
    },
    variants: {
        cursor: ['responsive', 'disabled'],
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
        textColor: ['responsive', 'hover', 'focus', 'visited']
    },
    plugins: [
    ]
};
