# General utility functions that don't fit anywhere else.

from datetime import datetime
from simpleflake import simpleflake
from base64 import urlsafe_b64encode

from .models import db

def update_last_activity(account):
    """Update the "last activity" timestamp for an account. This must
    be called from within an app context, and before any calls to commit
    the SQLAlchemy database session."""

    account.last_activity = datetime.utcnow()
    db.session.add(account)

def printable_flake_id():
    """Generate a printable, URL-safe Flake ID suitable for a filename."""

    flake = simpleflake()
    return urlsafe_b64encode(flake.to_bytes(9, 'big')).decode()

def sample_alias(model, pct=5):
    """Create an aliased SQLAlchemy selectable using the Bernoulli sample
    method. This allows us to retrieve random rows in a way that doesn't
    hammer the DB.

    * `model` - The model class: one of those defined in `models.py`.
    * `pct` - The maximum percentage of results to return. (Default 5%.)
    """

    from sqlalchemy import func, tablesample
    from sqlalchemy.orm import aliased

    alias = aliased(model, tablesample(
        model,
        func.bernoulli(pct),
        seed=func.random()
    ))

    # We *could* put the query call and things like that here, but we leave
    # those for calling code. This is by analogy with the model classes.

    return alias