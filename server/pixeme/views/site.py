# Views for "site" pages that don't fit in any other category.
# This includes things like the front page, FAQ, privacy policy, etc.

from flask import (Blueprint, flash, g, redirect, render_template, 
    session, url_for)
from flask_babel import gettext

from pixeme.models import *
from pixeme.error import NotYetImplemented

import pixeme.controllers as controllers

# This view also has its own blueprint path.
BLUEPRINT_PATH = ''

blueprint = Blueprint('site_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/')
def home():
    """Show the Pixeme home page."""

    # This is a bit harder than it looks, because we want to get a lot
    # of "meta" site data to fill in the various dynamic parts of the
    # page. So we have to collect that here, then pass it to the view.

    user_count = controllers.user.get_user_count()
    active_users = controllers.user.get_most_active_users()
    new_users = controllers.user.get_newest_users()
    language_count = controllers.language.get_language_count()
    popular_languages = controllers.language.get_most_popular_languages()
    popular_tags = controllers.tag.get_most_popular_tags()
    sample_images = controllers.image.get_random_images_sample()

    return render_template(
        'views/front.html',
        user_count=user_count,
        active_users=active_users,
        new_users=new_users,
        language_count=language_count,
        popular_languages=popular_languages,
        popular_tags=popular_tags,
        sample_images=sample_images,

        get_image_url=controllers.image.image_url
    )

@blueprint.route('/about')
def about():
    """Show the About page, which briefly describes Pixeme as a platform."""

    return render_template('views/meta/about.html')

@blueprint.route('/help')
def help_base():
    """Show the index page for the Help & Support section."""

    raise NotYetImplemented()

@blueprint.route('/help/<slug>')
def help_article(slug):
    """Show the help article with the given URL "slug"."""

    raise NotYetImplemented()

@blueprint.route('/terms')
def tos():
    """Show the Pixeme Terms of Service."""

    return render_template('views/meta/tos.html')

@blueprint.route('/privacy')
def privacy_policy():
    """Show the Pixeme Privacy Policy."""

    return render_template('views/meta/privacy-policy.html')

@blueprint.route('/cookies')
def cookie_policy():
    """Show the Pixeme cookie policy and settings page. This is required by the EU,
    California, and other repressive regimes, but we're good guys, so we'll make
    any nonessential cookies opt-in whenever possible."""

    # TODO: Make a proper opt-in form and list of cookies.
    # At the moment, we don't actually use them, and we'll just link to the
    # privacy policy.
    return render_template('views/meta/privacy-policy.html')
