# Views for account-handling pages

from functools import wraps

from flask import (abort, Blueprint, current_app, flash, g, jsonify,
    redirect, request, render_template, session, url_for)
from flask_babel import gettext
from webargs.flaskparser import use_args

from pixeme.models import (Account, AccountRole, User,
    AccountSchema, UserSchema)
from pixeme.schema import request_schema
from pixeme.error import APIError, AdminOnly, NotYetImplemented, RequiresLogin, WrongUser

import pixeme.controllers as controllers

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = ''

blueprint = Blueprint('account_view', __name__, url_prefix=BLUEPRINT_PATH)

### Helpers ###

def admin_required(f):
    """Admin requirement decorator for front-end views. This uses the session
    instead of a JWT token."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'account' in session:
            account = session['account']
            db_account = Account.query.get(account['id'])

            if db_account is None:
                raise WrongUser()

            if 'role' in account and account['role'] == AccountRole.admin.name:
                g.account = db_account
                return f(*args, **kwargs)
        
        # raise AdminOnly()
        # TODO: Make a better page for this
        abort(403)

    return wrapper

def account_required(f):
    """Decorator to mark views that are only accessible to users with accounts."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'account' in session:
            account = session['account']
            db_account = Account.query.get(account['id'])

            if db_account is None:
                raise APIError(403, "Wrong authentication")
            elif 'role' in account and account['role'] == AccountRole.banned:
                raise UserBanned()
            else:
                g.account = db_account
                g.user = db_account.user
                return f(*args, **kwargs)
        else:
            raise RequiresLogin()

    return wrapper

def account_maybe(f):
    """Decorator to mark views that may be accessed with or without being logged in,
    but have different functionality based on login status."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        # Basically the same as `account_required`, except that we don't
        # error out if the user isn't logged in.
        if 'account' in session:
            account = session['account']
            db_account = Account.query.get(account['id'])

            if db_account is None:
                # raise APIError(403, "Wrong authentication")
                pass
            elif 'role' in account and account['role'] == AccountRole.banned:
                # raise UserBanned()
                pass
            else:
                g.account = db_account
                g.user = db_account.user
        return f(*args, **kwargs)

    return wrapper

@blueprint.errorhandler(APIError)
def view_error(err):
    """Handle any API errors that aren't caught by the view functions themselves.
    This will include validation errors, as that part is checked before the view."""

    # If we need to do any custom processing (like a dedicated 404 page), do it here.
    flash(err.message, 'error')
    # TODO: Make a better error page, instead of just redirecting to the front.
    return redirect(url_for('site_view.home'))#, err.status_code

### Routes ###

@blueprint.route('/login', methods=('GET',))
def login():
    """Show the full-page login form. This is technically a fallback
    for users whose browsers don't support the main page dropdown."""

    return render_template('views/login.html')

@blueprint.route('/login', methods=('POST',))
@use_args(request_schema.login, location='form')
def handle_login(args):
    """Handle a user login attempt."""

    # Check that the username and password are correct. If so, we add
    # some data to the session, so we can keep track of the user
    # across pages.
    # If the login attempt fails, the controller will raise a 403
    # exception, which we pass on as a flash notification. (Later,
    # we need the front end to detect if JS is disabled or notifications
    # are otherwise unavailable, and redirect to an appropriate "incorrect
    # username/password" page.)
    try:
        g.account = controllers.account.login_username_password(**args)
        g.user = g.account.user
        session['account'] = AccountSchema().dump(g.account)
        session['user'] = UserSchema().dump(g.user)
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('site_view.home'))

@blueprint.route('/logout', methods=('POST',))
@account_required
def handle_logout():
    """Handle a user logout attempt."""

    # Remove any user-specific bits from the session data.
    del session['account']
    del session['user']
    
    flash(gettext("You have been logged out."))
    return redirect(url_for('site_view.home'))

@blueprint.route('/create-account', methods=('GET',))
def create_account():
    """Show the Create Account wizard. Actually creating the account is done
    following a POST request."""

    # TODO: Something for rate-limiting and keeping out bots. *NOT* ReCAPTCHA!

    popular_languages = controllers.language.get_most_popular_languages(count=100)
    popular_languages.sort(key=lambda l: l[0].english)

    return render_template('views/create_account.html',
        languages=popular_languages
    )

@blueprint.route('/create-account', methods=('POST',))
@use_args(request_schema.create_account_form, location='form')
def handle_create_account(args):
    """Handle an account creation attempt."""

    # Back out if the user doesn't accept the ToS. This shouldn't happen,
    # as the form prevents it, but JS or other machinations can interfere.
    if not args['tosAccept']:
        flash(gettext("You must accept the Terms of Service to create an account"))
        return redirect(url_for('site_view.home'))

    try:
        # Create the account.
        account = controllers.account.create_account_username_password(
            username=args['username'],
            password=args['password'],
            email=args['email']
        )

        # Set the user's display name, if given.
        controllers.user.set_name_and_profile(account.user.id, args['name'])

        controllers.user.set_user_fluencies(account.user.id, args['fluencies'])

        return render_template('views/post_create_account.html', account=account)
    except APIError as err:
        flash(err.message, 'error')
        return redirect(url_for('site_view.home'))

    # TODO: Implement
    return jsonify(args), 501
