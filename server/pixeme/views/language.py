# Views for languages

from flask import (abort, Blueprint, flash, g, redirect,
    render_template, session, url_for)
from flask_babel import gettext
from webargs.flaskparser import use_args

from pixeme.models import Language
from pixeme.schema import request_schema
from pixeme.error import NotFound, NotYetImplemented

import pixeme.controllers as controllers

from .account import account_required

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = '/language'

blueprint = Blueprint('language_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/', methods=('GET',))
def explore_languages():
    """Show the most popular and newest languages, or something."""

    languages = controllers.language.get_languages_list()

    return render_template('views/language_index.html', languages=languages)

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_by_id(id):
    """Get the page for a language given its ID."""

    language = controllers.language.get_single_language_id(id)

    if language is not None:
        return render_template('views/language_view.html', language=language)
    else:
        # TODO: How should we do this one? Flash a message and go back?
        # Dedicated 404 page?
        abort(404)

@blueprint.route('/name/<name>', methods=('GET',))
def get_by_name(name):
    """Get the page for a language with the given name. These should be unique
    throughout the database, so using `first()` here shouldn't matter."""

    language = controllers.language.get_single_language(name=name)

    if language is not None:
        # Instead of worrying about separate views for name/ID, just
        # redirect to the "by ID" page. That's the canonical location.
        return redirect(url_for('.get_by_id', id=language.id))
    else:
        # TODO: Same as above. We should work out an appropriate response.
        raise NotFound(type='language', additional=name)

@blueprint.route('/search', methods=('GET',))
@use_args(request_schema.language_search_query, location='query')
def search(args):
    """Search for a language by ID or (partial) name. If a single result is
    found, redirect to that language's page. Otherwise, show a results page."""

    if args['id'] is not None:
        # Single result by ID -> Go to the language's page.
        return redirect(url_for('.get_by_id', id=args['id']))
    elif args['name'] is not None:
        single = controllers.language.get_single_language(name=args['name'])
        if single is not None:
            # Single result by name -> Go to the language's page. (Note that we
            # skip straight to the ID page instead of chaining redirects.)
            return redirect(url_for('.get_by_id', id=single.id))
        else:
            # Multiple results -> Show a search results view.
            languages = controllers.language.search_similar(args['name'])
            # TODO: Search results view
            raise NotYetImplemented()
    else:
        # TODO: Appropriate error message/view.
        abort(400)