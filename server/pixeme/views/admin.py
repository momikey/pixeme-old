# Views for admin pages

from flask import (abort, Blueprint, current_app, flash, request, g, session,
    render_template, redirect, url_for)
from flask_babel import gettext
from webargs.flaskparser import use_args

from pixeme.models import Account, User, AccountRole, PhraseStatus
from pixeme.schema import request_schema
from pixeme.error import APIError, NotYetImplemented

import pixeme.controllers as controllers

from .account import admin_required

# Views have their own paths.
BLUEPRINT_PATH = "/admin"

blueprint = Blueprint('admin_view', __name__, url_prefix=BLUEPRINT_PATH)

### Helpers ###

@blueprint.before_request
@admin_required
def before_request():
    pass

### Routes ###

@blueprint.route('/')
def home():
    """Admin home page."""

    return render_template('views/admin/layout.html')

@blueprint.route('/users', methods=('GET',))
@use_args(request_schema.query, location='query')
def users_list(args):
    """Admin users list."""

    users = controllers.user.get_users_list(**args)

    return render_template('views/admin/users.html', users=users)

@blueprint.route('/users/<int:id>', methods=('GET',))
def user_details(id):
    """Admin page for a user's account details."""

    user = controllers.user.get_single_user(id=id)

    if user is not None:
        return render_template('views/admin/user_details.html', user=user)
    else:
        # TODO: Make a better 404 handler
        abort(404)

@blueprint.route('/users/create', methods=('GET',))
def start_create_user_account():
    """Admin page to enter the details for a new user account."""

    return render_template('views/admin/create_account.html')

@blueprint.route('/users/create', methods=('POST',))
@use_args(request_schema.create_account, location='form')
def create_user_account(args):
    """Admin page to create a user account."""

    try:
        account = controllers.account.create_account_username_password(**args)
        return render_template('views/admin/account_created.html', account=account)
    except APIError as err:
        flash(err.message, 'error')
        return redirect(url_for('admin_view.users_list'))

@blueprint.route('/users/<int:id>/update', methods=('POST',))
def update_user(id):
    """Admin form submission endpoint to update a user's data."""

    ### TODO: Implement a user update controller

    return redirect(url_for('admin_view.user_details', id=id))

@blueprint.route('/users/<int:id>/confirm-delete', methods=('POST',))
def confirm_user_delete(id):
    """Admin page to confirm deleting a user."""

    user = controllers.user.get_single_user(id=id)

    return render_template('views/admin/confirm_user_delete.html', user=user)

@blueprint.route('/users/<int:id>/delete', methods=('POST',))
def delete_user(id):
    """Admin page to delete a user after confirmation."""

    user = controllers.user.get_single_user(id=id)

    if user is not None:
        username = user.account.username
        if controllers.account.delete_user(id=user.account.id):
            flash(gettext("Deleted user %(name)s", name=username))
        else:
            flash(gettext("Could not delete user %(name)s", name=username))
    else:
        flash(gettext("User does not exist"), 'error')

    return redirect(url_for('admin_view.users_list'))

@blueprint.route('/users/<int:id>/confirm-ban', methods=('POST',))
def confirm_user_ban(id):
    """Admin page to confirm banning a user."""

    user = controllers.user.get_single_user(id=id)

    if user is not None:
        return render_template('views/admin/confirm_user_ban.html', user=user)
    else:
        # TODO Handler
        abort(404)

@blueprint.route('/users/<int:id>/ban', methods=('POST',))
def ban_user(id):
    """Admin page to ban a user after confirmation."""

    user = controllers.user.get_single_user(id=id)

    if user is not None:
        username = user.account.username
        if controllers.account.change_user_role(user.account.id, AccountRole.banned.name):
            flash(gettext("User %(name)s has been banned", name=username))
        else:
            flash(gettext("Could not change role for user %(name)s", name=username))
    else:
        flash(gettext("User does not exist"), 'error')

    return redirect(url_for('admin_view.users_list'))

@blueprint.route('/languages', methods=('GET',))
@use_args(request_schema.query, location='query')
def languages_list(args):
    """Admin languages list."""

    languages = controllers.language.get_languages_list(**args)

    return render_template('views/admin/languages.html', languages=languages)

@blueprint.route('/languages/<int:id>', methods=('GET',))
def language_details(id):
    """Admin page for details on a single language."""

    language = controllers.language.get_single_language_id(id)

    if language is not None:
        return render_template('views/admin/language_details.html', language=language)
    else:
        # TODO: 404 handler
        abort(404)

@blueprint.route('/languages/add', methods=('GET',))
def start_add_language():
    """Admin page to start adding a new language to the database."""

    return render_template('views/admin/add_language.html')

@blueprint.route('/languages/add', methods=('POST',))
@use_args(request_schema.create_language, location='form')
def add_language(args):
    """Admin page to add a new language to the database."""

    try:
        language = controllers.language.create_language(**args)
        # return render_template('views/admin/language_added.html', language=language)
        flash(gettext("Added %(lang)s language", lang=language.english), 'message')
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('admin_view.languages_list'))

@blueprint.route('/languages/<int:id>/update', methods=('POST',))
def update_language(id):
    """Admin form submission endpoint to update a language's data."""

    ### TODO

    return redirect(url_for('admin_view.language_details', id=id))

@blueprint.route('/languages/<int:id>/confirm-remove', methods=('POST',))
def confirm_remove_language(id):
    """Admin view to confirm removing a language from the database."""

    language = controllers.language.get_single_language_id(id)

    if language is not None:
        return render_template('views/admin/confirm_language_remove.html', language=language)
    else:
        # TODO Handler
        abort(404)

@blueprint.route('/languages/<int:id>/remove', methods=('POST',))
def remove_language(id):
    """Admin form submission endpoint to remove a language from the database."""

    language = controllers.language.get_single_language_id(id)

    # Check to see if the language exists. If not, then we can't delete it.
    # This is a generic 404, not an APIError.
    if language is None:
        # TODO Handler
        abort(404)

    ### Assumes we have confirmation
    try:
        name = language.english
        language = controllers.language.delete_language(id=id)
        flash(gettext("Removed %(lang)s", lang=name))
    except APIError as err:
        flash(gettext("Unable to delete language: %(msg)s", msg=err.message), 'error')

    return redirect(url_for('admin_view.languages_list'))

@blueprint.route('/images', methods=('GET',))
@use_args(request_schema.query, location='query')
def images_list(args):
    """Admin images list."""

    images = controllers.image.get_images_list(**args)

    return render_template('views/admin/images.html', images=images)

@blueprint.route('/images/<int:id>', methods=('GET',))
def image_details(id):
    """Admin page for details on a single image."""

    image = controllers.image.get_single_image(id=id)

    if image is not None:
        # TODO: This view will need a little finesse, because we have to show
        # the image, all the metadata, and everything else.
        return render_template('views/admin/image_details.html',
            image=image, image_url=controllers.image.image_url(image.filename))
    else:
        # TODO: 404 handler
        abort(404)    

@blueprint.route('/images/add', methods=('GET',))
def start_add_image():
    """Admin page to start adding a new image."""

    # TODO

    raise NotYetImplemented()

@blueprint.route('/images/<int:id>/confirm-remove', methods=('POST',))
def confirm_remove_image(id):
    """Admin view to confirm removing an image from the database."""

    image = controllers.image.get_single_image(id=id)

    if image is not None:
        return render_template('views/admin/confirm_image_remove.html', image=image)
    else:
        # TODO Handler
        abort(404)

@blueprint.route('/images/<int:id>/remove', methods=('POST',))
def remove_image(id):
    """Admin form submission endpoint to remove an image from the database."""

    image = controllers.image.get_single_image(id=id)

    # Check to make sure the image exists. Otherwise, it's a 404, not an APIError.
    if image is None:
        # TODO Handler
        abort(404)

    ### Assumes we have confirmation
    try:
        filename = image.filename
        image = controllers.image.delete_image(id=id)
        flash(gettext('Removed image "%(img)s"', img=filename))
    except APIError as err:
        flash(gettext("Unable to delete image: %(msg)s", msg=err.message), 'error')

    return redirect(url_for('admin_view.images_list'))

@blueprint.route('/phrases', methods=('GET',))
@use_args(request_schema.query, location='query')
def phrases_list(args):
    """Admin phrases list."""

    phrases = controllers.phrase.get_phrases_list(**args)

    return render_template('views/admin/phrases.html', phrases=phrases)

@blueprint.route('/phrases/<int:id>', methods=('GET',))
def phrase_details(id):
    """Admin page for details on a single phrase."""

    phrase = controllers.phrase.get_phrase_by_id(id=id)

    if phrase is not None:
        return render_template('views/admin/phrase_details.html', phrase=phrase)
    else:
        # TODO: 404 handler
        abort(404)

@blueprint.route('/phrases/add', methods=('GET',))
def start_add_phrase():
    """Admin page to start adding a new phrase."""

    return render_template('views/admin/add_phrase.html')

@blueprint.route('/phrases/add', methods=('POST',))
@use_args(request_schema.add_empty_phrase, location='form')
def add_phrase(args):
    """Admin page to add a new phrase to the database."""

    try:
        phrase = controllers.phrase.add_phrase(**args)
        flash(gettext('Added phrase "%(text)s"', text=phrase.text), 'message')
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('admin_view.phrases_list'))

@blueprint.route('/phrases/<int:id>/accept', methods=('POST',))
def accept_phrase(id):
    """Admin endpoint to change a phrase's status to accepted."""

    try:
        phrase = controllers.phrase.change_phrase_status(id, PhraseStatus.accepted)
        flash(gettext('Set phrase "%(text)s" to accepted', text=phrase.text), 'message')
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('admin_view.phrases_list'))

@blueprint.route('/phrases/<int:id>/reject', methods=('POST',))
def reject_phrase(id):
    """Admin endpoint to change a phrase's status to rejected."""

    try:
        phrase = controllers.phrase.change_phrase_status(id, PhraseStatus.rejected)
        flash(gettext('Set phrase "%(text)s" to rejected', text=phrase.text), 'message')
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('admin_view.phrases_list'))

@blueprint.route('/phrases/<int:id>/update', methods=('POST',))
def update_phrase(id):
    """Admin page to update a phrase."""

    # TODO

    raise NotYetImplemented()

@blueprint.route('/phrases/<int:id>/confirm-remove', methods=('POST',))
def confirm_remove_phrase(id):
    """Admin page to confirm removing a phrase."""

    phrase = controllers.phrase.get_phrase_by_id(id=id)

    if phrase is not None:
        return render_template('views/admin/confirm_phrase_removal.html', phrase=phrase)
    else:
        # TODO: Handler
        abort(404)

@blueprint.route('/phrases/<int:id>/remove', methods=('POST',))
def remove_phrase(id):
    """Admin form submission endpoint to remove a phrase from the database."""

    phrase = controllers.phrase.get_phrase_by_id(id=id)

    # Check if we're trying to delete a nonexistent phrase.
    # This is *not* an APIError, technically, so we just use our generic
    # 404 handler, or whatever we put in its place.
    if phrase is None:
        # TODO: Handler
        abort(404)

    ### Assumes we have confirmation
    try:
        text = phrase.text
        phrase = controllers.phrase.delete_phrase(id=id)
        flash(gettext('Removed phrase "%(text)s"', text=text))
    except APIError as err:
        flash(gettext("Unable to delete phrase: %(msg)s", msg=err.message), 'error')

    return redirect(url_for('admin_view.phrases_list'))

@blueprint.route('/tags', methods=('GET',))
@use_args(request_schema.query, location='query')
def tags_list(args):
    """Admin tags list."""

    tags = controllers.tag.get_tags_list(**args)

    return render_template('views/admin/tags.html', tags=tags)

@blueprint.route('/tags/<int:id>', methods=('GET',))
def tag_details(id):
    """Admin page for details on a single tag."""

    tag = controllers.tag.get_single_tag(id=id)

    if tag is not None:
        return render_template('views/admin/tag_details.html', tag=tag)
    else:
        # TODO: 404 handler
        abort(404)

@blueprint.route('/tags/add', methods=('GET',))
def start_add_tag():
    """Admin page to start adding a new tag."""

    return render_template('views/admin/add_tag.html')

@blueprint.route('/tags/add', methods=('POST',))
@use_args(request_schema.add_empty_tag, location='form')
def add_tag(args):
    """Admin page to add a new tag to the database."""

    try:
        tag = controllers.tag.add_tag(**args)
        flash(gettext('Added tag "%(text)s"', text=tag.text), 'message')
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('admin_view.tags_list'))

@blueprint.route('/tags/<int:id>/confirm-remove', methods=('POST',))
def confirm_remove_tag(id):
    """Admin page to confirm removing a tag."""

    tag = controllers.tag.get_single_tag(id=id)

    if tag is not None:
        return render_template('views/admin/confirm_tag_removal.html', tag=tag)
    else:
        # TODO: Handler
        abort(404)

@blueprint.route('/tags/<int:id>/remove', methods=('POST',))
def remove_tag(id):
    """Admin form submission endpoint to remove a tag from the database."""

    tag = controllers.tag.get_single_tag(id=id)

    # Check if we're trying to delete a nonexistent tag.
    # This is *not* an APIError, technically, so we just use our generic
    # 404 handler, or whatever we put in its place.
    if tag is None:
        # TODO: Handler
        abort(404)

    ### Assumes we have confirmation.
    try:
        tag = controllers.tag.delete_tag(tag.text)
        flash(gettext('Removed tag "%(text)s"', text=tag.text))
    except APIError as err:
        flash(gettext("Unable to delete tag: %(msg)s", msg=err.message), 'error')

    return redirect(url_for('admin_view.tags_list'))