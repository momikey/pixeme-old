# Views for media access

# This is a kind of special case. We don't want users to know or care
# about the internal filessytem of the server, but we *do* want that
# filesystem to not hate us for dumping possibly millions of images
# into the same directory. Thus, we partition Pixeme's image set by
# user, but present it as a flat filesystem to the view layer.
#
# However, this means we need a mapping from that flat filesystem
# back into the per-user scheme we're actually using. Hence this module.

from flask import current_app, Blueprint, abort, send_from_directory

from pixeme.error import APIError, NotYetImplemented

import pixeme.controllers as controllers

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = ''

blueprint = Blueprint('media_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/media/<filename>', methods=('GET',))
def get_upload(filename):
    """Get an uploaded file."""

    image = controllers.image.get_image_from_filename(filename)

    if image is None:
        # No image found, but we don't really need to abort.
        return '', 404
    else:
        return send_from_directory(
            controllers.image.directory_for(image),
            image.filename
        )
