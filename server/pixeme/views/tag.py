# Views for tags

from flask import (abort, Blueprint, flash, g, redirect,
    render_template, session, url_for)
from flask_babel import gettext
from webargs.flaskparser import use_args

from pixeme.models import Tag
from pixeme.schema import request_schema
from pixeme.error import NotFound, NotYetImplemented

import pixeme.controllers as controllers

from .account import account_required

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = '/tag'

blueprint = Blueprint('tag_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/', methods=('GET',))
def explore_tags():
    """Show some of the most used tags."""

    popular = controllers.tag.get_most_popular_tags(50)

    newest = controllers.tag.get_newest_tags(50)

    # raise NotYetImplemented()

    return render_template('views/tag_index.html')
    # return render_template('views/explore_tags.html',
    #     popular=popular, newest=newest)

@blueprint.route('/text/<text>', methods=('GET',))
def get_by_text(text):
    """Get the page for the tag with the given text. Note that this, not the ID,
    is considered the canonical page for tags."""

    tag = controllers.tag.get_single_tag(text=text)

    if tag is not None:
        return render_template('views/tag_view.html', tag=tag)
    else:
        # TODO: How to handle a nonexistent tag?
        abort(404)

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_by_id(id):
    """Get the page for the tag with the given database ID."""

    tag = controllers.tag.get_single_tag(id=id)

    if tag is not None:
        return redirect(url_for('.get_by_text', text=tag.text))
    else:
        # TODO Same as above.
        raise NotFound(type='tag', additional=id)

@blueprint.route('/search', methods=('GET',))
@use_args(request_schema.tag_search_query, location='query')
def search(args):
    """Search for a tag with the given ID, or all tags similar to
    the given text. If a single result is found, redirect to that
    tag's page. Otherwise, show a results page."""

    if args['id'] is not None:
        # Single result by ID -> Go to the tag's page.
        return redirect(url_for('.get_by_id', id=args['id']))
    elif args['text'] is not None:
        single = controllers.tag.get_single_tag(text=args['text'])
        if single is not None:
            # Single result by name -> Go to the tag's page. (Note that we
            # skip straight to the name page instead of chaining redirects.)
            return redirect(url_for('.get_by_text', text=single.text))
        else:
            # Multiple results -> Show a search results view.
            languages = controllers.tag.search_similar(args['text'])
            # TODO: Search results view
            raise NotYetImplemented()
    else:
        # TODO: Appropriate error message/view.
        abort(400)