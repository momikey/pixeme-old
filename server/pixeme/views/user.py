# Views for user pages

from functools import wraps

from flask import (abort, Blueprint, current_app, flash, g,
    redirect, request, render_template, session, url_for)
from flask_babel import gettext
from webargs.flaskparser import use_args

from pixeme.models import (Account, AccountRole, User,
    AccountSchema, UserSchema)
from pixeme.schema import request_schema
from pixeme.error import APIError, AdminOnly, NotYetImplemented, WrongUser

import pixeme.controllers as controllers

from .account import account_required, account_maybe

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = '/user'

blueprint = Blueprint('user_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/me', methods=('GET',))
@account_required
def view_my_profile():
    """Show the profile for the logged-in user, with editing options."""

    return render_template('views/user_profile.html',
        user=g.user, logged_in_user=True)

@blueprint.route('/me', methods=('POST', 'PUT'))
@account_required
def update_profile():
    """Update the profile of the logged in user."""

    raise NotYetImplemented()

@blueprint.route('/change-password', methods=('GET',))
@account_required
def change_password():
    """Show the view allowing users to change their passwords."""

    return render_template('views/change_password.html', user=g.user)

@blueprint.route('/change-password', methods=('POST',))
@account_required
@use_args(request_schema.change_password, location='form')
def handle_change_password(args):
    """Handle a password change request."""

    try:
        if args['new_password'] != args['retype']:
            raise APIError(message=gettext("Password mismatch"))

        result = controllers.account.change_user_password(
            g.account.id,
            args['old_password'],
            args['new_password']
        )

        if result:
            flash(gettext("Your password has been changed"))
        else:
            flash(gettext("Unable to change password"), 'error')
    except APIError as err:
        flash(err.message, 'error')

    return redirect(url_for('user_view.view_my_profile'))

@blueprint.route('/id/<int:id>', methods=('GET',))
@account_maybe
def get_user(id):
    """Show the public profile for the target user."""

    user = controllers.user.get_single_user(id)

    if user is not None:
        return render_template('views/user_profile.html',
            user=user, logged_in_user=(user.id == g.user.id if hasattr(g,'user') else False))
    else:
        # TODO: Handle user not found
        abort(404)

@blueprint.route('/id/<int:id>/languages', methods=('GET',))
@account_maybe
def get_user_languages(id):
    """Show the target user's language fluencies, with an option to add to one's own."""

    user = controllers.user.get_single_user(id)

    if user is not None:
        return render_template('views/user_languages.html',
            user=user, logged_in_user=(user.id == g.user.id if hasattr(g,'user') else False))
    else:
        abort(404)

@blueprint.route('/id/<int:id>/images', methods=('GET',))
@account_maybe
def get_user_images(id):
    """Show the target user's uploaded images, with options related to a logged-in user."""

    user = controllers.user.get_single_user(id)

    if user is not None:
        return render_template('views/user_images.html',
            user=user, logged_in_user=(user.id == g.user.id if hasattr(g,'user') else False))
    else:
        abort(404)

@blueprint.route('/id/<int:id>/phrases', methods=('GET',))
@account_maybe
def get_user_phrases(id):
    """Show the target user's posted phrases, with options for logged-in users."""

    user = controllers.user.get_single_user(id)

    if user is not None:
        return render_template('views/user_phrases.html',
            user=user, logged_in_user=(user.id == g.user.id if hasattr(g,'user') else False))
    else:
        abort(404)
