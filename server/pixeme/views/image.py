# Views for images

from flask import (abort, Blueprint, current_app, flash, g,
    redirect, request, render_template, session, url_for)
from flask_babel import gettext
from flask_uploads import UploadNotAllowed
from webargs.flaskparser import use_args

from pixeme.models import (AccountRole, Image, ImageSchema, Phrase,
    PhraseStatus, Language, User)
from pixeme.schema import request_schema
from pixeme.error import APIError, NotYetImplemented

import pixeme.controllers as controllers

from .account import account_required

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = '/image'

blueprint = Blueprint('image_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/', methods=('GET',))
def explore_images():
    """Show a page for exploring new images."""

    newest = controllers.image.get_newest_images(count=20)
    tagged = controllers.image.get_most_tagged_images(count=20)
    translated = controllers.image.get_most_translated_images(count=20)

    return render_template('views/image_index.html',
        newest=newest,
        tagged=tagged,
        translated=translated
    )

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_image(id):
    """Show the page for the given image."""

    image = controllers.image.get_single_image(id)

    if image is not None:
        return render_template('views/image_view.html',
            image=image,
            image_url=controllers.image.image_url(image.filename)
        )
    else:
        # TODO: How to handle this?
        abort(404)

@blueprint.route('/new', methods=('GET',))
@account_required
def upload_image():
    """Show the "upload image" page."""

    # TODO: For debugging only
    if not hasattr(g.user, 'languages') or len(g.user.languages) == 0:
        g.user.languages = controllers.language.get_languages_list()

    return render_template('views/post_new_image.html',
        user=g.user
    )

@blueprint.route('/new', methods=('POST',))
@account_required
@use_args(request_schema.create_image, location='form')
def handle_upload_image(args):
    """Handle an uploaded image, resizing it, saving it, and connecting
    it to the uploader, etc."""

    # raise NotYetImplemented(payload=args)

    # Get the file from the request.
    if 'image' in request.files:
        im = request.files['image']

        try:
            image, url = controllers.image.create_image_from_upload(im, **args)

            flash(gettext("Image successfully uploaded"))
            return redirect(url_for('.get_image', id=image.id))
        except UploadNotAllowed as error:
            # TODO: A better handler for files of the wrong type.
            abort(415)

    # NOTE: Here's the API version for easier reference.
    # # Get the file out of the request and send it to the controller.
    # if 'image' in request.files:
    #     im = request.files['image']
    #     try:
    #         image, url = image_controllers.create_image(im, **args)

    #         return (
    #             jsonify(response_schema.CreateImage().dump(image)),
    #             201,
    #             [('Location', url)]
    #         )
    #     except UploadNotAllowed as error:
    #         raise APIError(415, "Can't upload files of this type")