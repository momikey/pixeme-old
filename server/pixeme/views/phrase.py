# Views for phrases

from flask import (abort, Blueprint, flash, g, redirect,
    render_template, session, url_for)
from flask_babel import gettext
from webargs.flaskparser import use_args

from pixeme.models import Phrase, PhraseStatus
from pixeme.schema import request_schema, response_schema
from pixeme.error import NotFound, NotYetImplemented

import pixeme.controllers as controllers

from .account import account_required

# Blueprint path - we always use this, even if it's the root path
BLUEPRINT_PATH = '/phrase'

blueprint = Blueprint('phrase_view', __name__, url_prefix=BLUEPRINT_PATH)

### Routes ###

@blueprint.route('/', methods=('GET',))
def explore_phrases():
    """Show some of the most popular phrases, probably filtered by language."""

    raise NotYetImplemented()

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_by_id(id):
    """Get the page for a phrase given its ID."""

    phrase = controllers.phrase.get_phrase_by_id(id=id)

    if phrase is not None:
        return render_template('views/phrase_view.html',
            phrase=phrase,
            images=[response_schema.PhraseImages().dump(i) for i in phrase.images])
    else:
        # TODO: 404 handling, probably going back to the "explore" page.
        abort(404)
