###
# Error handlers

# Pixeme errors come in two flavors. The site will raise the more typical
# Werkzeug HTTPException subclasses, while the REST API will use the custom
# APIError subclasses defined here. The top layer of the app will handle
# them appropriately.

from http import HTTPStatus
from flask import jsonify

# The base API exception class is derived from the Flask docs.
class APIError(Exception):
    """Helper class to generate meaningful API exceptions with proper HTTP status"""

    def __init__(self, status_code=400, message=None, payload=None):
        Exception.__init__(self)

        self.status_code = HTTPStatus(status_code)
        self.message = message or self.status_code.description
        self.payload = payload
    
    def to_dict(self):
        resp = dict(self.payload or ())
        resp['status'] = self.status_code
        resp['message'] = self.message
        return resp

class AdminOnly(APIError):
    """Exception for attempting to access an admin-only resource without proper authentication."""

    def __init__(self, payload=None):
        APIError.__init__(self, 403, "This resource may only be accessed by administrators", payload)

class BadMethod(APIError):
    """Exception for attempting to access a resource through a method not implemented."""

    def __init__(self, method="POST", payload=None):
        APIError.__init__(self, 405, "{method} not allowed on this resource".format(method=method), payload)

class NoSuchUser(APIError):
    """Exception for attempting to access a user that doesn't exist. (View-only, as API endpoints
    can use a generic NotFound instead.)"""

    def __init__(self, id=None, payload=None):
        APIError.__init__(self, 404, "User {id} does not exist.".format(id=id), payload)

class WrongUser(APIError):
    """Exception for attempting to access a protected endpoint of another user."""

    def __init__(self, payload=None):
        APIError.__init__(self, 403, "You cannot access this resource.")

class NotYetImplemented(APIError):
    """Exception for a resource that isn't yet implemented, but will be in the future."""

    def __init__(self, payload=None):
        APIError.__init__(self, 501, "This resource has not yet been implemented.", payload)

class NotFound(APIError):
    """Exception for when a requested entity (e.g., user, group, post) doesn't exist in the database."""

    def __init__(self, type='resource', additional='no additional info provided', payload=None):
        APIError.__init__(self, 404, "{type} not found: {additional}".format(
            type=type.capitalize(), additional=additional), payload)

class UserBanned(APIError):
    """Exception for a banned user attempting to interact with a protected resource."""

    def __init__(self, user, payload=None):
        APIError.__init__(
            self, 403, "User {name} is banned on this server".format(name=user.account.username), payload)

class RequiresLogin(APIError):
    """Exception for attempting to access a login-required view without authentication."""

    def __init__(self, payload=None):
        APIError.__init__(self, 403, "You must be logged in to view this page.")

class RateLimitExceeded(APIError):
    """Exception for exceeding a rate limit."""
    
    def __init__(self, message):
        APIError.__init__(self, 429, "Rate limit exceeded: {msg}".format(msg=message))