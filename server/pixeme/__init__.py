import os

from flask import Flask, jsonify, render_template, session
from flask.json import JSONEncoder
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_executor import Executor
from flask_jwt_extended import JWTManager
from flask_limiter import Limiter

### NOTE: Using this method for rate limiting relies on knowing how
### many proxies the application is behind.
### See <https://werkzeug.palletsprojects.com/en/1.0.x/middleware/proxy_fix/>
from flask_limiter.util import get_remote_address

### NOTE: Flask-Static-Digest does not support Python 3.5,
### as it uses syntax that is only available on 3.6+. To fix this,
### I have edited the package's source on 3.5.
from flask_static_digest import FlaskStaticDigest

### NOTE: Current flask_uploads version is broken
### If there isn't a fixed release by the time I implement uploads,
### I may have to temporarily use the Github repo link instead.
import flask_uploads
from flask_uploads import UploadSet, configure_uploads, UploadNotAllowed

### NOTE: Flask-Babel claims to only support Python 3.6+, but I can
### find no actual 3.6-specific code in version 1.0.0. We'll need to
### watch this.
from flask_babel import Babel

from webargs.flaskparser import parser

from werkzeug.exceptions import HTTPException

from .error import APIError

# Add in the package version.
from .version import __version__

# Define extensions for ease of importing
from .models import db, ma
cors = CORS()
migrate = Migrate()
executor = Executor()
jwt = JWTManager()
static_digest = FlaskStaticDigest()
babel = Babel()

### NOTE: Later on, we might want to use memcached or Redis for storing
### info about rate limits. If we use Redis, we might as well add in Celery
### as a task queue and drop Flask-Executor. Memcached might be better, though.
### This needs further study.
limiter = Limiter(
    key_func=get_remote_address,
    default_limits=["1000/day", "100/hour"]
)

# Media sets for uploads and avatars
uploads_set = UploadSet('media', flask_uploads.DEFAULTS + flask_uploads.AUDIO)
avatars_set = UploadSet('avatars', flask_uploads.IMAGES)

class CustomJSONEncoder(JSONEncoder):
    """This custom encoder calls the `to_json()` method of an object, if present, to serialize it to JSON. If the
    object's class doesn't define this method, it will fall back to the default handler Flask provides."""

    def default(self, obj):
        try:
            result = obj.to_json()
            # Do we need to do any other checks here?
            return result
        except AttributeError:
            return JSONEncoder.default(self, obj)

@babel.localeselector
def get_locale():
    if 'locale' in session:
        return session['locale']
    elif 'user' in session and 'profile' in session['user'] and session['user']['profile'] is not None:
        return session['user']['profile'].get('locale', 'en_US')
    else:
        # Fallback to English
        return None

def create_app():
    """
    Create the main Pixeme Flask application.
    """

    # Create the app object itself.
    app = Flask(
        __name__,
        instance_relative_config=True,
        template_folder='../assets/templates'
    )

    # Load our custom JSON handler
    app.json_encoder = CustomJSONEncoder
    
    # Set up config stuff

    ### Instance data setup ###

    # Development/debug defaults (these will be changed for production)
    app.config.from_mapping(
        SECRET_KEY='secret',
        JWT_SECRET_KEY='secret',
        UPLOADS_DEFAULT_DEST = os.path.join(app.instance_path, "uploads"),
        AVATARS_URI_DIR = 'avatars',
        MEDIA_URI_DIR = 'media',
        MAX_CONTENT_LENGTH = 2 * 1024 * 1024,
        MAX_IMAGE_SIZE = (1280, 720)
    )

    # Site-wide configuration
    app.config.from_pyfile('config.py', silent=True)

    # Mode-specific configuration (this overrides site-wide)
    if app.env in ['production', 'development']:
        app.config.from_pyfile(app.env + '.py', silent=True)
        # Also check a "secrets" file for private keys and other things that
        # should not be checked into source control.
        app.config.from_pyfile(app.env + '.secret.py', silent=True)

    # Different settings for tests
    if app.env == 'testing':
        app.config['TESTING'] = True
        app.config.from_pyfile(os.path.join(root_path, 'tests', 'flask.py'))

    # Session setup, uses the SECRET_KEY config setting above
    app.secret_key = app.config['SECRET_KEY']
    
    ### Extensions setup ###
    
    # Server-wide CORS setup
    cors.init_app(app)
    
    # Databases and migrations
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)

    # Executor, so we can use futures
    executor.init_app(app)
    
    # JWT authentication
    jwt.init_app(app)

    # Static digest for the "basic" frontend
    static_digest.init_app(app)

    # Babel for i18n
    babel.init_app(app)

    # Rate limiter
    limiter.init_app(app)
    
    # Uploads
    for path in [
        app.instance_path,
        app.config["UPLOADS_DEFAULT_DEST"]
    ]:
        try:
            os.makedirs(path)
        except OSError:
            pass
    
    configure_uploads(app, uploads_set)
    configure_uploads(app, avatars_set)

    # HTML view blueprints
    from .views import (account, admin, image, language, media,
        phrase, site, tag, user)
    app.register_blueprint(account.blueprint)
    app.register_blueprint(admin.blueprint)
    app.register_blueprint(image.blueprint)
    app.register_blueprint(language.blueprint)
    app.register_blueprint(media.blueprint)
    app.register_blueprint(phrase.blueprint)
    app.register_blueprint(site.blueprint)
    app.register_blueprint(tag.blueprint)
    app.register_blueprint(user.blueprint)
    
    # REST API blueprints
    from .api import (account, image, language, phrase, tag, user)
    app.register_blueprint(account.blueprint)
    app.register_blueprint(image.blueprint)
    app.register_blueprint(language.blueprint)
    app.register_blueprint(phrase.blueprint)
    app.register_blueprint(tag.blueprint)
    app.register_blueprint(user.blueprint)

    # Jinja context processors
    @app.context_processor
    def add_account_role():
        """Make the AccountRole enum available to view templates."""
        from .models import AccountRole
        return dict(AccountRole=AccountRole)

    @app.context_processor
    def add_phrase_status():
        """Make the PhraseStatus enum available to view templates."""
        from .models import PhraseStatus
        return dict(PhraseStatus=PhraseStatus)

    @app.context_processor
    def add_today():
        """Add today's date to view templates."""
        from datetime import date

        return dict(today=date.today())

    @app.context_processor
    def add_pixeme_metadata():
        """Add some server-specific metadata to view templates."""

        return dict(
            pixeme_version=__version__
        )

    # Custom Jinja functions, filters, and tests

    @app.template_global()
    def can_edit_phrase():
        from .models import AccountRole

        if 'user' not in session:
            return False
        else:
            locale = session['user'].get('locale', 'en-US')
            return ('fluencies' in session['user'] and \
                locale in (l['tag'] for l in session['user']['fluencies'])) or \
                session['account']['role'] in (AccountRole.admin.name, AccountRole.moderator.name)

    @app.template_global()
    def image_url_for(img):
        from .controllers.image import image_url

        return image_url(img.filename) if img is not None else None

    # Custom error handler stuff

    # Register our custom error handler
    @app.errorhandler(APIError)
    def handle_api_error(error):
        response = jsonify(error.to_dict())
        return response, error.status_code

    # Convert webargs errors to API

    # @app.errorhandler(422)
    # @app.errorhandler(400)
    # def convert_api_error(error):
    #     headers = error.data.get('headers', None)
    #     messages = error.data.get('messages', ["Invalid request"])

    #     return handle_api_error(APIError(error.code, "Validation failure", payload={"errors": messages}))
    @parser.error_handler
    def handle_error(error, req, schema, **kwargs):
        print(error, req.form)
        raise APIError(422, "Validation failure", payload={"errors": error.messages})

    ### High-level routes ###
    
    # Debug sandbox page
    # NOTE: Remove this before we go into production!
    @app.route("/debug")
    def index():
        return render_template('views/hello.html')
    
    # Debug endpoint to show all known routes
    @app.route("/debug/routes")
    def debug_routes():
        return jsonify(repr(app.url_map))
    
    # All setup complete, so return our app object
    return app