# This package is for the Pixeme REST API

# The base path for all REST API routes. If we need to version this
# later on, we'll find a way.
API_PATH = '/api'