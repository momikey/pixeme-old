# API endpoints for handling account creation, login, and similar activities.

import os
from datetime import datetime, timedelta
from functools import wraps, partial

from flask import Blueprint, current_app, jsonify, request, g
from flask_jwt_extended import (create_access_token, jwt_required,
    get_jwt_identity, create_refresh_token, jwt_refresh_token_required,
    verify_jwt_in_request, verify_fresh_jwt_in_request)
from webargs.flaskparser import use_args, use_kwargs

from pixeme import db, jwt, limiter
from pixeme.models import Account, AccountRole, User
from pixeme.schema import request_schema, response_schema
from pixeme.helpers import update_last_activity
from pixeme.error import *

import pixeme.controllers.account as account_controllers

from . import API_PATH

# Each API module has its own path
BLUEPRINT_PATH = "{api}/account".format(api=API_PATH)

blueprint = Blueprint('account', __name__, url_prefix=BLUEPRINT_PATH)

# API blueprints need some custom error handling.
@blueprint.errorhandler(429)
def handle_limit_exceeded(e):
    raise RateLimitExceeded(e.description)

### Helpers ###

def admin_required(f):
    """Decorator to inspect the JWT authentication and fail if the user
    is not an admin. If the user *is* an admin, store the DB entity into
    `g.account`."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()

        account = Account.query.filter_by(username=get_jwt_identity()).first()
        if account is None or account.role != AccountRole.admin:
            raise AdminOnly()
        else:
            g.account = account
            return f(*args, **kwargs)

    return wrapper

def account_required(f=None, *, minimum=None):
    """Decorator to check for a valid JWT authentication. If one is found,
    the account entity associated with the sender is loaded into `g.account`."""

    if f is None:
        return partial(account_required, minimum=minimum)

    @wraps(f)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()

        account = Account.query.filter_by(username=get_jwt_identity()).first()
        if account is None:
            raise APIError(403, "Invalid authentication")
        elif account.role == AccountRole.banned:
            raise UserBanned()
        elif minimum is not None and account.role.value < minimum.value:
            raise APIError(403, "Insufficient privileges")
        else:
            g.account = account
            return f(*args, **kwargs)
    
    return wrapper

def update_activity(f):
    """Decorator to update a user's "last activity" field. Most protected
    API endpoints will need this."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        rv = f(*args, **kwargs)

        # If calling the wrapped function doesn't throw an exception,
        # we can assume the operation succeeded.

        g.account.last_activity = datetime.utcnow()
        db.session.add(g.account)
        db.session.commit()

        return rv

    return wrapper

### Routes ###

@blueprint.route('/create', methods=('POST',))
@limiter.limit("20/day")
@use_args(request_schema.create_account)
def create_account(args):
    """Create a new Pixeme user account."""

    # TODO: Allow OAuth login, check password strength, etc.

    # Get the values out of the request.
    username = args['username']
    password = args['password']
    email = args['email']

    account = account_controllers.create_account_username_password(
        username, password, email)

    # We return a temporary auth token, which will allow the front end to
    # login before redirecting to an onboarding page. The token expires
    # after a minute, which should be plenty of time.
    # TODO: If there's no login attempt in that time, should we look to
    # back out the account creation?
    if account is not None:
        return jsonify({
            'username': username,
            'temp_token': create_access_token(
                username,
                expires_delta=timedelta(seconds=60)
            )
        }), 201
    else:
        raise APIError("Something went wrong in account creation")

@blueprint.route('/exists/<username>', methods=('GET',))
@limiter.exempt
def username_exists(username):
    """Check whether a user with the given name already exists. This is intended
    to be used by clients to save on failed "create account" attempts."""

    # TODO: We need to rate-limit this, handle banned users, etc.
    return jsonify(account_controllers.account_exists(username)), 200

@blueprint.route('/login', methods=('POST',))
@limiter.limit("1/second")
@use_args(request_schema.login)
def login(args):
    """Attempt to log into the Pixeme site through the REST API."""

    username = args['username']
    password = args['password']

    account = account_controllers.login_username_password(username, password)

    # TODO: Server setting (possible name: 'login_expiration_days') to control how long
    # until the refresh token expires. The access tokens have a short duration by default
    # (only 15 minutes), so the client is expected to periodically call the refresh
    # endpoint defined below to get a "fresh" access token.
    access = create_access_token(username)
    refresh = create_refresh_token(username)
    return jsonify(access_token=access, refresh_token=refresh, role=account.role.name), 200

@blueprint.route('/logout', methods=('POST',))
@account_required
@update_activity
@limiter.exempt
def logout():
    """Attempt to log out of this server."""

    result = account_controllers.logout()
    return jsonify(logout=g.account.username), 200

@blueprint.route('/refresh', methods=('POST',))
@jwt_refresh_token_required
@limiter.limit("1/second")
def refresh():
    """Refresh an API access token, given a valid JWT refresh token."""

    # TODO: Is the default expiration time okay?
    username = get_jwt_identity()
    token = create_access_token(username)
    return jsonify(access_token=token), 200

@blueprint.route('/name/<name>/role', methods=('POST','PUT'))
@admin_required
@update_activity
@use_kwargs(request_schema.change_role)
def change_user_role(name, admin_id, user_id, new_role):
    """Change the role of a user's account to normal, moderator, admin, etc.
    This is an admin-only endpoint."""

    # Sanity check that the admin knows the correct user ID.
    named = Account.query.filter_by(username=name).first()
    if named.user.id != user_id:
        raise APIError(message="Incorrect username or ID")

    result = account_controllers.change_user_role(user_id, new_role)

    # Nothing to return here, AFAICT
    return '', 204

@blueprint.route('/name/<name>/ban', methods=('POST',))
@admin_required
@update_activity
@use_kwargs(request_schema.ban_or_unban)
def ban_user(name, admin_id, user_id):
    """Ban a user's account. This prevents that user from logging in or
    doing any kind of actions that require authentication, but does *not*
    prevent simply viewing the site, or even accessing unprotected APIs."""

    # Sanity check that the admin knows the correct user ID.
    named = Account.query.filter_by(username=name).first()
    if named.user.id != user_id:
        raise APIError(message="Incorrect username or ID")

    result = account_controllers.change_user_role(user_id, 'banned')

    # Nothing to return here, AFAICT
    return '', 204

@blueprint.route('/name/<name>/unban', methods=('POST',))
@admin_required
@update_activity
@use_kwargs(request_schema.ban_or_unban)
def unban_user(name, admin_id, user_id):
    """Remove a user's ban. That user does not retain any excess privileges
    from before the ban, such as moderator status, but can now access APIs
    that require login."""

    # Sanity check that the admin knows the correct user ID.
    named = Account.query.filter_by(username=name).first()
    if named.user.id != user_id:
        raise APIError(message="Incorrect username or ID")

    result = account_controllers.change_user_role(user_id, 'user')

    # Nothing to return here, AFAICT
    return '', 204

@blueprint.route('/name/<name>', methods=('DELETE',))
@admin_required
@update_activity
@use_kwargs(request_schema.ban_or_unban)
def delete_account(name, admin_id, user_id):
    """Delete a user's account. This actually does remove it from the database,
    so it should be used with caution."""

    # Sanity check that the admin knows the correct user ID.
    named = Account.query.filter_by(username=name).first()
    if named.user.id != user_id:
        raise APIError(message="Incorrect username or ID")

    result = account_controllers.delete_account(user_id)

    # Nothing to return here
    return '', 204

@blueprint.route('/name/<name>/password', methods=('PUT',))
@account_required
@update_activity
@limiter.limit("5/minute")
@use_kwargs(request_schema.change_password_api)
def change_password(name, password):
    """Change a user's password to the one given. This assumes that the user
    is already logged in."""

    named = Account.query.filter_by(username=name).first()
    if named is None:
        raise NoSuchUser(id=name)

    # Remember: always use this method to change the password.
    # *Never* store it anywhere, even in memory.
    named.set_password(password)

    return '', 204