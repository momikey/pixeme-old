# API endpoints for dealing with images. These are the foundation
# of Pixeme, so we'll have a lot of them.

import os
# Import the Pillow Image class with a different name, so we don't
# overlap with the Pixeme model class.
from PIL import Image as PILImage

from flask import (Blueprint, current_app, g, jsonify, request,
    make_response, send_file)
from flask_jwt_extended import (create_access_token, jwt_required,
    get_jwt_identity, create_refresh_token, jwt_refresh_token_required)
from flask_uploads import UploadNotAllowed
from webargs.flaskparser import use_args, use_kwargs

from pixeme import db, jwt, executor, uploads_set, limiter
from pixeme.models import (Account, AccountRole, Image, Language, Phrase,
    PhraseStatus, Tag)
from pixeme.schema import request_schema, response_schema
from pixeme.helpers import printable_flake_id, update_last_activity
from pixeme.error import *

import pixeme.controllers.image as image_controllers
from .account import admin_required, account_required, update_activity

from . import API_PATH

# Each API module has its own path
BLUEPRINT_PATH = "{api}/image".format(api=API_PATH)

blueprint = Blueprint('image', __name__, url_prefix=BLUEPRINT_PATH)

# API blueprints need some custom error handling.
@blueprint.errorhandler(429)
def handle_limit_exceeded(e):
    raise RateLimitExceeded(e.description)

### Routes ###

@blueprint.route('/', methods=('POST',))
@account_required
@update_activity
@use_args(request_schema.create_image, location='form')
def create_new_image(args):
    """Create a new image, with a descriptive phrase, caption, and optional
    tags and technical info. Note that this route requires these fields to be
    in form data, not a JSON body. This is because we're expecting an image
    upload, and it's not trivial to combine that with a body."""

    # Get the file out of the request and send it to the controller.
    if 'image' in request.files:
        im = request.files['image']
        try:
            image, url = image_controllers.create_image_from_upload(im, **args)

            return (
                jsonify(response_schema.CreateImage().dump(image)),
                201,
                [('Location', url)]
            )
        except UploadNotAllowed as error:
            raise APIError(415, "Can't upload files of this type")

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_image_by_id(id):
    """Get an image entity from the DB. This is largely for the benefit
    of client software; real people will use the URLs we generate to get
    the image itself."""

    image = image_controllers.get_single_image(id=id)

    if image is not None:
        return jsonify(response_schema.SingleImage().dump(image)), 200
    else:
        raise NotFound(type='image', additional="no known image with this ID")

@blueprint.route('/tag/<tag>', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_images_with_tag(args, **kwargs):
    """Get the images that have been given a specific tag, with optinoal
    query parameters."""

    image_list = image_controllers.get_images_list(tag=kwargs['tag'], **args)

    response = [response_schema.SingleImage().dump(i) for i in image_list]
    return jsonify(images=response), 200

@blueprint.route('/new', methods=('GET',))
@use_kwargs(request_schema.count, location='query')
def get_newest_images(count):
    """Get the newest N images. (default 10)"""

    images = image_controllers.get_newest_images(count=count)

    response = [response_schema.SingleImage().dump(i) for i in images]
    return jsonify(images=response), 200

@blueprint.route('/random', methods=('GET',))
@use_kwargs(request_schema.count, location='query')
def get_random_images(count):
    """Get a random sample of N images. (default 10)"""


    images = image_controllers.get_random_images_sample(max_count=count)

    response = [response_schema.SingleImage().dump(i) for i in images]
    return jsonify(images=response), 200

@blueprint.route('/id/<int:id>/phrases', methods=('GET',))
def get_image_phrases(id):
    """Get the phrases associated with an image."""

    image = image_controllers.get_single_image(id=id)

    if image is not None:
        return jsonify(
            phrases=[response_schema.ImagePhrases().dump(p) for p in image.phrases]
        ), 200
    else:
        raise NotFound(type='image', additional="no known image with this ID")

@blueprint.route('/id/<int:id>/examples', methods=('GET',))
def get_image_examples(id):
    """Get the examples associated with an image."""

    image = image_controllers.get_single_image(id=id)

    if image is not None:
        return jsonify(
            examples=[response_schema.ImageExamples().dump(p) for p in image.examples]
        ), 200
    else:
        raise NotFound(type='image', additional="no known image with this ID")

@blueprint.route('/id/<int:id>/phrases', methods=('POST',))
@account_required
@update_activity
@use_kwargs(request_schema.add_image_phrase)
def add_image_phrase(id, image_id, language_id, text):
    """Add a phrase to an existing image."""

    # Sanity check.
    if id != image_id:
        raise APIError(400, 'Invalid image ID in request')

    phrase = image_controllers.add_phrase_to_image(id, language_id, text)

    return jsonify(response_schema.SinglePhrase().dump(phrase)), 200

@blueprint.route('/id/<int:id>/tags', methods=('POST','PUT'))
@account_required
@update_activity
@use_kwargs(request_schema.add_image_tags)
def add_image_tags(id, image_id, tags):
    """Add tags to an image."""

    # Sanity check.
    if id != image_id:
        raise APIError(400, 'Invalid image ID in request')

    image = image_controllers.add_tags_to_image(id, tags)

    return jsonify(response_schema.SingleImage().dump(image)), 200

@blueprint.route('/id/<int:id>/tags/<tag>', methods=('DELETE',))
@admin_required
@update_activity
def remove_tag_from_image(id, tag):
    """Remove a tag from an image."""

    # Note that the controller allows us to delete multiple tags
    # at once, but this API is one at a time.
    image = image_controllers.remove_tags_from_image(id, tag)

    if image is not None:
        return '', 204
    else:
        raise APIError(message="Something went wrong")

@blueprint.route('/id/<int:id>/metadata', methods=('POST','PUT'))
@account_required
@update_activity
@use_kwargs(request_schema.update_image_metadata)
def update_image_metadata(id, image_id, metadata):
    """Update an image's metadata."""

    # Sanity checks.
    if id != image_id:
        raise APIError(400, 'Invalid image ID in request')

    image = image_controllers.update_image_metadata(id, metadata)

    return jsonify(response_schema.SingleImage().dump(image)), 200

@blueprint.route('/id/<int:id>', methods=('DELETE',))
@account_required
@update_activity
def delete_image_with_id(id):
    """Delete the image with the given ID. Admins can do this for any image,
    while users are only allowed to delete their own."""

    result = image_controllers.delete_image_by_id(id)

    if result:
        return '', 204
    else:
        raise APIError(message="Something went wrong")