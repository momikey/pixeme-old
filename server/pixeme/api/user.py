# API endpoints for users, profiles, etc.

from flask import Blueprint, current_app, jsonify, request
from flask_jwt_extended import (create_access_token, jwt_required,
    get_jwt_identity, create_refresh_token, jwt_refresh_token_required)
from webargs.flaskparser import use_args, use_kwargs

from pixeme import db, jwt, limiter
from pixeme.models import AccountRole, User
from pixeme.schema import request_schema, response_schema
from pixeme.error import *

import pixeme.controllers.user as user_controllers

from .account import account_required, update_activity

from . import API_PATH

# Each API module has its own path
BLUEPRINT_PATH = "{api}/user".format(api=API_PATH)

blueprint = Blueprint('user', __name__, url_prefix=BLUEPRINT_PATH)

# API blueprints need some custom error handling.
@blueprint.errorhandler(429)
def handle_limit_exceeded(e):
    raise RateLimitExceeded(e.description)

### Routes ###

@blueprint.route('/', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_users(args):
    """Get a list of users, optionally specifying query paramaters."""

    users = user_controllers.get_users_list(**args)

    response = [response_schema.UsersList().dump(u) for u in users]

    return jsonify(users=response), 200

@blueprint.route('/count', methods=('GET',))
def get_user_count():
    """Get the total number of active, non-banned users."""

    response = user_controllers.get_user_count()

    return jsonify(response), 200

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_user_by_id(id):
    """Get a user by ID."""

    user = user_controllers.get_single_user(id=id)

    # The user with the given ID may not exist, in which case we give a 404.
    # Banned users instead cause a 403 Forbidden, because you're not supposed
    # to access their information, but they're not actually deleted from the DB.
    # (In the future, if we do remove users for whatever reason, we'll need
    # to add a 410 Gone responder, or possibly a 451.)
    if user is None:
        raise NotFound(type='user', additional="no user with this ID exists")
    elif user.account.role == AccountRole.banned:
        raise UserBanned(user)
    else:
        return jsonify(response_schema.SingleUser().dump(user)), 200

@blueprint.route('/new', methods=('GET',))
@use_args(request_schema.count, location='query')
def get_new_users(args):
    """Get a list of users sorted with the newest first."""

    users = user_controllers.get_newest_users(args['count'])

    response = [response_schema.UsersList().dump(u) for u in users]

    return jsonify(users=response), 200

@blueprint.route('/active', methods=('GET',))
@use_args(request_schema.count, location='query')
def get_most_active_users(args):
    """Get a list of the most active users, sorted by activity."""

    users = user_controllers.get_most_active_users(args['count'])

    # TODO: Should we include the activity "score" in the response?
    # We'd need to make a new schema.
    response = [response_schema.UsersList().dump(u[0]) for u in users]

    return jsonify(users=response), 200

@blueprint.route('/id/<int:id>/name', methods=('PUT',))
@account_required
@update_activity
@use_kwargs(request_schema.change_display_name)
def change_display_name(id, user_id, name):
    """Change the name of the logged-in user."""

    # Sanity check to keep a user from accidentally or maliciously
    # changing another user's name.
    if id != user_id:
        raise WrongUser()

    user = user_controllers.set_name_and_profile(id, name=name)

    response = response_schema.SingleUser().dump(user)

    return jsonify(user=response), 200

@blueprint.route('/id/<int:id>/fluencies', methods=('PUT',))
@account_required
@update_activity
@use_kwargs(request_schema.change_fluencies)
def change_fluencies(id, user_id, fluencies):
    """Change the claimed language fluencies of the logged-in user."""

    # Sanity check.
    if id != user_id:
        raise WrongUser()

    user = user_controllers.set_user_fluencies(id=id, fluencies=fluencies)

    response = response_schema.SingleUser().dump(user)

    return jsonify(user=response), 200

@blueprint.route('/id/<int:id>/profile', methods=('PUT',))
@account_required
@update_activity
@use_kwargs(request_schema.update_profile)
def update_profile(id, user_id, profile):
    """Update the user's profile."""

    # TODO Define which fields are actually used by the site.

    # Sanity check.
    if id != user_id:
        raise WrongUser()

    user = user_controllers.set_name_and_profile(id, name=None, profile=profile)

    response = response_schema.SingleUser().dump(user)

    return jsonify(user=response), 200

@blueprint.route('/id/<int:id>/name-and-profile', methods=('PUT',))
@account_required
@update_activity
@use_kwargs(request_schema.update_name_and_profile)
def update_name_and_profile(id, user_id, name, profile):
    """Update the user's display name and profile at the same time.
    This is more of a convenience endpoint to save on API calls."""

    # TODO Define which fields are actually used by the site.

    # Sanity check.
    if id != user_id:
        raise WrongUser()

    user = user_controllers.set_name_and_profile(id, name=name, profile=profile)

    response = response_schema.SingleUser().dump(user)

    return jsonify(user=response), 200
