# API endpoints for dealing with phrases

from flask import (Blueprint, current_app, jsonify, request,
    make_response, url_for)
from flask_jwt_extended import (create_access_token, jwt_required,
    get_jwt_identity, create_refresh_token, jwt_refresh_token_required)
from webargs.flaskparser import use_args, use_kwargs

from pixeme import db, jwt, limiter
from pixeme.models import (Phrase, PhraseSchema, PhraseStatus, AccountRole)
from pixeme.schema import request_schema, response_schema
from pixeme.helpers import update_last_activity
from pixeme.error import *

import pixeme.controllers.phrase as phrases_controllers

from .account import account_required, admin_required, update_activity

from . import API_PATH

# Each API module has its own path
BLUEPRINT_PATH = "{api}/phrase".format(api=API_PATH)

blueprint = Blueprint('phrase', __name__, url_prefix=BLUEPRINT_PATH)

# API blueprints need some custom error handling.
@blueprint.errorhandler(429)
def handle_limit_exceeded(e):
    raise RateLimitExceeded(e.description)

### Routes ###

@blueprint.route('/', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_phrases(args):
    """Get a list of all phrases, optionally using query parameters to
    retrieve a selected subset."""

    phrases = phrases_controllers.get_phrases_list(**args)

    response = [response_schema.PhrasesList().dump(p) for p in phrases]

    return jsonify(phrases=response), 200

@blueprint.route('/', methods=('POST',))
@admin_required
@update_activity
@use_kwargs(request_schema.add_empty_phrase)
def add_phrase(text, status, language_id, submitter_id):
    """Add a new phrase that is not accompanied by or attached to an image.
    This is technically an admin-only action, as users are intended to add
    phrases by posting or updating images."""

    phrase = phrases_controllers.add_phrase(
        text=text,
        status=status,
        language_id=language_id,
        submitter_id=submitter_id
    )

    if phrase is None:
        # This phrase already exists in the database, so that's a 409 Conflict.
        raise APIError(409, message="This phrase already exists")
    else:
        response = response_schema.PhraseSchema().dump(phrase)

        return (
            jsonify(phrase=response),
            201,
            [('Location', url_for('phrase.get_phrase_by_id', id=phrase.id))]
        )

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_phrase_by_id(id):
    """Get a phrase by its database ID."""

    phrase = phrases_controllers.get_phrase_by_id(id)

    if phrase is not None:
        return jsonify(response_schema.PhraseSchema().dump(phrase)), 200
    else:
        raise NotFound(type='phrase', additional='no phrase with this ID')

@blueprint.route('/id/<int:id>', methods=('PUT',))
@account_required(minimum=AccountRole.moderator)
@update_activity
@use_args(request_schema.update_phrase)
def update_phrase(args, id=None):
    """Update the text or status or both for a phrase."""

    # Sanity check.
    if id != args['id']:
        raise APIError(409, message="Incorrect ID in request")

    del args['id']
    phrase = phrases_controllers.update_phrase(id, **args)

    if phrase is not None:
        response = response_schema.PhraseSchema().dump(phrase)
        return jsonify(phrase=response), 200
    else:
        raise NotFound(type='phrase', additional='no phrase with this ID')

@blueprint.route('/id/<int:id>', methods=('DELETE',))
@account_required(minimum=AccountRole.moderator)
@update_activity
def delete_phrase(id):
    """Delete a phrase. This action is reserved for moderators and admins."""

    phrase = phrases_controllers.delete_phrase(id)
    return '', 204

@blueprint.route('/id/<int:id>/images', methods=('GET',))
def get_images_for(id):
    """Get a list of the images associated with this phrase."""

    phrase = phrases_controllers.get_phrase_by_id(id)

    if phrase is not None:
        response = [response_schema.PhraseImages().dump(i) for i in phrase.images]
        return jsonify(images=response), 200
    else:
        raise NotFound(type='phrase', additional='no phrase with this ID')

@blueprint.route('/search', methods=('GET',))
@use_args(request_schema.phrase_search, location='query')
def search(args):
    """Search for phrases with the given text."""

    phrases = phrases_controllers.search_similar(text=args['q'], count=args['count'])

    response = [response_schema.PhrasesList().dump(p) for p in phrases]
    return jsonify(results=response), 200

@blueprint.route('/id/<int:id>/status', methods=('PUT',))
@account_required(minimum=AccountRole.moderator)
@update_activity
@use_args(request_schema.update_phrase_status)
def update_status(args, id=None):
    """Update the status of a phrase."""

    # Sanity check.
    if id != args['id']:
        raise APIError(409, message="Incorrect ID in request")

    phrase = phrases_controllers.get_phrase_by_id(id)

    if phrase is not None:
        phrase = phrases_controllers.change_phrase_status(id, args['status'])
        response = response_schema.PhraseSchema().dump(phrase)
        return jsonify(phrase=response), 200
    else:
        raise NotFound(type='phrase', additional='no phrase with this ID')
