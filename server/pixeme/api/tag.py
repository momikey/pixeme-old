# API endpoints directly dealing with image tags

from flask import (Blueprint, current_app, jsonify, request,
    make_response, url_for)
from flask_jwt_extended import (create_access_token, jwt_required,
    get_jwt_identity, create_refresh_token, jwt_refresh_token_required)
from webargs.flaskparser import use_args, use_kwargs

from pixeme import db, jwt, limiter
from pixeme.models import (Tag, Account, AccountRole, TagSchema)
from pixeme.schema import request_schema, response_schema
from pixeme.helpers import update_last_activity
from pixeme.error import *

import pixeme.controllers.tag as tags_controllers

from .account import account_required, admin_required, update_activity

from . import API_PATH

# Each API module has its own path
BLUEPRINT_PATH = "{api}/tag".format(api=API_PATH)

blueprint = Blueprint('tag', __name__, url_prefix=BLUEPRINT_PATH)

# API blueprints need some custom error handling.
@blueprint.errorhandler(429)
def handle_limit_exceeded(e):
    raise RateLimitExceeded(e.description)

### Routes ###

@blueprint.route('/', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_tags(args):
    """Get a list of all known tags, optionally using query parameters."""

    tags = tags_controllers.get_tags_list(**args)

    response = [response_schema.TagsList().dump(t) for t in tags]

    return jsonify(tags=response), 200

@blueprint.route('/text/<text>', methods=('GET',))
def get_tag_by_text(text):
    """Get a tag by its name."""

    tag = tags_controllers.get_single_tag(text=text)

    # Either the tag exists or it doesn't.
    if tag is not None:
        return jsonify(response_schema.TagSchema().dump(tag)), 200
    else:
        raise NotFound(type='tag', additional="no tag with this text")

@blueprint.route('/text/<text>', methods=('POST',))
@admin_required
@update_activity
def add_new_tag(text):
    """Add a new "empty" tag, one not connected to any images."""

    tag = tags_controllers.add_tag(text=text)

    if tag is not None:
        # New tag, so return the created record.
        response = response_schema.SingleTag().dump(tag)
        return jsonify(tag=response), 201, [('Location', url_for('tag.get_tag_by_text', text=text))]
    else:
        # A tag with that text already exists.
        # Strictly speaking, if this is a POST, it should be a 409 Conflict error.
        raise APIError(409, message="This tag already exists")

@blueprint.route('/text/<text>', methods=('DELETE',))
@admin_required
@update_activity
def delete_tag(text):
    """Delete the tag with the given text. This is an admin-only action
    that should only be used for tags whose content is against ToS."""

    # The controller will raise a 404 if we're trying to delete a tag that doesn't exist.
    tag = tags_controllers.delete_tag(text)
    return '', 204

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_tag_by_id(id):
    """Get a tag by its database ID."""

    tag = tags_controllers.get_single_tag(id=id)

    # Either the tag exists or it doesn't.
    if tag is not None:
        return jsonify(response_schema.TagSchema().dump(tag)), 200
    else:
        raise NotFound(type='tag', additional="no tag with this ID")

@blueprint.route('/new', methods=('GET',))
@use_args(request_schema.count, location='query')
def get_new_tags(args):
    """Get a list of the newest tags, with an optional query parameter for
    the number to retrive."""

    tags = tags_controllers.get_newest_tags(args['count'])

    response = [response_schema.TagsList().dump(t) for t in tags]
    return jsonify(tags=response), 200

@blueprint.route('/popular', methods=('GET',))
@use_args(request_schema.count, location='query')
def get_popular_tags(args):
    """Get a list of the most popular tags, with an optional query
    parameter for the number to retrieve. Popularity is determined
    by the number of images with this tag."""

    tags = tags_controllers.get_most_popular_tags(args['count'])

    # TODO: Should we return the popularity, too?
    response = [response_schema.TagsList().dump(t[0]) for t in tags]
    return jsonify(tags=response), 200

@blueprint.route('/search', methods=('GET',))
@use_args(request_schema.tag_search, location='query')
def search(args):
    """Search for tags matching the given query text."""

    tags = tags_controllers.search_similar(text=args['q'], count=args['count'])

    response = [response_schema.TagsList().dump(t) for t in tags]
    return jsonify(results=response), 200
