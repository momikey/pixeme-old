# API endpoints for language data

from flask import (Blueprint, current_app, jsonify, request,
    make_response)
from flask_jwt_extended import (create_access_token, jwt_required,
    get_jwt_identity, create_refresh_token, jwt_refresh_token_required)
from webargs.flaskparser import use_args, use_kwargs

from pixeme import db, jwt, limiter
from pixeme.models import (Language, Account, AccountRole, LanguageSchema)
from pixeme.schema import request_schema, response_schema
from pixeme.helpers import update_last_activity
from pixeme.error import *

from .account import admin_required, account_required, update_activity

import pixeme.controllers.language as language_controllers

from . import API_PATH

# Each API module has its own path
BLUEPRINT_PATH = "{api}/language".format(api=API_PATH)

blueprint = Blueprint('language', __name__, url_prefix=BLUEPRINT_PATH)

# API blueprints need some custom error handling.
@blueprint.errorhandler(429)
def handle_limit_exceeded(e):
    raise RateLimitExceeded(e.description)

### Routes ###

@blueprint.route('/', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_languages(args):
    """Get a list of languages, optionally using query parameters."""

    languages = language_controllers.get_languages_list(**args)

    response = [response_schema.LanguagesList().dump(l) for l in languages]

    return jsonify(languages=response), 200

@blueprint.route('/count', methods=('GET',))
def get_language_count():
    """Get the total number of languages known to Pixeme. This is a convenience
    endpoint intended for stats and the like."""

    return jsonify(language_controllers.get_language_count()), 200

@blueprint.route('/id/<int:id>', methods=('GET',))
def get_language_by_id(id):
    """Get a language by its ID."""

    language = language_controllers.get_single_language_id(id)

    # This has only two possible outcomes. Either the language exists in
    # the database, and we return it; or it doesn't, and we give a 404.
    if language is not None:
        return jsonify(response_schema.SingleLanguage().dump(language)), 200
    else:
        raise NotFound(type='language', additional="no known language with this ID")

@blueprint.route('/tag/<tag>', methods=('GET',))
def get_language_by_tag(tag):
    """Get a language by its tag. Note that this is the full and exact
    language tag."""

    language = language_controllers.get_single_language(tag=tag)

    if language is not None:
        return jsonify(response_schema.SingleLanguage().dump(language)), 200
    else:
        raise NotFound(type='language', additional="no language with tag {tag}".format(
            tag=tag))

@blueprint.route('/group/<group>', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_language_group(args, **kwargs):
    """Get a "group" of languages all sharing the same primary language tag.
    Note: for constructed languages with tags in the private use area, the idea
    of a group is ill-defined, so we issue an error for now."""

    languages = language_controllers.get_language_group(kwargs['group'], **args)

    if len(languages) == 0:
        raise NotFound(type='language', additional="no languages in this group found")

    response = [response_schema.LanguagesList().dump(l) for l in languages]
    return jsonify(languages=response), 200

@blueprint.route('/group/', methods=('GET',))
@use_args(request_schema.query, location='query')
def get_language_groups(args):
    """Get the list of langauge groups, optionally using query parameters."""

    group_list = language_controllers.get_language_groups_list(**args)

    return jsonify(groups=[g.primary for g in group_list]), 200

@blueprint.route('/popular', methods=('GET',))
@use_args(request_schema.count, location='query')
def get_most_popular_languages(args):
    """Get the list of most popular languages, with an optional count as a query parameter."""

    languages = language_controllers.get_most_popular_languages(args['count'])

    # TODO: Maybe send the popularity score? It's just the number of fluent
    # speakers, so it's probably not that important. We can always make another
    # API call for that.
    response = [response_schema.LanguagesList().dump(l[0]) for l in languages]
    return jsonify(languages=response), 200

@blueprint.route('/', methods=('POST',))
@admin_required    # Only admins can create new language tags
@update_activity
@use_args(request_schema.create_language)
def create_language(args):
    """Create a new language record in the DB, including IETF language tag,
    English and native names."""

    tag = args['tag']
    english = args['english']
    # The native name isn't required (conlangs might not have it, for instance).
    # If not provided, use the English name instead.
    native = args.get('native', english)

    language = language_controllers.create_language(tag, english, native)

    response = LanguageSchema().dump(language)

    return jsonify(response), 201

@blueprint.route('/id/<int:lang_id>', methods=('PUT',))
@admin_required     # Only admins should be able to update language tags.
@use_kwargs(request_schema.update_language)
def update_language(lang_id, id, tag, english, native):
    """Update the database entry for a language."""

    if lang_id != id:
        raise APIError(400, 'Invalid language ID in request')

    language = language_controllers.update_language(lang_id, tag, english, native)

    response = LanguageSchema().dump(language)

    return jsonify(response), 200
    
@blueprint.route('/id/<int:lang_id>', methods=('DELETE',))
@admin_required    # Only admins should be able to delete language entries.
@update_activity
def delete_language(lang_id):
    """Delete the database entry for a language."""

    # TODO: We may need some confirmation for this, but isn't that client side?

    language = language_controllers.delete_language(lang_id)

    return '', 204

@blueprint.route('/search', methods=('GET',))
@use_args(request_schema.language_search, location='query')
def search(args):
    """Search for languages matching the given text."""

    languages = language_controllers.search_similar(text=args['q'], count=args['count'])

    response = [response_schema.LanguagesList().dump(l) for l in languages]
    return jsonify(results=response), 200