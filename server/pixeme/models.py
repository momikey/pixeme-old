import re
from datetime import datetime
from enum import Enum

from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_enum import EnumField
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.sql import expression
from sqlalchemy.types import TIMESTAMP
from werkzeug.security import check_password_hash, generate_password_hash

# Create the DB object, so we can build models from it.
# (Note that we set it up in the main app.)
db = SQLAlchemy()

# Create the Marshmallow object, which we can use for schema generation
# and easy serialization/deserialization. (This *must* be initialized
# after SQLAlchemy)
ma = Marshmallow()

# UTC timestamp handling
# Pixeme only supports Postgres, so we don't have to worry about any other DBs.
class utcnow(expression.FunctionElement):
    """UTC timestamp expression class, as per SQLAlchemy docs"""
    type = TIMESTAMP()

@compiles(utcnow, 'postgresql')
def utcnow_pg(element, compiler, **kwargs):
    """UTC timestamps for Postgres"""
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"

### Enums ###

class AccountRole(Enum):
    banned = 0
    user = 1
    moderator = 2
    admin = 3
    # Other roles as needed

class PhraseStatus(Enum):
    proposed = 0
    accepted = 1
    rejected = 2
    questioned = 3
    # Any others?

### Association tables for many-to-many relations ###

# User fluencies connect users to the languages they claim to speak at
# a native or fluent level.
user_fluencies = db.Table('user_fluencies',
    db.Column('user', db.Integer, db.ForeignKey('users.id'), primary_key=True),
    db.Column('language', db.Integer, db.ForeignKey('languages.id'), primary_key=True)
)

# Image phrases connect images to the different phrases (possibly in different
# languages) to which they have been linked.
image_phrases = db.Table('image_phrases',
    db.Column('image', db.Integer, db.ForeignKey('images.id'), primary_key=True),
    db.Column('phrase', db.Integer, db.ForeignKey('phrases.id'), primary_key=True)
)

# Image tags connect images to the tags assigned to them.
image_tags = db.Table('image_tags',
    db.Column('image', db.Integer, db.ForeignKey('images.id'), primary_key=True),
    db.Column('tag', db.Integer, db.ForeignKey('tags.id'), primary_key=True)
)

# Image examples connect images to any example sentences used to describe them.
image_examples = db.Table('image_examples',
    db.Column('image', db.Integer, db.ForeignKey('images.id'), primary_key=True),
    db.Column('example', db.Integer, db.ForeignKey('examples.id'), primary_key=True)
)

### Models ###

class Account(db.Model):
    """A user account."""

    __tablename__ = 'accounts'
    MAX_USERNAME_LENGTH = 64

    id = db.Column(db.Integer, primary_key=True)

    # The username for this account.
    # NOTE: Maximum length is 64 characters. This has no real justification,
    # except that I don't like extremely long usernames.
    username = db.Column(db.String(MAX_USERNAME_LENGTH), unique=True, nullable=False)

    # The password hash for this account. (We're smart enough not to store
    # the password itself in plaintext.)
    password = db.Column(db.Text, nullable=False)

    # The email address associated with this account.
    # We make this unique for lookups, spam prevention, password resets, etc.
    # Forget about validation *inside* the DB, though. That's a lost cause.
    email = db.Column(db.Text, unique=True, nullable=False)

    # The date this account was created. We only use this for statisticsd,
    # and to show a "date joined" text on a profile page.
    date_created = db.Column(TIMESTAMP, nullable=True, server_default=utcnow())

    # The last login time for this account, null if the user has not logged in
    # We track this mostly so we can show stats like "active users". There is no
    # intention of violating the user's privacy.
    last_login = db.Column(TIMESTAMP, nullable=True)

    # The last activity time for this account, null if it has not performed any
    # Again, we only track this for stats purposes, not for advertising or other
    # malicious reasons.
    last_activity = db.Column(TIMESTAMP, nullable=True)

    # This account's role, such as user or admin
    # This will determine privileges.
    role = db.Column(db.Enum(AccountRole), server_default="user")

    ### Relations ###

    # Each account will have an associated "user" object, which will hold all
    # the relationships and similar data.
    user = db.relationship('User', back_populates='account')
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    # Then notifications this account has received. (These are tied to the account,
    # not the user profile, as they're considered site metadata.)
    notifications = db.relationship('Notification', back_populates='account')

    ### Methods ###

    def __repr__(self):
        return "<Account {self.username} ({self.email})>".format(self=self)

    def set_password(self, pw):
        """Hash a user's password to securely store in the database."""
        self.password = generate_password_hash(pw)

    def check_password(self, pw):
        """Check that a given password hashes to the known value, and is thus correct."""
        return check_password_hash(self.password, pw)

class User(db.Model):
    """A user of the Pixeme platform."""

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)

    # The user's preferred "display" name, which is not the same as the username.
    # If left blank, the username will be used instead.
    name =db.Column(db.String, nullable=True)

    # Miscellaneous profile data for this user, stored as JSON.
    profile = db.Column(MutableDict.as_mutable(JSONB))

    ### Relations ###

    # The relation back to the user's account.
    account = db.relationship('Account', uselist=False, back_populates='user')

    # The languages this user claims fluency in. This is used to determine who
    # may verify a translation or image label in a particular language.
    fluencies = db.relationship(
        'Language',
        secondary=user_fluencies,
        back_populates='speakers'
    )

    # The phrases this user has submitted.
    submitted = db.relationship('Phrase',
        back_populates='submitter',
        foreign_keys="[Phrase.submitter_id]"
    )

    # The phrases this user has approved.
    approved = db.relationship('Phrase',
        back_populates='approved_by',
        foreign_keys="[Phrase.approved_by_id]"
    )

    # The images this user has uploaded.
    images = db.relationship('Image',
        back_populates='uploader'
    )

    ### Methods ###

    def __repr__(self):
        return "<User {name} ({username})>".format(
            name=self.name,
            username=self.account.username
        )

class Language(db.Model):
    """Any language, natural or constructed."""

    __tablename__ = 'languages'

    id = db.Column(db.Integer, primary_key=True)

    # TODO: We'll probably need to incorporate the `language-tags` package
    # at some point, if only to keep the tags properly formatted.

    # The "primary" language code. This will be 2-3 letters. (Unofficially,
    # we can use the `qaa-qtz` private use space for conlangs. In practice,
    # I think the best bet will be to use `qcX` for these, with `X` being
    # the first letter of the conalng's English name.)
    #
    # Examples:
    # * `en` - English
    # * `ase` - American Sign Language (an example; don't ask how we'd show this)
    # * `qcv` - Virisai (one of my conlangs)
    primary = db.Column(db.String(3), nullable=False)

    # The "full" language tag for the language. This can be any length, and
    # it can include any of the subtags defined in RFC 5646, though we'll try
    # to limit that to within reason.
    #
    # Examples of natural language subtags:
    # * `en-US` - US English
    # * `de-CH` - Swiss German 
    # * `es-419` - Latin American Spanish (using a UN region code)
    # (TODO: Script and dialect subtags?)
    #
    # For conlangs, we'll take advantage of the `x-` private use subtags.
    # * `qcv-x-virisai` - my conlang from above
    # * `qci-x-ithkuil` - John Quijada's Ithkuil
    # etc.
    tag = db.Column(db.String, nullable=False)

    # The name of the language (or even dialect) in English. This will be shown
    # in many parts of the UI.
    english = db.Column(db.String, nullable=False)

    # The native name of the language. This may be written in a non-Latin script,
    # so we have to ensure Unicode can handle it.
    #
    # For conlangs whose scripts aren't in Unicode, it may be best to ask the users
    # to use the English name. We still won't make it a nullable field, though.
    native = db.Column(db.String, nullable=False)

    ### Relations ###

    # The users who claim fluency in this language.
    speakers = db.relationship(
        'User',
        secondary=user_fluencies,
        back_populates='fluencies'
    )

    # All submitted phrases in this language.
    phrases = db.relationship('Phrase', back_populates='language')

    # (TODO: Should we make a "translations" relation here? Something to map languages
    # to their names in *other* languages? That would be necessary once we expand, IMO.)

    ### Methods ###

    def __repr__(self):
        return '<Language {english} | {native} ({tag})>'.format(
            english=self.english,
            native=self.native,
            tag=self.tag
        )

    ### Table metadata ###

    # Add GIN indexes to the "english" and "native" columns. This is to
    # enable fast trigram searching.
    __table_args__ = (
        db.Index(
            'name_english_idx', 'english',
            postgresql_using='gin',
            postgresql_ops={"english": "gin_trgm_ops"}
        ),
        db.Index(
            'name_native_idx', 'native',
            postgresql_using='gin',
            postgresql_ops={"native": "gin_trgm_ops"}
        )
    )

class Phrase(db.Model):
    """A word or phrase used as a Pixeme picture caption."""

    __tablename__ = 'phrases'

    id = db.Column(db.Integer, primary_key=True)

    # The text of the phrase. This can be in any language and script.
    text = db.Column(db.Text, nullable=False)

    # The current moderation status of this phrase.
    status = db.Column(db.Enum(PhraseStatus), server_default='proposed')

    ### Relations ###

    # The language of this phrase.
    language = db.relationship('Language', back_populates='phrases')
    language_id = db.Column(db.Integer, db.ForeignKey('languages.id'), nullable=False)

    # The user who submitted this phrase.
    submitter_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    submitter = db.relationship('User',
        back_populates='submitted',
        foreign_keys=[submitter_id]
    )

    # The user who approved this phrase. This is NULL if not yet approved.
    approved_by_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    approved_by = db.relationship('User',
        back_populates='approved',
        foreign_keys=[approved_by_id]
    )

    # The example translations linked to this phrase.
    translations = db.relationship('Example', back_populates='phrase')

    # The images linked to this phrase.
    images = db.relationship('Image',
        secondary=image_phrases,
        back_populates='phrases'
    )

    ### Methods ###

    def __repr__(self):
        return '<Phrase "{text}" ({status})>'.format(
            text=self.text,
            status=self.status.name
        )

    ### Table metadata ###

    # Phrases can be searched, so we'll add a trigram index for them, too.
    __table_args__ = (
        db.Index(
            'phrase_text_idx', 'text',
            postgresql_using='gin',
            postgresql_ops={"text": "gin_trgm_ops"}
        ),
    )

class Example(db.Model):
    """An example sentence using a particular phrase."""

    __tablename__ = 'examples'

    id = db.Column(db.Integer, primary_key=True)

    # The text of the translation in the source language.
    source_text = db.Column(db.Text, nullable=False)

    # The text of the translation in the target languages.
    target_text = db.Column(db.Text, nullable=False)

    ### Relations ###

    # The phrase this example references.
    phrase = db.relationship('Phrase', back_populates='translations')
    phrase_id = db.Column(db.Integer, db.ForeignKey('phrases.id'), nullable=False)

    # The language this example is translating into. We use `backref` in the
    # relationship call because languages honestly shouldn't care, but we
    # still want to keep that link.
    target_language = db.relationship('Language', backref='translations_to')
    target_language_id = db.Column(db.Integer, db.ForeignKey('languages.id'), nullable=False)

    # Any images that connect to this example sentence.
    images = db.relationship('Image',
        secondary=image_examples,
        back_populates='examples'
    )

    ### Methods ###

    def __repr__(self):
        return '<Translation of "{source}" to {language}>'.format(
            source=self.source_text,
            language=self.target_language.english
        )

class Image(db.Model):
    """A Pixeme image, with relations to captions, translations, etc."""

    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True)

    # The filename of the image; this is generated elsewhere in the code,
    # so we make it a required field with no default for safety reasons.
    filename = db.Column(db.String, nullable=False)

    # A caption, as provided by the uploader.
    caption = db.Column(db.Text, nullable=False)

    # A description of the image itself (such as original artist,
    # technical details, etc.)
    description = db.Column(db.Text, nullable=True)

    # Any miscellaneous metadata, stored as a JSON object.
    # We have to rename because SQLAlchemy stupidly errors out if we
    # try to do the Python thing and use sensible names.
    image_metadata = db.Column(MutableDict.as_mutable(JSONB))

    # An upload timestamp, for tracking and stats.
    timestamp = db.Column(TIMESTAMP, nullable=False, server_default=utcnow())

    ### Relations ###

    # The user who uploaded this image.
    uploader_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    uploader = db.relationship('User', back_populates='images')

    # The translation phrases for this image.
    phrases = db.relationship('Phrase',
        secondary=image_phrases,
        back_populates='images'
    )

    # Any tags that have been assigned to this image.
    tags = db.relationship('Tag',
        secondary=image_tags,
        back_populates='images'
    )

    # Example sentences for this image.
    examples = db.relationship('Example',
        secondary=image_examples,
        back_populates='images'
    )

    ### Methods ###

    def __repr__(self):
        return '<Image {filename} ({caption})>'.format(
            filename=self.filename,
            caption=self.caption
        )

class Tag(db.Model):
    """A plain text tag for classifying Pixeme images."""

    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)

    # The text of the tag. This can be in any language; we won't discriminate.
    text = db.Column(db.String, nullable=False, unique=True)

    ### Relations ###

    # The images that have been given this tag.
    images = db.relationship('Image',
        secondary=image_tags,
        back_populates='tags'
    )
    
    ### Methods ###

    def __repr__(self):
        return '<Tag "{text}">'.format(text=self.text)

    ### Table metadata ###

    # We add a Postgres trigram index to the text column to enable similarity search.
    __table_args__ = (
        db.Index(
            'tag_text_idx', 'text',
            postgresql_using='gin',
            postgresql_ops={"text": "gin_trgm_ops"}
        ),
    )

class Notification(db.Model):
    """A notification delivered to a user's account."""

    __tablename__ = 'notifications'

    id = db.Column(db.Integer, primary_key=True)

    # The text of the notification. This will be denormalized by design, as we
    # will need to localize notification text, as well as insert things like
    # image URLs. That does increase the size of the DB, unfortunately, but the
    # bset alternative would be a lot of gettext wrangling. Maybe later.
    text = db.Column(db.String, nullable=False)

    # Whether the notification has been read. We'll need to detect that in the
    # front end.
    read = db.Column(db.Boolean, nullable=False, default=False, server_default='false')

    # When this notification was sent.
    timestamp = db.Column(TIMESTAMP, nullable=False, server_default=utcnow())

    ### Relations ###

    # The account of the user who is to receive this notification.
    account = db.relationship('Account', back_populates='notifications')
    account_id = db.Column(db.Integer, db.ForeignKey('accounts.id'), nullable=False)

    ### Methods ###

    def __repr__(self):
        return '<Notification "{text}" for {account}>'.format(
            text=(self.text[:10] + '...') if len(self.text) > 10 else self.text,
            account=self.account.username
        )

### Schemas ###

# NOTE: The Marshmallow-SQLAlchemy package dropped support for Python 3.5, which is our
# minimum supported version. Thus, we have to rely on an older version of this package,
# v0.18.0, which doesn't contain the "AutoSchema" classes. In a domino effect, the Flask
# integration package for these two *also* doesn't have those classes, meaning that we
# must rely on the older, now-deprecated `ModelSchema` and `TableSchema`. The API is
# almost identical, as far as we're concerned. (The only real difference is in overriding
# fields: `field_for` vs. `auto_field`)

class AccountSchema(ma.ModelSchema):
    """Schema for an Account."""

    role = EnumField(AccountRole)

    class Meta:
        model = Account
        exclude = ('password',)

class LanguageSchema(ma.ModelSchema):
    """Schema for a Language."""

    class Meta:
        model = Language
        exclude = ('speakers','phrases')

class UserSchema(ma.ModelSchema):
    """Schema for a User."""

    account = ma.Nested(AccountSchema)
    fluencies = ma.Nested(LanguageSchema)

    class Meta:
        model = User
        exclude = ('submitted', 'approved')

class PhraseSchema(ma.ModelSchema):
    """Schema for a Phrase."""

    submitter = ma.Nested(UserSchema)
    status = EnumField(PhraseStatus)

    class Meta:
        model = Phrase

class ExampleSchema(ma.ModelSchema):
    """Schema for an Example."""

    phrase = ma.Nested(PhraseSchema)
    target_language = ma.Nested(LanguageSchema)

    class Meta:
        model = Example

class TagSchema(ma.ModelSchema):
    """Schema for a Tag."""

    class Meta:
        model = Tag
        only = ('id', 'text')

class ImageSchema(ma.ModelSchema):
    """Schema for an Image."""

    uploader = ma.Nested(UserSchema)
    phrases = ma.Nested(PhraseSchema, many=True)
    tags = ma.Nested(TagSchema, many=True)

    class Meta:
        model = Image

class NotificationSchema(ma.ModelSchema):
    """Schema for a Notification."""

    # Note that we don't nest the account here. There's no need to,
    # as notifications should always be linked with their targets.
    class Meta:
        model = Notification
        only = ('id', 'text', 'read', 'timestamp', 'account_id')