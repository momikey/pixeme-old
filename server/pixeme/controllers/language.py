###
# Controllers for handling language data
###

from sqlalchemy import func, or_

from pixeme import db
from pixeme.models import Language
from pixeme.error import APIError, NotYetImplemented

from .query import use_query_parameters

# Note that we split the "get single" functionality into multiple controllers
# for this module. That's because I'd rather handle all the code lookup,
# native vs. English naming, etc., in its own function.

def get_single_language_id(id):
    """Retrieve a single language by its database ID."""

    return Language.query.get(id)

def get_single_language(tag=None, name=None):
    """Retrieve a single language by its name (native or English) or
    its langauge code. Both fields are case-insensitive. Returns None
    if neither is provided."""

    if tag is not None:
        return Language.query.filter(Language.tag.ilike(tag)).first()
    elif name is not None:
        return Language.query.filter(or_(Language.english.ilike(name),
            Language.native.ilike(name))).first()
    else:
        return None

def get_language_count():
    """Get the total number of languages which have at least one fluent speaker."""

    from pixeme.models import user_fluencies

    subquery = db.session.query(user_fluencies.c.language).group_by(
        user_fluencies.c.language).subquery()

    language_count = Language.query.outerjoin(
        subquery, Language.id==subquery.c.language).count()

    return language_count

@use_query_parameters
def get_languages_list(**kwargs):
    """Retrieve a list of languages meeting given query criteria."""

    # TODO: Implement model-specific criteria
    return Language.query

@use_query_parameters
def get_language_group(group=None, **kwargs):
    """Retrieve a list of languages forming a "group". These are all languages
    known to the server which have the same primary code, such as 'en' for all
    dialects of English.

    For conlangs, this operation is not well-defined, so we currently raise
    a NotImplemented error. The view layer should detect this, and convert to
    an appropriate error page."""

    if len(group) == 3 and group[0:2] == 'qc':
        # Conlangs currently return an error
        raise NotYetImplemented()

    return Language.query.filter(Language.primary == group)

@use_query_parameters
def get_language_groups_list(**kwargs):
    """Retrieve a list of language groups: primary language tags representing
    the "standard" form of each language."""

    return db.session.query(Language.primary).filter(
        ~Language.primary.ilike('qc%')
    ).group_by(Language.primary)

def get_most_popular_languages(count=10):
    """Retrieve a list of the most "popular" languages: those having the most
    speakers claiming fluency. There are other ways of calculating this, but
    our method is the simplest."""

    from sqlalchemy import func
    from pixeme.models import user_fluencies

    counts = db.session.query(user_fluencies.c.language, func.count('*').label('speaker_count')) \
        .group_by(user_fluencies.c.language).subquery()

    languages = db.session.query(Language, func.coalesce(counts.c.speaker_count, 0)) \
        .outerjoin(counts, Language.id==counts.c.language) \
        .order_by(counts.c.speaker_count.desc())[:count]

    return languages

def create_language(tag=None, english=None, native=None):
    """Create a new Language entity with the given name and IETF language code.
    If the native name is ont given, it defaults to the English name."""

    if tag is None or english is None:
        raise APIError(message="Missing required fields")

    # Check to see if a language with this tag already exists.
    if get_single_language(tag=tag) is not None:
        raise APIError(message="A language with this code already exists")

    # The primary language field holds the highest-level IETF or ISO code.
    primary = tag.split('-')[0]

    language = Language(
        primary=primary,
        tag=tag,
        english=english,
        native=native if native is not None else english
    )

    db.session.add(language)
    db.session.commit()

    return language

def update_language(id, tag=None, english=None, native=None):
    """Update a language's database entry. There are very few reasons why we should
    need to do this; languages aren't often going to change names. Still, it's useful
    for cases when, e.g., we didn't originally have a native name."""

    # Get the existing language.
    language = Language.query.get(id)
    if language is None:
        raise NotFound(type='language', additional="cannot update a language that doesn't exist")

    # Update any existing fields.
    if tag is not None:
        language.tag = tag

        # The primary may also change, so recheck it.
        primary = tag.split('-')[0]
        if language.primary != primary:
            language.primary = primary

    if english is not None:
        language.english = english
    
    if native is not None:
        language.native = native

    db.session.add(language)
    db.session.commit()

    return language

def delete_language(id):
    """Delete a language from the database. This should only be done if it is
    a mistake, an inappropriate entry, or possibly a conlang that has been
    abandoned by its creator."""

    # Get the existing language.
    language = Language.query.get(lang_id)
    if language is None:
        raise NotFound(type='language', additional="cannot delete a language that doesn't exist")

    db.session.delete(language)
    db.session.commit()

    return language

def search_similar(text, count=10):
    """Search for the closest language matches to the given text."""

    from sqlalchemy.sql.operators import op

    languages = Language.query \
        .filter(or_(Language.english.ilike("%{}%".format(text)), Language.native.ilike("%{}%".format(text)))) \
        .order_by(func.greatest(
            func.similarity(Language.english, text), func.similarity(Language.native, text))) \
        [:count]

    return languages