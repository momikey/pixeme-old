###
# Controllers dealing with tags
###

from sqlalchemy import func

from pixeme import db
from pixeme.models import Tag
from pixeme.error import APIError, NotFound

from .query import use_query_parameters

def get_single_tag(id=None, text=None):
    """Retrieve a single tag with the given ID or text. If neither is
    provided, returns None. Further error handling is the job of the
    caller."""

    if id is not None:
        return Tag.query.get(id)
    elif text is not None:
        return Tag.query.filter_by(text=text).first()
    else:
        return None

@use_query_parameters
def get_tags_list(**kwargs):
    """Retrieve a query object for a list of tags meeting specific
    query critera."""

    tags = Tag.query

    # Optional search criteria:
    # 'text'        Search for tags with this text (case-insensitive)
    # 'image_id'    Search for tags on the given image

    if 'text' in kwargs:
        tags = tags.filter(Tag.text.ilike(kwargs['text']))
    
    if 'image_id' in kwargs:
        tags = tags.filter(Tag.images.any(id=kwargs['image_id']))

    return tags

def get_most_popular_tags(count=10):
    """Retrieve a list of the N most popular tags, where popularity is
    determined by the number of images with the tag."""

    from sqlalchemy import func
    from pixeme.models import image_tags

    tag_counts = db.session.query(image_tags.c.tag, func.count('*').label('popularity')) \
        .group_by(image_tags.c.tag).subquery()

    tags = db.session.query(Tag, func.coalesce(tag_counts.c.popularity, 0)) \
        .outerjoin(tag_counts, Tag.id==tag_counts.c.tag) \
        .order_by(tag_counts.c.popularity.desc())[:count]

    return tags

def get_newest_tags(count=10):
    """Retrieve a list of the N newest tags, assuming that the latest
    database IDs correspond to the newest."""

    return Tag.query.order_by(Tag.id.desc())[:count]

def search_similar(text, count=10):
    """Search for the closest tag matches to the given text."""

    from sqlalchemy.sql.operators import op

    tags = Tag.query.filter(Tag.text.ilike("%{}%".format(text))) \
        .order_by(func.similarity(Tag.text, text))[:count]

    return tags

def add_tag(text):
    """Add a new tag to the database without any accompanying images.
    This should be considered an admin-only action."""

    tag = get_single_tag(text=text)

    # If the tag already exists, do nothing.
    if tag is not None:
        return None

    tag = Tag(text=text)

    db.session.add(tag)
    db.session.commit()

    return tag

def delete_tag(text):
    """Delete a tag. This really only needs to be used for tags that are
    in some way illegal or destructive."""

    tag = get_single_tag(text=text)

    if tag is None:
        raise NotFound(type='tag', additional="can't remove a tag that doesn't exist")

    db.session.delete(tag)
    db.session.commit()

    return tag
