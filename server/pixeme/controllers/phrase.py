###
# Controllers for Pixeme "phrases", i.e., image captions
###

from sqlalchemy import func

from pixeme import db
from pixeme.models import Phrase, PhraseStatus
from pixeme.error import APIError, NotFound, NotYetImplemented

from .query import use_query_parameters

def get_phrase_by_id(id):
    """Get a phrase by its database ID. Returns None if the ID
    isn't in use."""

    return Phrase.query.get(id)

@use_query_parameters
def get_phrases_list(**kwargs):
    """Retrieve a query object for a list of phrases which meet
    the given query criteria."""

    phrases = Phrase.query

    # Optional search criteria:
    # 'language_id' Search for phrases in the given language
    # 'lang_id'     Synonym for 'language_id'
    # 'user_id'     Search for phrases posted by the given user
    # 'status'      Search for phrases with a specific status (accepted, etc.)
    # TODO: Implement other model-specific criteria

    if 'language_id' in kwargs:
        phrases = phrases.filter(Phrase.language_id == kwargs['language_id'])
    elif 'lang_id' in kwargs:
        phrases = phrases.filter(Phrase.language_id == kwargs['lang_id'])

    if 'user_id' in kwargs:
        phrases = phrases.filter(Phrase.submitter_id == kwargs['user_id'])

    if 'status' in kwargs:
        phrases = phrases.filter(Phrase.status == PhraseStatus[kwargs['status']])

    # TODO: Implement custom sorting
    return phrases.order_by(Phrase.id.asc())

def search_similar(text, count=10):
    """Search for the closest phrase matches for the given text,
    returning up to `count` results, sorted by similarity."""

    phrases = Phrase.query.filter(Phrase.text.ilike("%{}%".format(text))) \
        .order_by(func.similarity(Phrase.text, text))[:count]

    return phrases

def add_phrase(**kwargs):
    """Add a new phrase to the database without any accompanying image.
    This should be considered an admin-only action."""

    from sqlalchemy import and_

    # If a similar phrase in the same language already exists, do nothing.
    phrase = Phrase.query.filter(and_(Phrase.text == kwargs['text'],
        Phrase.language_id == kwargs['language_id'])).first()

    if phrase is not None:
        return None

    phrase = Phrase(
        text=kwargs['text'],
        status=PhraseStatus[kwargs['status']],
        submitter_id=kwargs['submitter_id'],
        language_id=kwargs['language_id']
    )

    db.session.add(phrase)
    db.session.commit()

    return phrase

def update_phrase(id, **kwargs):
    """Update the database entry for a phrase. This only allows changing
    certain "core" fields (text and status). Returns None for an invalid
    phrase ID, or the updated Phrase entity otherwise."""

    phrase = get_phrase_by_id(id)

    if phrase is not None:
        if 'text' in kwargs and kwargs['text'] != '':
            phrase.text = kwargs['text']
        if 'status' in kwargs:
            if isinstance(kwargs['status'], str):
                phrase.status = PhraseStatus[kwargs['status']]
            elif isinstance(kwargs['status'], int):
                phrase.status = PhraseStatus(kwargs['status'])
            elif isinstance(kwargs['status'], PhraseStatus):
                phrase.status = kwargs['status']
            else:
                raise TypeError("Unable to convert status value")

        db.session.add(phrase)
        db.session.commit()

    return phrase

def delete_phrase(id):
    """Delete a phrase from the database. This should be a very rare
    operation, only done by administrators after careful thought and
    probably some community discussion."""

    # Get the existing phrase.
    phrase = Phrase.query.get(id)
    if phrase is None:
        raise NotFound(type='phrase', additional="cannot delete a nonexistent phrase")

    db.session.delete(phrase)
    db.session.commit()

    return phrase

def change_phrase_status(id, status):
    """Change the accepted status of the phrase. This is mostly a shorthand
    for one use of the general phrase updating controller."""

    return update_phrase(id, status=status)