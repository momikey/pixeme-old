###
# Decorator for "query" arguments.
#
# These are the common operations to filter and sort a database result set,
# and they correspond to the SQL SELECT criteria: ORDER BY, OFFSET, etc.
#
# Routes using this should also use the `use_args` request decorator
# from webargs as such:
# > @use_args({request schema}, location='query')

from functools import wraps
import sqlalchemy_utils

def use_query_parameters(f):
    @wraps(f)
    def wrapper(*args, **kwargs):

        # Retrieve an SQLAlchemy query object from the wrapped controller.
        # This will be our working result set.
        results = f(*args, **kwargs)

        start = kwargs.get('start')
        count = kwargs.get('count')
        order = kwargs.get('order')
        desc = kwargs.get('desc')

        # Apply order by and sort order.
        if order is not None:
            sort = ('-' if desc else '') + order
            results = sqlalchemy_utils.sort_query()

        # Apply limit/offset.
        if count is not None:
            results = results[start:(start+count)]
        else:
            results = results[start:]

        # We're done.
        return results
    return wrapper