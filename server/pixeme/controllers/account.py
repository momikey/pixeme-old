###
# Controllers for handling user accounts and authentication.
###

import os
from datetime import datetime

from flask import current_app, g

from pixeme import db
from pixeme.models import Account, AccountRole, User
from pixeme.error import APIError
from pixeme.helpers import update_last_activity

from .query import use_query_parameters

def create_account_username_password(username=None, password=None, email=None):
    """Create a new Pixeme user account with the given username, password,
    and email address."""

    if username is None or password is None or email is None:
        raise APIError(message="Missing required field")

    # Check to see if someone already has this username or email address,
    # because these must be unique.
    existing_user = Account.query.filter_by(username=username).count()
    if existing_user > 0:
        raise APIError(message="A user with this name already exists")

    existing_email = Account.query.filter_by(email=email).count()
    if existing_email > 0:
        raise APIError(message="A user with this email address already exists")

    # Create the Account entity for the database.
    account = Account(
        username=username,
        email=email,
        role=AccountRole.user,
        date_created=datetime.utcnow()
    )

    # We set the password separately, so there's absolutely no chance of
    # plaintext passwords being stored in the DB.
    account.set_password(password)

    # Create a User entity linked to this account, so the user can change
    # settings, profile info, and whatever else we add.
    user = User(account=account, name=username)

    account.last_activity = datetime.utcnow()

    db.session.add_all((user, account))
    db.session.commit()

    # To keep the image directories from getting too cluttered, we'll create
    # a subdirectory for each user. Two, actually: one each for media and
    # avatars.
    # TODO: Actually set up avatars
    for d in ['media', 'avatars']:
        os.makedirs(
            os.path.join(current_app.config['UPLOADS_DEFAULT_DEST'], d, account.username),
            exist_ok=True
        )

    return account

def get_account_by_username(username):
    """Get the account object with the given username."""

    account = Account.query.filter_by(username=username).first()

    # TODO: How do we handle banned users?

    return account

def account_exists(username):
    """Check to see if an account with the given username exists."""

    return Account.query.filter_by(username=username).count() > 0

def login_username_password(username=None, password=None):
    """Attempt to log into a Pixeme account with the given username and password."""

    account = Account.query.filter_by(username=username).first()
    if account is None or not account.check_password(password):
        # According to current security practices, you shouldn't tell the user whether it's
        # the name or password that's incorrect. This is supposed to increase the burden on
        # those trying to compromise an account. It does make it harder on people who
        # genuinely mistype one or the other, however.

        # There are two schools of thought on the matter of responses in this case.
        # A 401 is the "proper" response for invalid credentials, but it requires a header
        # that doesn't really fit with a REST API. The alternative is 403, but that has
        # a different connotation. We'll use it here for now; that's very tentative.

        # Note also that we should probably institute checks for repeated authentication
        # failures, as that's likely a sign of an attempted intrusion.
        raise APIError(403, "Invalid username or password")
    elif account.role == AccountRole.banned:
        raise UserBanned(account)

    # Update activity timestamps
    account.last_login = datetime.utcnow()
    account.last_activity = datetime.utcnow()

    db.session.add(account)
    db.session.commit()

    return account

def logout():
    """Log a user out of the server."""

    # TODO: How much needs to be done here? Token blackilsting and session clearing
    # are more likely view-level concerns, IMO.

    g.account.last_activity = datetime.utcnow()
    db.session.commit()

    return True

def change_user_role(id, new_role):
    """Change a user's role to regular user, moderator, admin, or banned. This operation
    is restricted to admins only."""

    user = Account.query.get(id)

    # Change the role. This controller can also be used to ban/unban users.
    user.role = AccountRole[new_role]

    db.session.add(user)
    db.session.commit()

    return True

def delete_account(id):
    """Delete the user account with the given ID. All confirmation will be done by
    the view layer or the API."""

    user = Account.query.get(id)

    db.session.delete(user.account)
    db.session.delete(user)
    db.session.commit()

    return True

def change_user_password(id, old_password, new_password):
    """Change the given user's password."""

    user = Account.query.get(id)

    if user.check_password(old_password):
        user.set_password(new_password)
        user.last_activity = datetime.utcnow()
        db.session.add(user)
        db.session.commit()

        return True
    else:
        # TODO: What kind of error?
        return False