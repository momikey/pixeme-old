###
# Controllers for handling images, the core of Pixeme.
###

import os
# Import the Pillow Image class with a different name, so we don't
# overlap with the Pixeme model class.
from PIL import Image as PILImage

from sqlalchemy import and_

from flask import current_app, g

from pixeme import db, executor, uploads_set
from pixeme.models import (AccountRole, Image, Language, Phrase,
    PhraseStatus, Tag)
from pixeme.error import APIError, NotYetImplemented
from pixeme.helpers import printable_flake_id

from .query import use_query_parameters

### Helper functions ###

def path_for(image):
    """Return the full path for the given Image database entity."""

    # The general format for uploads is BASE_PATH/USERNAME/FILENAME.
    # BASE_PATH is set in the server config, while the others can be
    # derived from the entity itself.
    base_path = os.path.join(
        current_app.config['UPLOADS_DEFAULT_DEST'],
        current_app.config['MEDIA_URI_DIR']
    )
    username = image.uploader.account.username
    filename = image.filename

    return os.path.join(base_path, username, filename)

def directory_for(image):
    """Return only the directory containing the given Image entity."""

    # Same as in path_for(), but without the filename.
    return os.path.join(
        current_app.config['UPLOADS_DEFAULT_DEST'],
        current_app.config['MEDIA_URI_DIR'],
        image.uploader.account.username
    )

def image_url(filename):
    """Return a URL for the image with the given filename."""

    return '/'.join((
        current_app.config.get('SERVER_ROOT', 'http://localhost:5000'),
        current_app.config['MEDIA_URI_DIR'],
        filename
    ))

@executor.job
def resize_image(filename):
    """Resize the given file to fit within a server-specified size."""

    pi = PILImage.open(filename)
    pi.thumbnail(current_app.config['MAX_IMAGE_SIZE'])
    pi.save(filename)

### Controllers ###

def get_single_image(id):
    """Retrieve a single Image entity with the given ID."""

    return Image.query.get(id)

@use_query_parameters
def get_images_list(tag=None, **kwargs):
    """Retrieve a query object for a list of all images with the given
    tag, optionally filtered using query criteria. If no tag is specified,
    retrieve all iamges."""

    images = Image.query

    if tag is not None:
        images = images.filter(Image.tags.any(Tag.text.ilike(tag)))

    return images

def get_image_from_filename(filename):
    """Retrieve the image with the given filename. These have a pseudorandom
    component, so this controller should only ever find a single result.
    Returns None if no image is found."""

    image = Image.query.filter_by(filename=filename).first()

    return image

def get_random_images_sample(max_count=50):
    """Retrieve a random sample of images from the database, up to a maximum
    of `max_count` (default 50)."""

    from pixeme.helpers import sample_alias

    # Grab a random subset of images, sorted with the newest first.
    images = sample_alias(Image).query.order_by(Image.id.desc()).all()[:max_count]

    return images

def get_newest_images(count=10):
    """Retrieve the newest N images that have been posted (default 10)."""

    images = Image.query.order_by(Image.id.desc()).all()[:count]

    return images

def get_most_tagged_images(count=10):
    """Retrieve a set of images with the most tags."""

    from sqlalchemy import func
    from pixeme.models import image_tags

    # This uses the "coalesce and subquery" method we've been using elsewhere.
    tagged_images = db.session.query(image_tags.c.image, func.count('*').label('tag_count')) \
        .group_by(image_tags.c.image).subquery()

    images = db.session.query(Image, func.coalesce(tagged_images.c.tag_count, 0)) \
        .outerjoin(tagged_images, Image.id==tagged_images.c.image) \
        .order_by(tagged_images.c.tag_count.desc())[:count]

    return [i[0] for i in images]

def get_most_translated_images(count=10):
    """Retrieve the N images with the most translation phrases. At present, this doesn't
    account for the possibility of multiple phrases in the same language (e.g., synonyms)."""

    from sqlalchemy import func
    from pixeme.models import image_phrases

    # Same as above.
    translated_images = db.session.query(image_phrases.c.image, func.count('*').label('translations')) \
        .group_by(image_phrases.c.image).subquery()

    images = db.session.query(Image, func.coalesce(translated_images.c.translations, 0)) \
        .outerjoin(translated_images, Image.id==translated_images.c.image) \
        .order_by(translated_images.c.translations.desc())[:count]

    return [i[0] for i in images]

def create_image_from_upload(image_file, **kwargs):
    """Create an image entry from an uploaded file. The image will be saved
    to disk with a pseudorandom filename based on its Flake ID. Max resolution
    is saved as a server config setting; images larger than this will be scaled
    so both dimensions are no larger than their counterparts in that setting."""

    name_with_id = printable_flake_id() + '-' + image_file.filename
    filename = uploads_set.save(image_file, folder=g.account.username, name=name_with_id)

    # Do the resizing in the background
    resize_image.submit(filename)

    # Create the database entries
    upload = Image(
        filename=name_with_id,
        caption=kwargs['caption'],
        description=kwargs['description'],
        uploader=g.account.user
    )

    # Check to see if the phrase (in the user's chosen language) already exists.
    # If it does, just connect the image to that. Otherwise, create a new entry.
    phrase = Phrase.query.filter_by(language_id=kwargs['language_id'], text=kwargs['phrase']).first()

    if phrase is None:
        phrase = Phrase(
            text=kwargs['phrase'],
            language_id=kwargs['language_id'],
            submitter=g.account.user,
            images=[upload]
        )
    else:
        phrase.images.append(upload)

    upload.tags = [Tag(text=t) for t in kwargs['tags']]

    db.session.add_all((upload, phrase))
    db.session.commit()

    return upload, image_url(name_with_id)

def add_phrase_to_image(id, language_id=None, text=None):
    """Add a phrase to an image. The database ID of the phrase's language
    must also be given."""
    
    # Get both the image and the phrase's language, for later use.

    image = Image.query.get(id)

    if image is None:
        raise NotFound(type='image', additional="no known image with this ID")

    language = Language.query.get(language_id)

    if language is None:
        raise NotFound(type='language', additional="can't add a phrase with this language")

    # Phrases and examples use a slightly different pattern. Admins can add
    # wherever they want. Users can, but they are considered "proposed" by default,
    # unless the user claims fluency in the language.
    can_add = (g.account.user in language.speakers or
        g.account.role == AccountRole.admin)

    # See if this phrase already exists in the DB. If it does, then just update it.
    phrase = Phrase.query.filter(and_(Phrase.text == text, Phrase.language_id == language_id)).first()

    if phrase is None:
        # Add a new phrase if nonexistent.
        phrase = Phrase(
            text=text,
            status=PhraseStatus.accepted if can_add else PhraseStatus.proposed,
            language=language,
            submitter=g.account.user,
            images=[image]
        )
    else:
        # Simply update the existing phrase to add the new image.
        phrase.images.append(image)

    db.session.add(phrase)
    db.session.commit()

    return phrase

def add_tags_to_image(id, tags=None):
    """Add tags to an image."""

    image = Image.query.get(id)

    if image is None:
        raise NotFound(type='image', additional="no known image with this ID")

    # Convert a string into a singleton list, None into an empty list.
    if isinstance(tags, str):
        tags = [tags]
    elif tags is None:
        tags = []

    for t in tags:
        tag = Tag.query.filter_by(text=t).first()

        if tag is None:
            # If this is a new tag, create it.
            tag = Tag(text=t, images=[image])
        else:
            # The tag already exists, so just add the image to its list.
            tag.images.append(image)
        
        db.session.add_all((tag,image))
        db.session.commit()

    return image

def remove_tags_from_image(id, tags=None):
    """Remove tags from an image."""

    image = Image.query.get(id)

    if image is None:
        raise NotFound(type='image', additional="no known image with this ID")

    # Convert a string into a singleton list, None into an empty list.
    if isinstance(tags, str):
        tags = [tags]
    elif tags is None:
        tags = []

    # Attempting to remove a tag that isn't present on the image is not an error.
    new_tags = Tag.query.filter(and_(Tag.images.any(id=id), ~Tag.text.in_(tags))).all()
    image.tags = new_tags

    db.session.add(image)
    db.session.commit()

    return image

def update_image_metadata(id, metadata=None):
    """Update the metadata associated with an image."""

    image = Image.query.get(id)

    if image is None:
        raise NotFound(type='image', additional="no known image with this ID")

    # Only the uploader or an admin is allowed to change the metadata, as it
    # may contain legal information such as copyright status.
    if (g.account.user != image.uploader) and (g.account.role != AccountRole.admin):
        raise APIError(403, message="You are not allowed to edit this data")
    else:
        # Everything's okay, so we can merge the provided metadata with what's
        # already stored in the database.

        if image.image_metadata is None:
            image.image_metadata = {}

        # NOTE: See PEP 448 for the syntax here.
        image.image_metadata = {**image.image_metadata, **metadata}

        db.session.add(image)
        db.session.commit()

def delete_image_by_id(id):
    """Delete an image. This removes both the database record and the image file
    saved on disk."""

    image = Image.query.get(id)

    if image is None:
        raise NotFound(type='image', additional="no known image with this ID")

    if (g.account.user != image.uploader) and (g.account.role != AccountRole.admin):
        raise APIError(403, message="You are not allowed to delete this iamge")
    else:
        # If we made it this far, we can actually delete the image, because
        # the user has the necessary permission, and it's an existing image.

        # TODO: We probably need a check for dangerous filenames, though
        # Flask-Uploads is supposed to protect against those.
        if image.filename in os.listdir(directory_for(image)):
            os.remove(path_for(image))

        db.session.delete(image)
        db.session.commit()

        return True