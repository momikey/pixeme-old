###
# Controllers dealing with users
###

from functools import wraps

from flask import g, session

from pixeme import db
from pixeme.models import (User, Account, AccountRole, Image,
    Language, Phrase, PhraseStatus)
from pixeme.error import APIError, NoSuchUser

from .query import use_query_parameters

### Helpers ###

def user_locale():
    """Get the user's preferred locale from the session data. Falls back
    to English (`en`) if none is set."""

    return session.get('locale', 'en')

### Controllers ###

def get_single_user(id, name=None):
    """Retrieve a single user by ID or name. If neither is given,
    returns None, and it is up to the view or API layer to issue
    the appropriate error."""

    if id is not None:
        return User.query.get(id)
    elif name is not None:
        return User.query.filter(Account.username == name).first()
    else:
        return None

@use_query_parameters
def get_users_list(**kwargs):
    """Retrieve a query object representing a list of users 
    who fulfill specific query criteria."""

    # TODO: Add support for searching, etc.
    return User.query

def set_name_and_profile(id, name=None, profile={}):
    """Set or update the given user's display name and profile data. This
    is an incremental update for the profile, merging the new data with
    that already present."""

    user = User.query.get(id)

    if user is None:
        raise NoSuchUser(id=id)
    else:
        # Only change the name if one is provided. We don't want to clear
        # it by accident.
        if name is not None:
            user.name = name
        
        # PEP 448
        if user.profile is None:
            user.profile = {}
        user.profile = {**user.profile, **profile}

        db.session.add(user)
        db.session.commit()

        return user

def set_user_fluencies(id, fluencies=[]):
    """Set or update the list of a user's claimed fluencies. The user must
    have a language in the fluency list to directly add translations or
    phrases in that language. Otherwise, they are sent to a moderation queue."""

    user = User.query.get(id)

    if user is None:
        raise NoSuchUser(id=id)
    else:
        # Merge the given list with twhat's already present.
        fl = Language.query.filter(Language.id.in_(fluencies)).all()
        user.fluencies += fl

        db.session.add(user)
        db.session.commit()

        return user

def get_user_count():
    """Get a count of active, non-banned users."""

    # TODO: At a later stage, we may want to clarify that active users
    # must have some kind of upload or other contribution. For now, we
    # can pad the numbers.
    count = User.query.filter(Account.role != AccountRole.banned).count()

    return count

def get_most_active_users(count=10):
    """Get the "most active" users on the platform.  That is, those users who
    have the most approved phrases and/or uploaded images."""

    from sqlalchemy import func, text

    # We use two subqueries here, one for image uploads, the other for approved
    # phrases. We group them by user ID so we can get the right activity count,
    # which is the sum of the two.
    uploads = db.session.query(Image.uploader_id, func.count('*').label("upload_count")) \
        .group_by(Image.uploader_id).subquery()

    phrases = db.session.query(Phrase.submitter_id, func.count('*').label("phrase_count")) \
        .filter(Phrase.status==PhraseStatus.accepted).group_by(Phrase.submitter_id).subquery()

    users = db.session.query(User,
            (func.coalesce(uploads.c.upload_count,0) + func.coalesce(phrases.c.phrase_count, 0)) \
            .label('activity_score')) \
        .outerjoin(uploads, User.id==uploads.c.uploader_id) \
        .outerjoin(phrases, User.id==phrases.c.submitter_id) \
        .order_by(text('activity_score desc'))[:count]

    return users

def get_newest_users(count=10):
    """Get a list of the newest users. At present, we assume that database IDs
    increase monotonically. Thus, the highest IDs will be the newest users."""

    return User.query.order_by(User.id.desc())[:count]