# Request schemas

import re
from webargs import fields, validate

from pixeme.models import AccountRole, PhraseStatus

### Helper functions ###

def validate_username(username):
    """Validate a username to ensure that it is of a proper length and
    contains no invalid characters."""

    # For Pixeme, valid usernames must start with any Unicode letter
    # or the underscore, then be followed by at least one other "word"
    # character (letters, numbers, underscore). There are pathological
    # cases, such as `_1234`, but we can deal with them elsewhere.

    # The max username length is 64, but this may change later.
    return re.fullmatch('(?!\d)\w(?:\w{1,63})', username) is not None

### Schemas ###

# Most of these are in webargs format, so basically a dictionary of field
# names to Marshmallow `Field` objects. As they're objects rather than
# classes, we stick to the convention of all-lowercase names.

# Schema for any general query operation that needs to be paged.
query = {
    'count': fields.Int(required=False, missing=None),
    'start': fields.Int(required=False, missing=0),
    'order': fields.Str(required=False, missing=None),
    'desc': fields.Boolean(required=False, missing=False)
}

# Schema for any general "list" non-paged operation.
count = {
    'count': fields.Int(required=False, missing=10)
}

# Schema for creating a new account directly through the Pixeme API.
create_account = {
    'username': fields.Str(required=True, validate=validate_username),
    'email': fields.Email(required=True),

    # TODO: We only check that a password has at least 6 characters,
    # but later on, we should consider a password strength scheme,
    # security questions, 2FA, OAuth, and a lot of other things.
    'password': fields.Str(required=True, validate=validate.Length(min=6))
}

# Schema for creating a new user account using the Pixeme account wizard.
create_account_form = {
    'username': fields.Str(required=True, validate=validate_username),
    'email': fields.Email(required=True),

    # TODO: We only check that a password has at least 6 characters,
    # but later on, we should consider a password strength scheme,
    # security questions, 2FA, OAuth, and a lot of other things.
    'password': fields.Str(required=True, validate=validate.Length(min=6)),

    # The user has to accept the ToS.
    'tosAccept': fields.Boolean(required=True),

    # Creating an account using the wizard gives the user a chance to
    # add a couple of profile fields.
    'name': fields.Str(required=False, missing=None),
    'fluencies': fields.List(fields.Int(), required=False, missing=[]),

    # We get a few extra fields with the wizard, because disabling them
    # in templates is...not easy. The code will mostly just ignore them.

    # "Retype password" field (used in client-side validation)
    'retype': fields.Str(require=False, missing=None)
}

# Schema for updating a user's account details
update_account = {
    'user_id': fields.Int(required=True),   # sanity check
    'username': fields.Str(required=False, validate=validate_username),
    'email': fields.Email(required=False),
    'display_name': fields.Str(required=False),
    'role': fields.Str(required=False, validate=lambda r: r in AccountRole.__members__)
}

# Schema for updating a user's profile metadata
update_profile = {
    'user_id': fields.Int(required=True),
    'profile': fields.Dict(required=True)
}

# Schema for updating a user's name and profile simultaneously through the REST API
update_name_and_profile = {
    'user_id': fields.Int(required=True),
    'name': fields.Str(required=True),
    'profile': fields.Dict(required=True)
}

# Schema for a login attempt.
login = {
    'username': fields.Str(required=True),
    'password': fields.Str(required=True, validate=validate.Length(min=6))
}

# Schema for a password change attempt
change_password = {
    'old_password': fields.Str(required=True, validate=validate.Length(min=6)),
    'new_password': fields.Str(required=True, validate=validate.Length(min=6)),
    'retype': fields.Str(required=True, validate=validate.Length(min=6))
}

# Schema for a password change through the REST API.
# This does not require a retype or the old password.
change_password_api = {
    'password': fields.Str(required=True, validate=validate.Length(min=6)),
}

# Schema for the admin action of changing a user's role.
change_role = {
    'admin_id': fields.Int(required=True),   # sanity check
    'user_id': fields.Int(required=True),
    'new_role': fields.Str(required=True, validate=lambda r: r in AccountRole.__members__)
}

# Schema for simple ban/unban actions.
ban_or_unban = {
    'admin_id': fields.Int(required=True),   # sanity check
    'user_id': fields.Int(required=True)
}

# Schema to change a user's display name.
change_display_name = {
    'user_id': fields.Int(required=True),
    'name': fields.Str(required=True)
}

# Schema to update a user's language fluencies.
change_fluencies = {
    'user_id': fields.Int(required=True),
    'fluencies': fields.List(fields.Int(), required=True)
}

# Schema to create a new language entry in the database.
create_language = {
    'tag': fields.Str(required=True),
    'english': fields.Str(required=True),
    'native': fields.Str(required=False)
}

# Schema to update an existing language entry in the database.
update_language = {
    'id': fields.Int(required=True),    # sanity check
    'tag': fields.Str(required=False, missing=None),
    'english': fields.Str(required=False, missing=None),
    'native': fields.Str(required=False, missing=None)
}

# Schema for searching the list of languages.
language_search = {
    'q': fields.Str(required=True),     # query string
    'count': fields.Int(required=False, missing=10)
}

# Schema for a partial name or ID language search.
language_search_query = {
    'id': fields.Int(required=False, missing=None),
    'name': fields.Str(required=False, missing=None)
}

# Schema for searching the list of tags.
tag_search = {
    'q': fields.Str(required=True),     # query string
    'count': fields.Int(required=False, missing=10)
}

# Schema for a partial text or ID tag search.
tag_search_query = {
    'id': fields.Int(required=False, missing=None),
    'text': fields.Str(required=False, missing=None)
}

# Schema for searching the list of phrases.
phrase_search = {
    'q': fields.Str(required=True),     # query string
    'count': fields.Int(required=False, missing=10)
}

# Schema for a partial text or ID phrase search.
phrase_search_query = {
    'id': fields.Int(required=False, missing=None),
    'text': fields.Str(required=False, missing=None)
}

# Schema to create a new image with a translation phrase and optional metadata.
# This one's a little convoluted, because we have to use form data (as this will
# have an attached image file). Form data doesn't play well with nested data
# structures or JSON, so...yeah.
create_image = {
    'language_id': fields.Int(required=True),
    'phrase': fields.Str(required=True),

    # The caption describes the image in a way intended for learning.
    'caption': fields.Str(required=True),

    # The description is for more technical details. (These are hidden by default.)
    'description': fields.Str(required=False, missing=None),

    'tags': fields.List(fields.Str(), required=False, missing=[])
}

# Separate schema for image metadata.
update_image_metadata = {
    'image_id': fields.Int(required=True),
    'metadata': fields.Dict(required=True)
}

# Separate schema for image examples.
add_image_example = {
    'image_id': fields.Int(required=True),
    'example_source': {
        'language_id': fields.Int(required=True),
        'text': fields.Str(required=True)
    },
    'example_target': {
        'language_id': fields.Int(require=True),
        'text': fields.Str(required=True)
    }
}

# Additional schema for adding phrases to an image after it's been uploaded.
add_image_phrase = {
    'image_id': fields.Int(required=True),
    'language_id': fields.Int(required=True),
    'text': fields.Str(required=True)
}

# Additional schema for adding new tags to an image that has been uploaded.
add_image_tags = {
    'image_id': fields.Int(required=True),
    'tags': fields.List(fields.Str(), required=True)
}

# Additional schema for adding an "empty" tag: one without any accompanying images.
add_empty_tag = {
    'text': fields.Str(required=True)
}

# Additional schema for adding an "empty" phrase.
add_empty_phrase = {
    'text': fields.Str(required=True),
    'status': fields.Str(required=False, missing='proposed', validate=lambda s: s in PhraseStatus.__members__),
    'language_id': fields.Int(required=True),
    'submitter_id': fields.Int(required=True)
}

# Update a phrase's status.
update_phrase_status = {
    'id': fields.Int(required=True),
    'status': fields.Str(required=True, validate=lambda s: s in PhraseStatus.__members__)
}

# Update a phrase's text and status.
update_phrase = {
    'id': fields.Int(required=True),
    'text': fields.Str(required=False, missing=''),
    'status': fields.Str(required=False, validate=lambda s: s in PhraseStatus.__members__)
}