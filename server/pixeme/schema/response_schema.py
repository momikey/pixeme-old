# Response schemas

from marshmallow_enum import EnumField

from pixeme.models import *

from pixeme.controllers.image import image_url

### Helper functions ###

### Schemas ###

# Generally, the response schemas are in the form of classes, as this
# is how you're expected to work with the Marshmallow-SQLAlchemy package.
# Thus, they follow class naming guidelines in being CamelCase.

class UsersList(ma.ModelSchema):
    """Schema for retrieving a list of users."""

    account = ma.Nested(AccountSchema)

    class Meta:
        model = User
        exclude = ('submitted','approved','images')

class SingleUser(ma.ModelSchema):
    """Schema for retrieving a single user."""

    # TODO: Which fields are necessary, and which should stay as IDs?

    account = ma.Nested(AccountSchema)
    fluencies = ma.Nested(LanguageSchema, many=True)

    class Meta:
        model = User

class LanguagesList(ma.ModelSchema):
    """Schema for retrieving a list of languages."""

    class Meta:
        model = Language
        exclude = ('speakers','phrases')

class SingleLanguage(ma.ModelSchema):
    """Schema for retrieving a single language."""

    class Meta:
        model = Language

class PhrasesList(ma.ModelSchema):
    """Schema for retrieving a list of phrases."""

    status = EnumField(PhraseStatus)

    class Meta:
        model = Phrase
        exclude = ('language', 'submitter', 'approved_by', 'translations', 'images')

class TagsList(ma.ModelSchema):
    """Schema for retrieving a list of tags."""

    class Meta:
        model = Tag
        only = ('id', 'text')

class CreateImage(ma.ModelSchema):
    """Schema for the response to creating an image."""

    phrases = ma.Nested(PhraseSchema, many=True)
    tags = ma.Nested(TagsList, many=True)

    class Meta:
        model = Image
        exclude = ('examples',)

class SingleImage(ma.ModelSchema):
    """Schema for retrieving a single image."""

    tags = ma.Nested(TagsList, many=True)

    class Meta:
        model = Image

class ImagePhrases(ma.ModelSchema):
    """Schema for the phrases associated with an image."""

    language = ma.Nested(LanguagesList)
    status = EnumField(PhraseStatus)

    class Meta:
        model = Phrase
        exclude = ('images',)

class ImageExamples(ma.ModelSchema):
    """Schema for examples associated with an image."""

    phrase = ma.Nested(PhraseSchema, only=('language_id',))

    class Meta:
        model = Example

class SinglePhrase(ma.ModelSchema):
    """Schema for a single phrase."""

    language = ma.Nested(LanguagesList)
    status = EnumField(PhraseStatus)

    class Meta:
        model = Phrase

class PhraseImages(ma.ModelSchema):
    """Schema for the images associated with a phrase.
    This is tricky because we have to add the URL, which isn't stored."""

    url = ma.Function(lambda obj: image_url(obj.filename))

    class Meta:
        model = Image
        exclude = ('filename', 'uploader', 'phrases', 'tags', 'examples')