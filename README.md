![Pixeme logo](./pixeme-logo-64.png)

# Pixeme - Pictures for a thousand words

Pixeme is a place to connect images and words. Learn new vocabulary, test your knowledge, and provide translations, all with the help of pictures. Add your own if you like: photographs, drawings, and diagrams are all accepted. As long as you can describe it in a single word or phrase (in any language, not just English), bring it on!

## Progress

As of October 2020, Pixeme is in a pre-alpha state and getting close to initial launch. The model and controller layers are mostly done, while work on the static front end and REST API is proceeding. Last will be the SPA front end, which may wait until after launch.

## Layout

Ignore the `client/` directory for now. It's for the SPA, which I've put on the back burner. All work is currently being done inside `server/`.

* `server/assets` contains the front end. It has the following subdirectories:
    * `css`: Site-wide CSS (empty for now)
    * `js`: Site-wide JS, built into a script that is imported on each page
    * `scss`: SASS source files for styling
    * `templates`: Jinja templates for each page or component
* `server/migrations` holds Alembic database migrations and configuration
* `server/pixeme` is the back end. It has the following files and subdirectories:
    * `error.py`: Helper classes for informative API errors
    * `models.py`: Definitions for database models
    * `api`: Flask routes for the REST API
    * `controllers`: A controller layer to interact with the database
    * `schema`: Request and response schemas for use with Marshmallow/webargs
    * `static`: Static assets such as post-build site CSS/JS (this is *not* in the repo!)
    * `views`: Routes that handle the view layer
* `server/tests` has, of course, tests

Inside the `server/` directory itself are a few configuration files, as well as localization support; I may move that into its own subdirectory later on.

## Roadmap

As of right now, everything is on the roadmap. These are the main areas that need work before launch:

* Finish all views
* REST API and documentation for it
* Locale support
* Much, much more

## Dependencies

* Python 3.6+ (I have made some accommodations for 3.5)
* Flask 1.1.2
* PostgreSQL 9.6+ (the version installed on my dev PC)
* SQLAlchemy 1.3
* Babel 2.8.0 (The Python i18n library Babel, not Babel.js!)
* webargs 6.x
* TailwindCSS 1.8
* Alpine JS 2.4

## License

The technology behind Pixeme is open source software under the [MIT License](LICENSE).

Copyright © 2020 Michael H. Potter
